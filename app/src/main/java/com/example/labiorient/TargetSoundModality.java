package com.example.labiorient;

import android.content.Context;
import android.util.Log;

import com.jsyn.unitgen.LineOut;
import com.jsyn.unitgen.SineOscillator;

import java.util.ArrayList;

class TargetSoundModality extends Modality {

    SineOscillator oscillator1;
    LineOut lineOut;

    private double minimo = 0;

    public TargetSoundModality(Context applicationContext, double direzioneAiuto, ArrayList<Double> angoliPermessi) {
        super(angoliPermessi,direzioneAiuto);
        synthesizer.add(oscillator1 = new SineOscillator());

        synthesizer.add(lineOut = new LineOut());

        setVolume(0);
        oscillator1.output.connect(0, lineOut.input, 0);
        oscillator1.output.connect(0, lineOut.input, 1);
        oscillator1.frequency.set(360);

        synthesizer.start();
        lineOut.start();

        setAngoloPrecedente(a1);

        Log.e("TSM","START");
    }

    @Override
    public void applyMode(double angoloSettoreCorrente, double angoloCorrente) {
        diff = calcoloAngolo(a1,angoloCorrente,angoloSettoreCorrente);
        if (diff <= tolleranza) {
            if (non_ha_già_fatto_ping) {
                oscillator1.frequency.set(360);
                setVolume(0.65);
                non_ha_già_fatto_ping = false;
            }
        } else if (diff > tolleranza && diff < Math.abs(a1)){
            setVolume(0);
            non_ha_già_fatto_ping = true;
        }
        if(aiutoGuida) FrequencyHelp(angoloCorrente,oscillator1);
    }

    private void FrequencyHelp(double angoloCorrente, SineOscillator oscillator1) {
        if(getDiff(angoloHelp, angoloCorrente,360) < 1){
            oscillator1.frequency.set(366);
            setVolume(0.65);
        }
    }

    @Override
    public void setSilence() {
        try {
            setVolume(0);
            synthesizer.stop();
            lineOut.stop();
            super.setSilence();
            oscillator1=null;
            synthesizer=null;
        }
        catch (NullPointerException e){;}
        catch (IllegalStateException e){;}
        Log.d("TSM", "Silence!");
    }

    private void setVolume(double i) {
        oscillator1.amplitude.set(i);
    }



}
