package com.example.labiorient;

public class Coordinata {

	public int x, y;

	public Coordinata(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	@Override
	public String toString() {
		return "x = "+this.getX()+"; y = "+ this.getY()+";  ";
	}

	public boolean diversa(Coordinata due) {
		if(this.getX()!=due.getX() && this.getY()!=due.getY())
			return true;
		else
			return false;
	}
	
}
