package com.example.labiorient;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Salvataggio {

    private final static String TAG = Salvataggio.class.getCanonicalName();

    // FUNZIONI PER LA MEMORIZZAZIONE DELLO STATO DI GIOCO

    // controllo se c'è una partita salvata
    boolean presenzaPartitaSalvata(Context context){
        SharedPreferences sharedSession = context.getSharedPreferences(TAG,0);
        return sharedSession.getBoolean("datiInMemoria", false);
    }

    // memorizzo i dati del gioco per riprendere successivamente la partita
    void memorizzaStatoDelGioco(Labirinto labirinto, Context context, long timer, String difficolta){
        SharedPreferences sharedSession = context.getSharedPreferences(TAG,0);
        SharedPreferences.Editor edit = sharedSession.edit();
        edit.putBoolean("datiInMemoria",true);
        // memorizzo lo stato del labirinto per la ricostruzione
        int xgioco = labirinto.x;
        int ygioco = labirinto.y;
        edit.putInt("asseX",xgioco);
        edit.putInt("asseY",ygioco);
        edit.putInt("xu",Gioco.xu);
        edit.putInt("yu",Gioco.yu);
        edit.putInt("fineX",labirinto.fine.getX());
        edit.putInt("fineY",labirinto.fine.getY());
        // memorizzo stato del timer, direzione e difficoltà del gioco
        edit.putLong("timer",timer);
        edit.putString("direzione", ""+Gioco.direzione);
        edit.putString("difficoltaUltimoLabirinto", difficolta);
        // memorizzo i contatori
        edit.putInt("conteggioVicoli",Gioco.conteggioV);
        edit.putInt("conteggioPassi",Gioco.conteggioPassi);
        edit.putInt("conteggioLoop",Gioco.conteggioLoop);
        // salvare le caratteristiche
        edit.putInt("vc",labirinto.caratteristiche[0]); // vc
        edit.putInt("lp",labirinto.caratteristiche[1]); // loop
        edit.putInt("n1",labirinto.caratteristiche[2]); // numero di 1
        edit.putInt("intersections",labirinto.caratteristiche[3]); // intersezioni
        // memorizzo il labirinto con un set di stringhe identificate dal numero di riga.
        for (int k=0; k<xgioco; k++){
            String row = "";
            for (int l=0; l<ygioco; l++){
                row+=String.valueOf(labirinto.maze[k][l]);
            }
            edit.putString("Riga"+k,row);
        }
        edit.apply();
    }

    // ritorno se vi è una partita memorizzata
    void partitaMemorizzata(Context context, boolean memorizza) {
        SharedPreferences sharedSession = context.getSharedPreferences(TAG, 0);
        SharedPreferences.Editor edit = sharedSession.edit();
        Log.e("SALVATAGGIO", memorizza ? "true" : "false");
        edit.putBoolean("datiInMemoria", memorizza);
        edit.apply();
    }

    // recupero la partita
    Labirinto recuperaPartita(Context context){
        SharedPreferences sharedSession = context.getSharedPreferences(TAG,0);
        int x=sharedSession.getInt("asseX",1);
        int y=sharedSession.getInt("asseY",1);
        // recupero lo stato del labirinto per la ricostruzione
        Labirinto labirinto = new Labirinto((x-2)/2,(y-2)/2);
        Gioco.direzione=sharedSession.getString("direzione","a").charAt(0);
        // recupero i contatori
        Gioco.conteggioV=sharedSession.getInt("conteggioVicoli",0);
        Gioco.conteggioPassi=sharedSession.getInt("conteggioPassi",0);
        Gioco.conteggioLoop=sharedSession.getInt("conteggioLoop",0);
        // recupero le caratteristiche iniziali del labirinto
        labirinto.caratteristiche[0] = sharedSession.getInt("vc",0); // vc
        labirinto.caratteristiche[1] = sharedSession.getInt("lp",0); // loop
        labirinto.caratteristiche[2] = sharedSession.getInt("n1",0); // numero di 1
        labirinto.caratteristiche[3] = sharedSession.getInt("intersections",0); // intersezioni
        // recupero la struttura del labirinto
        for (int k=0; k<x; k++){
            String row = sharedSession.getString("Riga"+k,"010");
            for (int l=0; l<y; l++){
                labirinto.maze[k][l]=Integer.parseInt(""+row.charAt(l));
            }
        }

        Gioco.xu=sharedSession.getInt("xu",0); // posizione x dell'utente
        Gioco.yu=sharedSession.getInt("yu",0); // posizione y dell'utente
        labirinto.fine= new Coordinata(sharedSession.getInt("fineX",0),sharedSession.getInt("fineY",0));
        labirinto.inizio= new Coordinata(Gioco.xu,Gioco.yu);

        if(labirinto.maze[Gioco.xu][Gioco.yu]==0 ){
            System.out.println("RIDEFINISCO PUNTO DI INIZIO");
            labirinto.inizio = Labirinto.posizioneValore(8,labirinto.maze);
            if(labirinto.maze[labirinto.inizio.getX()][labirinto.inizio.getY()]==0){
                labirinto.inizio = labirinto.definePosizione(labirinto.maze,8);
            }
            Gioco.xu = labirinto.inizio.getX();
            Gioco.yu = labirinto.inizio.getY();
        }
        if(labirinto.maze[labirinto.fine.getX()][labirinto.fine.getY()]==0){
            System.out.println("RIDEFINISCO PUNTO DI FINE");
            labirinto.fine = Labirinto.posizioneValore(9,labirinto.maze);
            if(labirinto.maze[labirinto.inizio.getX()][labirinto.inizio.getY()]==0){
                labirinto.fine = labirinto.definePosizione(labirinto.maze,9);
            }
        }
        return labirinto;
    }

    // recupero il timer
    long recuperaTimer(Context context) {
        SharedPreferences sharedSession = context.getSharedPreferences(TAG,0);
        return sharedSession.getLong("timer", 600000);
    }

    // incremento il valore di un elemento salvato
    void incrementaPrestazione(Context context,String key) {
        SharedPreferences sharedSession = context.getSharedPreferences(TAG,0);
        SharedPreferences.Editor edit = sharedSession.edit();
        Log.e("INCREMENTO",key);
        edit.putInt(key,sharedSession.getInt(key,0)+1);
        edit.apply();
    }

    // restituisco il valore intero di una chiave memorizzata
    int ottieniPrestazione(Context context, String key) {
        SharedPreferences sharedSession = context.getSharedPreferences(TAG,0);
        return sharedSession.getInt(key, 0);
    }

    // aggiorno il valore float di una chiave memorizzata
    void aggiornaPrestazioneFloat(Context context, String key, float prestazione) {
        SharedPreferences sharedSession = context.getSharedPreferences(TAG,0);
        SharedPreferences.Editor edit = sharedSession.edit();
        edit.putFloat(key,prestazione);
        edit.apply();
    }

    // aggiorno il valore di una chiave Long
    void aggiornaPrestazioneLong(Context context, String key, long prestazione) {
        SharedPreferences sharedSession = context.getSharedPreferences(TAG,0);
        SharedPreferences.Editor edit = sharedSession.edit();
        edit.putLong(key,prestazione);
        edit.apply();
    }

    // restituisco il valore float di una chiave memorizzata
    float ottieniPrestazioneFloat(Context context, String key) {
        SharedPreferences sharedSession = context.getSharedPreferences(TAG,0);
        return sharedSession.getFloat(key, 0);
    }

    // restituisco il valore di una chiave Long
    long ottieniPrestazioneLong(Context context, String key) {
        SharedPreferences sharedSession = context.getSharedPreferences(TAG,0);
        return sharedSession.getLong(key, 0);
    }

    // aggiorno il valore del tempo di percorrenza
    void aggiornaTempoPercorrenza(Context context, long finalMillisecondi, String difficolta) {
        long valoreAttuale = ottieniPrestazioneLong(context,"tempo_percorrenza_"+difficolta);
        incrementaPrestazione(context,"contatore_tempo_percorrenza_"+difficolta);
        long valore_tempo_percorrenza = (valoreAttuale + finalMillisecondi);
        aggiornaPrestazioneLong(context,"tempo_percorrenza"+difficolta,valore_tempo_percorrenza);
    }

    // restituisco il valore della difficoltà
    public String ottieniImpostazioneDifficolta(Context context) {
        SharedPreferences sharedSession = PreferenceManager.getDefaultSharedPreferences(context);
        String diff = sharedSession.getString("difficolta", "facile");
        if(diff.equals("nessuna")){
            CustomDecisionTree dt = new CustomDecisionTree(0.6);
            diff = dt.difficoltaUtente("PerformanceLabiorient");
        }
        return diff;
    }

    // recupero la difficoltà dell'ultimo labirinto
    public String recuperaDifficoltaUltimoLabirinto(Context context) {
        SharedPreferences sharedSession = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedSession.getString("difficoltaUltimoLabirinto", "facile");
    }

    // restituisco il valore sottoforma di stringa della preferenza e controllo se è abilitata
    boolean preferenzaSwitch(Context context, String preferenza){
        SharedPreferences sharedSession = PreferenceManager.getDefaultSharedPreferences(context);
        return (sharedSession.getString(preferenza, "Abilitata")).equals("Abilitata");
    }

    // aggiorno il numero della partita
    public void aggiornaNumeroPartita(Context context, int numeroPartita) {
        SharedPreferences sharedSession = context.getSharedPreferences(TAG,0);
        SharedPreferences.Editor edit = sharedSession.edit();
        edit.putInt("numeroPartita",(numeroPartita+1)%5);
        edit.apply();
    }

    // salvo in un file txt le caratteristiche della partita dell'utente
    public void salvaPrestazioniPartita(Context context, int numeroPartita, String difficolta, int[] caratteristiche, int conteggioPassi, int conteggioV, int conteggioLoop, long tempoImpiegato, long tempoMassimo, double errore_accumulato, long tempo_accumulato, List<String> misure) {
        // Scrivo su un file txt tutto con un certo formato di codifica di modo che sia leggibile successivamente.
        // - numero partita, difficoltà, numero di vicoli ciechi lab, numero loop lab, numero passi fatti, numero vicoli ciechi incontrati, numero loop incontrati, tempo impiegato totale, tempo medio intersezione (calcolo da solo), errore medio intersezione (calcolo da solo), gioco concluso/sospeso/fallito (statoDelGioco) (SEQUENZA STANDARD)
        // - errore intersezione (tanti) calcolo media e varianza con indicato il tipo di guida usata e tempo (LISTA)
        // CREO LA CARTELLA E IL FILE
        File file1 = null, filePartite = null, fileErrore1 = null;
        BufferedWriter bw1 = null, bw2 = null, bw3 = null;
        FileWriter fw1 = null, fw2 = null, fw3 = null;
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            try {
                // creo i file utili
                String myfolder = Environment.getExternalStorageDirectory() + "/PerformanceLabiorient";
                File f = new File(myfolder);
                if (!f.exists()) {
                    File folder = Environment.getExternalStorageDirectory(); // oppure context.getFilesDir()
                    f = new File(folder, "PerformanceLabiorient");
                    f.mkdir();
                }
                file1 = UnFilePerPartita("partita",numeroPartita);
                file1.delete();
                filePartite = UnFilePerTutteLePartite();
                fileErrore1 = UnFilePerPartita("intersezioni",numeroPartita);
                fileErrore1.delete();

                // imposto i buffer per la scrittura dei file
                fw1 = new FileWriter(file1, true);
                bw1 = new BufferedWriter(fw1);
                fw2 = new FileWriter(filePartite, true);
                bw2 = new BufferedWriter(fw2);
                fw3 = new FileWriter(fileErrore1, true);
                bw3 = new BufferedWriter(fw3);

                double errore_medio=0, tempo_medio=0;
                try {
                    errore_medio = errore_accumulato / misure.size();
                    tempo_medio = tempo_accumulato / misure.size();
                } catch (ArithmeticException e){;}
                // imposto un metodo di scrittura ... ho scelto quello più modulare e separato per ogni scopo.
                //metodoScrittura1(numeroPartita, difficolta, caratteristiche, conteggioPassi, conteggioV, conteggioLoop, tempoImpiegato, errore_accumulato, tempo_accumulato, misure, bw1);
                //metodoScrittura2(numeroPartita, difficolta, caratteristiche, conteggioPassi, conteggioV, conteggioLoop, tempoImpiegato, misure, bw1);
                //metodoScrittura3(numeroPartita, difficolta, caratteristiche, conteggioPassi, conteggioV, conteggioLoop, tempoImpiegato, tempoMassimo, errore_medio, tempo_medio, misure, bw1, bw2, bw3);
                metodoScrittura4(difficolta, caratteristiche, conteggioPassi, conteggioV, conteggioLoop, tempoImpiegato, tempoMassimo, errore_medio, tempo_medio, misure, bw1, bw2, bw3);
            } catch (IOException e) {
                Log.e("MEMORIZZAZIONE", "IO EXCEPTION");
                e.printStackTrace();
            } catch (NullPointerException exe) {
                Log.e("MEMORIZZAZIONE", "NULLPOINTER EXCEPTION");;
            }
        }
    }

    private File UnFilePerPartita(String nomeFile,int numeroPartita) {
        File file;
        file = new File(Environment.getExternalStorageDirectory() + "/PerformanceLabiorient/"+ nomeFile + numeroPartita + ".txt");
        Log.i(TAG, file.getPath());
        return file;
    }

    private File UnFilePerTutteLePartite() throws IOException {
        File filePartite;
        filePartite = new File(Environment.getExternalStorageDirectory() + "/PerformanceLabiorient/partite.txt");
        filePartite.createNewFile();
        return filePartite;
    }

    private void metodoScrittura1(int numeroPartita, String difficolta, int[] caratteristiche, int conteggioPassi, int conteggioV, int conteggioLoop, long tempoImpiegato, long tempoMassimo, double errore_accumulato, long tempo_accumulato, List<String> misure, BufferedWriter bw1) throws IOException {
        bw1.write(String.valueOf(numeroPartita)); // numero partita
        bw1.newLine();
        bw1.write(difficolta); // difficoltà
        bw1.newLine();
        bw1.write(String.valueOf(caratteristiche[0])); // vicoli ciechi presenti
        bw1.newLine();
        bw1.write(String.valueOf(caratteristiche[1])); // loop presenti
        bw1.newLine();
        bw1.write(String.valueOf(caratteristiche[2])); // 1 presenti
        bw1.newLine();
        bw1.write(String.valueOf(caratteristiche[3])); // intersezioni presenti
        bw1.newLine();
        bw1.write(String.valueOf(conteggioPassi)); // passi fatti
        bw1.newLine();
        bw1.write(String.valueOf(conteggioV)); // vicoli ciechi incontrati
        bw1.newLine();
        bw1.write(String.valueOf(conteggioLoop)); // loop incontrati
        bw1.newLine();
        bw1.write(misure.size()); // intersezioni incontrate
        bw1.newLine();
        bw1.write(String.valueOf(tempoImpiegato)); // tempo impiegato
        bw1.newLine();
        bw1.write(String.valueOf(tempoMassimo)); // tempo massimo per la difficoltà
        bw1.newLine();
        bw1.write(String.valueOf(errore_accumulato/misure.size())); // errore angolare medio
        bw1.newLine();
        bw1.write(String.valueOf(tempo_accumulato/misure.size())); // tempo medio intersezione
        bw1.newLine();
        bw1.flush();
        for (String measure : misure) {
            bw1.write(measure); // tempo;errore;categoriaIntersezione;guida
            bw1.newLine();
        }
        bw1.flush();
    }

    private void metodoScrittura2(int numeroPartita, String difficolta, int[] caratteristiche, int conteggioPassi, int conteggioV, int conteggioLoop, long tempoImpiegato, long tempoMassimo, List<String> misure, BufferedWriter bw1) throws IOException {
        bw1.write("NumeroPartita;Difficoltà;VCpresenti;LoopPresenti;IntersezioniPresenti;Numero1Presenti;PassiFatti;VCincontrati;" +
                "LoopIncontrati;IntersezioniIncontrate;TempoImpiegato;TempoMassimo;TempoIntersezione;ErroreIntersezione;CategoriaIntersezione;GuidaUtilizzata;");
        bw1.newLine();
        for (String measure : misure) {
            bw1.write(String.valueOf(numeroPartita)+ ";"+difficolta+ ";"+String.valueOf(caratteristiche[0])+ ";"+String.valueOf(caratteristiche[1])+ ";"
                    + String.valueOf(caratteristiche[2])+ ";"+ String.valueOf(caratteristiche[3])+ ";"+String.valueOf(conteggioPassi)+ ";"
                    + String.valueOf(conteggioV)+ ";"+String.valueOf(conteggioLoop)+ ";"+String.valueOf(misure.size())+ ";"
                    + String.valueOf(tempoImpiegato)+ ";"+String.valueOf(tempoMassimo)+";" +measure);
            bw1.newLine();
        }
        bw1.flush();
    }

    private void metodoScrittura3(int numeroPartita, String difficolta, int[] caratteristiche, int conteggioPassi, int conteggioV, int conteggioLoop, long tempoImpiegato, long tempoMassimo, double errore_medio, double tempo_medio, List<String> misure, BufferedWriter bw1, BufferedWriter bw2, BufferedWriter bw3) throws IOException {
        // dati della singola partita
        bw1.write("NumeroPartita;Difficoltà;VCpresenti;LoopPresenti;IntersezioniPresenti;Numero1Presenti;PassiFatti;VCincontrati;" +
                "LoopIncontrati;IntersezioniIncontrate;TempoImpiegato;TempoMassimo;ErroreMedioIntersezioni;TempoMedioIntersezioni;");
        bw1.newLine();
        bw1.write(String.valueOf(numeroPartita)+ ";"+difficolta+ ";"+String.valueOf(caratteristiche[0])+ ";"+String.valueOf(caratteristiche[1])+ ";"
                + String.valueOf(caratteristiche[2])+ ";"+ String.valueOf(caratteristiche[3])+ ";"+String.valueOf(conteggioPassi)+ ";"
                + String.valueOf(conteggioV)+ ";"+String.valueOf(conteggioLoop)+ ";"+String.valueOf(misure.size())+ ";"
                + String.valueOf(tempoImpiegato)+ ";" +String.valueOf(tempoMassimo)+";" + String.valueOf(errore_medio) +";"+ String.valueOf(tempo_medio) + ";");
        bw1.newLine();
        bw1.flush();
        // dati di tutte le partite
        bw2.write(String.valueOf(numeroPartita)+ ";"+difficolta+ ";"+String.valueOf(caratteristiche[0])+ ";"+String.valueOf(caratteristiche[1])+ ";"
                + String.valueOf(caratteristiche[2])+ ";"+ String.valueOf(caratteristiche[3])+ ";"+String.valueOf(conteggioPassi)+ ";"
                + String.valueOf(conteggioV)+ ";"+String.valueOf(conteggioLoop)+ ";"+String.valueOf(misure.size())+ ";"
                + String.valueOf(tempoImpiegato)+ ";"+String.valueOf(tempoMassimo)+";" + String.valueOf(errore_medio) +";"+ String.valueOf(tempo_medio) + ";");
        bw2.newLine();
        bw2.flush();
        // dati delle intersezioni della singola partita
        bw3.write("TempoIntersezione;ErroreIntersezione;CategoriaIntersezione;GuidaUtilizzata;");
        bw3.newLine();
        for (String measure : misure) {
            bw3.write(measure);
            bw3.newLine();
        }
        bw3.flush();
    }

    private void metodoScrittura4(String difficolta, int[] caratteristiche, int conteggioPassi, int conteggioV, int conteggioLoop, long tempoImpiegato, long tempoMassimo, double errore_medio, double tempo_medio, List<String> misure, BufferedWriter bw1, BufferedWriter bw2, BufferedWriter bw3) throws IOException {
        bw1.write(difficolta);
        bw1.newLine();
        bw1.write(String.format("%.5f",((float)misure.size())/(caratteristiche[3]*2))+ ";"
                + String.format("%.5f",((float)conteggioPassi)/(caratteristiche[2]*2))+ ";"
                + String.format("%.5f",((float)conteggioV)/caratteristiche[0])+ ";"
                + String.format("%.5f",((float)conteggioLoop)/(Math.max(caratteristiche[1],1)*4))+ ";"
                + String.format("%.5f",((float)tempoImpiegato)/tempoMassimo)+";");// + String.valueOf(errore_medio) +";"+ String.valueOf(tempo_medio) + ";");
        bw1.newLine();
        bw1.flush();
        // dati di tutte le partite
        bw2.write(difficolta+ ";"
                + String.format("%.5f",((float)misure.size())/(caratteristiche[3]*2))+ ";"
                + String.format("%.5f",((float)conteggioPassi)/(caratteristiche[2]*2))+ ";"
                + String.format("%.5f",((float)conteggioV)/caratteristiche[0])+ ";"
                + String.format("%.5f",((float)conteggioLoop)/(Math.max(caratteristiche[1],1)*4))+ ";"
                + String.format("%.5f",((float)tempoImpiegato)/tempoMassimo)+";");// + String.valueOf(errore_medio) +";"+ String.valueOf(tempo_medio) + ";");
        bw2.newLine();
        bw2.flush();
        // dati delle intersezioni della singola partita
        for (String measure : misure) {
            bw3.write(measure);
            bw3.newLine();
        }
        bw3.flush();
    }

    // recupero la lista di misure
    public List<String> recuperaMisure(Context context) {
        SharedPreferences sharedSession = context.getSharedPreferences(TAG,0);
        List<String> list = null;
        try{
            list = new ArrayList<String>(sharedSession.getStringSet("misure", null));
        } catch (NullPointerException e){
            list = new ArrayList<String>();
        }
        return list;
    }

    // salvo la lista di misure
    public void salvaMisure(Context context, List<String> listOfMeasures) {
        Log.e("SALVATAGGIO", "MISURE");
        if(listOfMeasures != null) {
            Log.e("MISURE SALVATE",listOfMeasures.toString());
            SharedPreferences sharedSession = context.getSharedPreferences(TAG, 0);
            SharedPreferences.Editor edit = sharedSession.edit();
            edit.putStringSet("misure", new HashSet<String>(listOfMeasures));
            edit.apply();
        }
    }

}