package com.example.labiorient;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;

import com.jsyn.unitgen.LineOut;
import com.jsyn.unitgen.SquareOscillator;

import java.util.ArrayList;

/**
 * Created by gabriele_galimberti on 15/04/18.
 */

class DiscreteIntermittenceModality extends Modality {

    SquareOscillator oscillator2;
    LineOut lineOut;

    private int a=1, b=14;
    private double minimo = 0;

    public MediaPlayer mp = null;


    public DiscreteIntermittenceModality(Context applicationContext, double direzioneAiuto, boolean makePing, ArrayList<Double> angoliPermessi) {
        super(angoliPermessi,direzioneAiuto);
        synthesizer.add(lineOut = new LineOut());
        synthesizer.add(oscillator2 = new SquareOscillator());

        oscillator2.output.connect(0,lineOut.input,0);
        oscillator2.output.connect(0,lineOut.input,1);
        oscillator2.amplitude.set(0.4);
        oscillator2.frequency.set(1);

        synthesizer.start();
        lineOut.start();

        this.makePing = makePing;
        mp = new MediaPlayer();
        mp = MediaPlayer.create(applicationContext, R.raw.ping_2);
        setAngoloPrecedente(a1);

        Log.e("DISCR INTERMIT MODALITY","START");
    }

    @Override
    public void applyMode(double angoloSettoreCorrente, double angoloCorrente) {

        diff = calcoloAngolo(a1,angoloCorrente,angoloSettoreCorrente);

        if (diff <= tolleranza) {
            if (non_ha_già_fatto_ping) {
                oscillator2.frequency.set(b + a);
                if(makePing)
                    mp.start();
                non_ha_già_fatto_ping = false;
            }
        } else if (diff > tolleranza && diff < Math.abs(a1)) {
            non_ha_già_fatto_ping = true;
            minimo = setIntermittenzaEsponenziale(a1, diff);
        }
        oscillator2.frequency.set(b * (minimo) + a);
        if(makePing) zeroCrossing(diff, mp);
        if(aiutoGuida) FrequencyHelp(angoloCorrente,oscillator2);
    }

    private void FrequencyHelp(double angoloCorrente, SquareOscillator oscillator2) {
        if(getDiff(angoloHelp, angoloCorrente,360) < 1){
            oscillator2.frequency.set(b * 20 + a);
            oscillator2.amplitude.set(0.1);
        } else {
            oscillator2.amplitude.set(0.4);
        }
    }

    @Override
    public void setSilence() {
        try {
            oscillator2.amplitude.set(0);
            synthesizer.stop();
            lineOut.stop();
            super.setSilence();
            oscillator2=null;
            synthesizer=null;
            if(mp != null){
                mp.stop();
                mp.release();
                mp = null;
            }
            Log.e("DISCR INTERMIT MODALITY","Silence!");
        }
        catch (IllegalStateException e){;}
        catch (NullPointerException e){;}
    }
}
