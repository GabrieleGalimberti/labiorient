package com.example.labiorient;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

class PrestazioniAdapter extends ArrayAdapter<Prestazione> {

    private LayoutInflater prestazioniInfo;

    public PrestazioniAdapter(@NonNull Context context, @LayoutRes int resource, ArrayList<Prestazione> textViewResourceId) {
        super(context, resource, textViewResourceId);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null) convertView = prestazioniInfo.from(getContext()).inflate(R.layout.prestazione,null);

        Prestazione currentPrestazione = getItem(position);
        TextView title = (TextView) convertView.findViewById(R.id.prestazioneDescription);
        TextView value = (TextView) convertView.findViewById(R.id.value);

        title.setText(currentPrestazione.getDescrizione());
        value.setText(String.valueOf((int)currentPrestazione.getValuePrestazioneDouble()));

        convertView.setTag(position);
        return convertView;
    }
}
