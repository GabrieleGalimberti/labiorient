package com.example.labiorient;

/**
 * Created by gabriele_galimberti on 04/04/18.
 */

public class Measure {

    private long durata;
    private String errore;
    private String categoriaDistanza;
    private int guideMode;

    public Measure(long i, String m, String n, int guide) {
        this.durata = i;
        this.errore = m;
        this.categoriaDistanza = n;
        this.guideMode = guide;
    }

    @Override
    public String toString() { return String.valueOf(durata) + ";" + errore + ';' + categoriaDistanza +";" + String.valueOf(guideMode) + ";"; }

}