package com.example.labiorient;

import android.content.Context;
import android.util.Log;

import com.jsyn.unitgen.Delay;
import com.jsyn.unitgen.LineOut;
import com.jsyn.unitgen.SquareOscillator;
import java.util.ArrayList;

/**
 * Created by gabriele_galimberti on 15/04/18.
 */

class DelayNoTarget extends Modality {

    SquareOscillator oscillator;
    LineOut lineOut;
    Delay delay;


    private int a=8;
    private double minimo = 0;

    public DelayNoTarget(double direzioneAiuto, ArrayList<Double> angoliPermessi) {
        super(angoliPermessi,direzioneAiuto);
        synthesizer.add(lineOut = new LineOut());
        synthesizer.add(oscillator = new SquareOscillator());
        synthesizer.add(delay = new Delay());

        delay.output.connect(0,lineOut.input,0);
        delay.output.connect(0,lineOut.input,1);
        oscillator.output.connect(0,lineOut.input,0);
        oscillator.output.connect(0,lineOut.input,1);
        oscillator.amplitude.set(0.4);
        oscillator.frequency.set(1);

        delay.input.connect(oscillator.output);
        delay.allocate(176400);
        delay.generate(0,8);
        synthesizer.start();
        lineOut.start();
        setAngoloPrecedente(a1);

        Log.e("RNT MODALITY","START");
    }

    @Override
    public void applyMode(double angoloSettoreCorrente, double angoloCorrente) {

        diff = calcoloAngolo(a1,angoloCorrente,angoloSettoreCorrente);
        delay.allocate(176400);

        if (diff <= tolleranza) {
            if (non_ha_già_fatto_ping) {
                delay.generate(0, 2);
                non_ha_già_fatto_ping = false;
            }
        } else if (diff > tolleranza && diff < Math.abs(a1)) {
            non_ha_già_fatto_ping = true;
            minimo = setIntermittenzaLineare(a1, diff);
            delay.generate(0, (int) (8-a*minimo));

        }
        if(aiutoGuida) FrequencyHelp(angoloCorrente);
    }

    private void FrequencyHelp(double angoloCorrente) {
        if(getDiff(angoloHelp, angoloCorrente,360) < 1){
            //delay.allocate(1);
            delay.generate(0, 1);
        }
    }

    @Override
    public void setSilence() {
        try {
            oscillator.amplitude.set(0);
            synthesizer.stop();
            lineOut.stop();
            super.setSilence();
            delay=null;
            oscillator=null;
            synthesizer=null;
            Log.e("RNT MODALITY","Silence!");
        }
        catch (IllegalStateException e){;}
        catch (NullPointerException e){;}
    }
}
