package com.example.labiorient;

import android.content.Context;
import android.graphics.Point;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class Gioco extends AppCompatActivity implements SensorEventListener, TextToSpeech.OnInitListener {

    private final static String TAG = Gioco.class.getCanonicalName();
    public static Random r = new Random();
    private static boolean continueRun = false;
    public static char direzione = 'a', direzionePrecedente='?';
    public static Coordinata posizioneVicolo;
    public static TextToSpeech speaker;
    protected static double predirection=0, direzioneAiuto=0;
    public static ArrayList<Double> angoliPermessi = null;
    public static int xu=0, yu=0;   // posizione dell'utente
    public static long millisecondi=600000;
    protected static boolean loop=false, multiLoop=false, permessoVibrazione=false, permessoAiuto=false, labirintoTrovato=false;
    protected static Vibrator v=null;

    public boolean start=true, vicoloCieco=false, interazione=true,
            vincoloTemporale = true, percorso=true, diNuovoVicoloCieco=false,
            nuovaPartita=false, conteggioVicolo = true, tempoScaduto=false,
            ORIENTAMENTO_TRAMITE_SENSORI=true, tocco=false, permessoNotificaBordo=false, notificaBordo=false;
    public long inizioTS, millisecondiallafine =0, tempo, tempo_inizio;
    public float toccoX, toccoY;
    float K[] = new float[9];    // matrice di rotazione
    float orientation[] = new float[3];
    private double angoloCorrente = 0, setOrientamento = 0, angoloCorrenteAssoluto = 0, angoloSettoreCorrente=0;
    private int modalita = 0, widthHalfScreen=0, heightHalfScreen=0;
    private Modality modality = null;
    private AudioFactory audioFactory = new AudioFactory();
    private Timer timer = null;
    public ArrayList<Coordinata> vie = new ArrayList<>();
    private Semaforo semaforo = null;
    private MediaPlayer earcon = null, alarm=null;
    public Labirinto labirinto = null;
    private Salvataggio salvataggio;
    public String difficolta = "facile";
    public CountDownTimer scadenza;
    public LabirinthManager labirinthManager = null;
    private SensorManager mSensorManager = null;
    private Sensor mRotationVector = null;
    private Button taskButton = null;

    // dati delle prestazioni
    protected List<String> misure = null;
    protected static int conteggioV=0, conteggioPassi=0, conteggioLoop=0;   // contatori
    protected long tempoImpiegato=0, tempo_accumulato=0;
    protected float errore_accumulato=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // set della schermata principale del Gioco.
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_gioco);
        // invoco un manager che mi gestisca alcuni eventi nel labirinto.
        labirinthManager = new LabirinthManager(getApplicationContext());
        // preparo lo speaker per comunicazioni verbali
        speaker = new TextToSpeech(this,this);
        speaker.setSpeechRate(2);
        // imposto i manager per l'ottenimento dell'orientamento basato su GAME_ROTATION_VECTOR
        mSensorManager = (SensorManager) this.getSystemService(Context.SENSOR_SERVICE);
        mRotationVector = mSensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR);
        // se il device non dispone dei sensori o non sono funzionanti allora è possibile orientarsi tramite tocco sullo schermo.
        if (mRotationVector == null || mSensorManager==null) {
            ORIENTAMENTO_TRAMITE_SENSORI = false;
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            widthHalfScreen=size.x/2;
            heightHalfScreen=size.y/2;
        }
        // preparo gli oggetti per le earcon sonore
        earcon = new MediaPlayer();
        alarm = new MediaPlayer();
        // oggetto per il salvataggio di dati, impostazioni e preferenze
        salvataggio = new Salvataggio();
        // imposto la vibrazione
        v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        // recupero della difficoltà dalle impostazioni
        difficolta = salvataggio.ottieniImpostazioneDifficolta(this);
        Log.e("DIFFICOLTA",difficolta);
        // recupero delle impostazioni di guida
        permessoNotificaBordo = salvataggio.preferenzaSwitch(this,"notifica_al_bordo");
        permessoVibrazione = salvataggio.preferenzaSwitch(this,"vibrate");
        permessoAiuto = salvataggio.preferenzaSwitch(this,"help");

        // bottone per la gestione della partita
        taskButton = (Button) findViewById(R.id.button);

        Log.e("PARTITA SALVATA?", String.valueOf(salvataggio.presenzaPartitaSalvata(this)));
        // se c'è una partita salvata allora la ripristino dal punto in cui era stata interrotta
        if(salvataggio.presenzaPartitaSalvata(this)){
            difficolta = salvataggio.recuperaDifficoltaUltimoLabirinto(this);
            labirinto = salvataggio.recuperaPartita(this);
            labirinto.maze[labirinto.fine.getX()][labirinto.fine.getY()] = 9;

            long controlloTimer = salvataggio.recuperaTimer(this);
            if(controlloTimer==0){
                millisecondi = returnTempo(difficolta);
            } else {
                millisecondi = controlloTimer;
            }
            nuovaPartita=false;
            // recupero le misure precedenti della partita dai dati condivisi con un set di stringhe.
            misure = salvataggio.recuperaMisure(this);
            tempo_accumulato=salvataggio.ottieniPrestazioneLong(this,"tempo_accumulato");
            errore_accumulato=salvataggio.ottieniPrestazioneFloat(this,"errore_accumulato");
        }
        else {
            Log.e(TAG, "COSTRUZIONE LABIRINTO");
            // blocco il pulsante nel caso l'utente prema accidentalmente lo schermo durante la creazione del labirinto.
            taskButton.setEnabled(false);
            (new Thread(findLabirinth)).start();
            // estraggo il labirinto
            labirinto = Labirinto.estraiLabirinto(difficolta,Labirinto.getLimiteVicoli(difficolta),Labirinto.getLimiteLoop(difficolta),0);
            taskButton.setEnabled(true);
            millisecondi = returnTempo(difficolta);

            // estraggo la posizione iniziale
            Log.e(TAG, "IMPOSTO LE COORDINATE INIZIALI");
            Coordinata posizione = labirinto.getInizio();
            xu = posizione.getX();
            yu = posizione.getY();
            labirinto.maze[xu][yu] = 1;

            nuovaPartita=true;

            // imposto alcuni contatori
            conteggioV=0;
            conteggioPassi=0;
            conteggioLoop=0;
            misure = new ArrayList<>();
        }

        // se il labirinto ha un inizio che corrisponde alla fine allora chiudo l'activity
        if(labirinto.maze[xu][yu]==9 || Controllo.stessaCoordinata(labirinto.inizio,labirinto.fine)){
            salvataggio.partitaMemorizzata(this,false);
            finish();
        }

        // creo un timer per la gestione della partita con una durata in base alla difficoltà del labirinto.
        // gestisco con avvisi gli eventi di prossimità alla scadenza del timer e la fine di questo.
        final boolean[] avviso = {true};
        long finalMillisecondi = millisecondi;
        Log.e("DURATA TIMER (MILLIS)", String.valueOf(millisecondi));
        scadenza = new CountDownTimer(finalMillisecondi, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                if(millisUntilFinished<=1000){
                    parla("Tempo scaduto!", TextToSpeech.QUEUE_FLUSH);
                    tempoScaduto=true;
                }
                else if(millisUntilFinished<=60000 && avviso[0]){
                    parla("Ti rimane poco tempo per arrivare alla soluzione", TextToSpeech.QUEUE_ADD);
                    avviso[0] = false;
                }
                else if(millisUntilFinished<=60000){
                    play(3,alarm);
                }
                millisecondiallafine=millisUntilFinished;
            }

            @Override
            public void onFinish() {
                // fine del tempo a disposizione per la partita
                tempoScaduto=true;
                salvataggio.incrementaPrestazione(getApplicationContext(),"fallimenti_diff_"+difficolta);

                tempoImpiegato = returnTempo(difficolta);
                Log.e("T. IMPIEGATO END TIMER",String.valueOf(tempoImpiegato));

                Log.e("FINE TIMER","ENDGAME");
                salvataggio.aggiornaTempoPercorrenza(getApplicationContext(),tempoImpiegato/1000,difficolta);
                salvataggio.partitaMemorizzata(getApplicationContext(),false);

                finish();
            }
        };
        scadenza.start();

        DEBUG(labirinto.maze, labirinto.x, labirinto.y, xu, yu, direzione);

        Log.e("INIZIO PARTITA","TRUE");
        // memorizzo lo stato del gioco appena creato
        salvataggio.memorizzaStatoDelGioco(labirinto,this,millisecondiallafine,difficolta);
        salvataggio.partitaMemorizzata(this,true);
        // se sono a inizio partita allora valuto le vie disponibili nella attuale posizione
        if(start) {
            vie = labirinthManager.vieDisponibili(labirinto, xu, yu, labirinto.fine, permessoAiuto);
        }
        // preparo le liste per il controllo dei loop e delle intersezioni passate
        ArrayList<Coordinata> intersezioniRecenti = new ArrayList<>();
        HashMap<String,Integer> intersezioniPassate = new HashMap<>();
        Log.e("PRE-PULSANTE","DEBUG");
        // gestione del gioco attraverso il bottone sullo schermo.
        taskButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // nel caso in cui non ci sono sensori disponibili per l'orientamento e ho abilitato il tocco,
                // applico la guida in base al tocco sullo schermo dell'utente grazie al quale calcolo l'angolo di orientamento.
                if(!ORIENTAMENTO_TRAMITE_SENSORI && tocco && vincoloTemporale) {
                    toccoX = event.getX();
                    toccoY = event.getY();
                    angoloCorrenteAssoluto = calcoloAngoloTramiteTocco();
                    double angolo = (angoloCorrenteAssoluto + predirection) + 540;
                    angoloCorrente = angolo % 360 - 180; // converto fra 180 e -180 l'angolo corrente rispetto al tocco fatto.
                    angoloSettoreCorrente = angolo % 90 - 45;
                }

                if(event.getAction()==MotionEvent.ACTION_DOWN){
                    tempo=System.currentTimeMillis();
                }
                if(event.getAction()==MotionEvent.ACTION_UP && !tempoScaduto){
                    Log.i("up","intro");
                    // tale delay varia in base a quel che succede nella posizione corrente.
                    // serve per ridurre il più possibile la sovrapposizione fra guida sonora e comunicazioni verbali
                    int delay = 1450;
                    // gestisco la posizione del labirinto nel caso in cui non ha appena cominciato la partita
                    if(! start) {
                        if(vie.size()==1){
                            Log.e(TAG,"VICOLO CIECO");
                            // se mi trovo in un vicolo cieco allora avviso l'utente, inverto la direzione e dico che alla prossima intersezione rimuovo il vicolo cieco
                            avvisoVicoloCieco();
                            labirinthManager.gestioneVicoloCieco(labirinto,xu,yu,direzione);
                            muovi();
                        }
                        else if(vie.size()==2 && Controllo.controlloPercorso(labirinto.maze,xu,yu) && percorso){
                            // se mi trovo in una posizione senza intersezioni (percorso) allora avanzo nella direzione corrente.
                            Log.e(TAG,"PERCORSO");
                            muovi();
                            // DEBUG
                            DEBUG(labirinto.maze, labirinto.x, labirinto.y, xu, yu, direzione);
                        }
                        else if(vie.size()>=2) {
                            // Rimozione del vicolo cieco
                            if(vicoloCieco) {
                                Log.e(TAG, "RIMOZIONE VICOLO CIECO");
                                String frase = "Rimozione del Vicolo Cieco.";
                                parla(frase, TextToSpeech.QUEUE_FLUSH);
                                delay += 600;
                                Log.e("Vicolo", posizioneVicolo.toString());
                                removeVicoloCieco(labirinto, xu, yu, labirinto.fine);
                            }
                            // se mi trovo di nuovo in un vicolo cieco dopo la rimozione del precedente allora preparo la notifica all'utente
                            diNuovoVicoloCieco = vie.size() == 1;
                            // controllo se mi trovo in una intersezione
                            if(vie.size()>1) {
                                gestioneIntersezione(delay, intersezioniRecenti, intersezioniPassate);
                            }
                        }
                    } // inizio del labirinto: in ogni caso viene gestito come se il punto fosse una intersezione.
                    else {
                        Log.e(TAG,"START");
                        // all'inizio del labirinto impongo una intersezione automatica per far decidere all'utente in che direzione andare.
                        // Non viene memorizzato questo punto come una intersezione a meno che non sia già un punto di intersezione.
                        gestioneIntersezione(delay, intersezioniRecenti, intersezioniPassate);
                    }
                }
                return true;
            }
        });
        // imposto un semaforo binario.
        semaforo = new Semaforo(1);
        Log.e("POST-PULSANTE","DEBUG");
    }

    // gestisco l'intersezione, compresa la presenza di loop, inizio intersezione e fine intersezione
    private void gestioneIntersezione(int delay, ArrayList<Coordinata> intersezioniRecenti, HashMap<String, Integer> intersezioniPassate) {
        tempo_inizio = System.currentTimeMillis();
        if(interazione){
            // se è una nuova partita allora aumento il numero di labirinti percorsi nello storico
            if(nuovaPartita) {
                salvataggio.incrementaPrestazione(getApplicationContext(), "labirinti_diff_" + difficolta + "_percorsi");
                nuovaPartita=false;
            }
            // avviso nel caso in cui ci sono già passato
            // devo inserire l'intersezione nella lista e controllare se è un loop
            if(vie.size()>1 && !Controllo.controlloPercorso(labirinto.maze,xu,yu)) {
                delay = labirinthManager.gestioneLoop(delay, intersezioniRecenti, intersezioniPassate, xu, yu);
            }
            // ripristino il conteggio di vicoli ciechi
            conteggioVicolo=true;
            // applico l'interazione nell'intersezione
            if(permessoVibrazione)
                gestioneInizioIntersezione(delay,r.nextInt(14));
            else
                gestioneInizioIntersezione(delay,r.nextInt(13));
        } else {
            // concludo l'intersezione e confermo la direzione appena il pulsante viene abilitato
            if(vincoloTemporale)
                Log.e("INTERSEZIONE","CONFERMA DELLA DIREZIONE");
                gestioneFineIntersezione();
        }
    }

    // notifico la presenza del vicolo cieco.
    private void avvisoVicoloCieco() {
        String frase = "Sei";
        // informo che ti trovi di nuovo in un vicolo cieco se sei già stato in uno nell'avviso precedente
        if(diNuovoVicoloCieco) {
            frase += " di nuovo ";
            diNuovoVicoloCieco=false;
        }
        diNuovoVicoloCieco=true;
        // conto il vicolo cieco in cui è capitato l'utente
        if(conteggioVicolo){
            conteggioV++;
            conteggioVicolo=false;
        }
        frase+="in un Vicolo Cieco, Inverto la tua direzione. Alla prossima intersezione verrà rimosso.";
        parla(frase, TextToSpeech.QUEUE_FLUSH);
        // memorizzo dove c'è il vicolo cieco per una prossima rimozione.
        vicoloCieco=true;
        posizioneVicolo = new Coordinata(xu,yu);
    }

    // rimuovo il vicolo cieco reprecedente non appena mi trovo in una intersezione.
    protected void removeVicoloCieco(Labirinto labirinto, int xu, int yu, Coordinata end) {
        if(posizioneVicolo.getX()==xu){
            // rimuovo gli 1 in orizzontale
            Log.e("Vicolo","rimuovo in orizzontale");
            labirinthManager.remove1Horizontal(labirinto,xu,yu,posizioneVicolo);
        }
        else if(posizioneVicolo.getY()==yu){
            // rimuovo gli 1 in verticale
            Log.e("Vicolo","rimuovo in verticale");
            labirinthManager.remove1Vertical(labirinto,xu,yu,posizioneVicolo);
        }
        // ricalcolo le vie disponibili dopo la rimozione
        posizioneVicolo=null;
        vicoloCieco=false;
        vie = labirinthManager.vieDisponibili(labirinto, xu, yu, end, permessoAiuto);
        DEBUG(labirinto.maze,labirinto.x, labirinto.y,xu,yu,direzione);
    }

    // applico la fine positiva del gioco
    private void endGame() {
        // notifica di vittoria
        String frase = "Complimenti. Hai Vinto!";
        parla(frase, TextToSpeech.QUEUE_FLUSH);
        // tempo impiegato
        tempoImpiegato = returnTempo(difficolta) - millisecondiallafine;
        // memorizzazione delle prestazioni
        salvataggio.incrementaPrestazione(this,"labirinti_diff_"+difficolta+"_conclusi");
        // salvo i dati dell'andamento della partita
        // ... per partita ... :
        // # vicoli ciechi incontrati -> conteggioV
        // # passi fatti -> conteggioPassi
        // # loop incontrati -> conteggioLoop
        // # intersezioni -> misure.size()
        // - Tempo impiegato
        // - errore angolare commesso per ogni istruzione rispetto una direzione (se commesso o no)
        int numeroPartita = salvataggio.ottieniPrestazione(this,"numeroPartita");
        salvataggio.salvaPrestazioniPartita(this,numeroPartita,difficolta,labirinto.caratteristiche,conteggioPassi,conteggioV,conteggioLoop,tempoImpiegato,returnTempo(difficolta),errore_accumulato,tempo_accumulato,misure);
        salvataggio.aggiornaNumeroPartita(this,numeroPartita);
        // imposto la fine effettiva della partita senza possibilità di riprenderla
        Log.e("FINE PARTITA","ENDGAME");
        salvataggio.partitaMemorizzata(this,false);
        // blocco il pulsante
        taskButton.setEnabled(false);
        // gestisco le tempistiche della partita
        Log.e("T. IMPIEGATO END GAME",String.valueOf(tempoImpiegato/1000));
        salvataggio.aggiornaTempoPercorrenza(getApplicationContext(),tempoImpiegato/1000,difficolta);
        scadenza.cancel();
        timer.cancel();
        // dopo qualche secondo chiudo il gioco (l'Activity del Gioco)
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() { finish(); }
        },2500);
    }

    // gestisco l'inizio dell'intersezione.
    protected void gestioneInizioIntersezione(int delay, int i) {
        setOrientamento = angoloCorrenteAssoluto;
        modalita = i;
        tocco=true;
        intersezione(delay);
        interazione = false;
        percorso = false;
    }

    // gestisco la fine dell'intersezione.
    protected void gestioneFineIntersezione() {
        // verifico la scelta della direzione
        direzionePrecedente=direzione;
        boolean sceltaEffettuata = labirinthManager.valutaDirezione(angoloCorrente,labirinto.maze,xu,yu,direzione);
        if (sceltaEffettuata && vincoloTemporale) {
            tocco=false;
            // stop della guida
            timer.cancel();
            silence();
            percorso = true;
            muovi();
            DEBUG(labirinto.maze, labirinto.x, labirinto.y, xu, yu, direzione);
            start=false;
        } else{
            direzione=direzionePrecedente;
            parla("Scegli meglio",TextToSpeech.QUEUE_ADD);
        }
    }

    private void muovi() {
        // applico il movimento nel labirinto
        boolean MOVIMENTO_AVVENUTO = labirinthManager.move(labirinto,xu,yu,direzione);
        // controllo della nuova posizione in caso di movimento avvenuto con successo.
        if(MOVIMENTO_AVVENUTO) {
            play(2,earcon);
            conteggioPassi++;
            // aggiungo le misure delle prestazioni all'intersezione (durata intersezione, errore rispetto alla direzione scelta)
            double ERRORE_ANGOLARE = Math.abs(direzioneAiuto-angoloCorrente);
            long tempoIntersezione = System.currentTimeMillis()-inizioTS;
            errore_accumulato += ERRORE_ANGOLARE;
            tempo_accumulato += tempoIntersezione;
            misure.add(new Measure(tempoIntersezione,String.valueOf(Math.min(ERRORE_ANGOLARE,360-ERRORE_ANGOLARE)),labirinthManager.getCategoriaDistanza(xu,yu,labirinto.fine),modalita).toString());
            salvataggio.aggiornaPrestazioneLong(this,"tempo_accumulato",tempo_accumulato);
            salvataggio.aggiornaPrestazioneFloat(this,"errore_accumulato", errore_accumulato);
            // verifico la nuova posizione
            if(labirinto.maze[xu][yu]==9){
                endGame();
            } else {
                vie = labirinthManager.vieDisponibili(labirinto, xu, yu, labirinto.fine, permessoAiuto);
                Log.e("LUNGHEZZA LISTA", String.valueOf(vie.size()));
                // notifico il tipo della nuova posizione con un suono
                if (vie.size() == 2 && (Controllo.controlloPercorso(labirinto.maze, xu, yu))) {
                    // percorso ... non faccio nulla... notifico con il suono del passo ed avanzo nella direzione impostata
                }
                else if (vie.size() >= 2) {
                    play(1,earcon); // intersezione
                    interazione = true;
                } else if (vie.size() == 1) {
                    play(0,earcon); // vicolo cieco
                }
                // Notifica che sei arrivato sul bordo. (disattivabile dal menù)
                if(permessoNotificaBordo && (xu == labirinto.x-2 || yu == labirinto.y-2 || yu==1 || xu==1)){
                    if(notificaBordo){
                        parla("Ti trovi su un bordo del labirinto",TextToSpeech.QUEUE_ADD);
                        notificaBordo =false;
                    }
                }
                else{
                    notificaBordo =true;
                }
                // memorizzo il nuovo stato della partita
                salvataggio.memorizzaStatoDelGioco(labirinto,getApplicationContext(),millisecondiallafine,difficolta);
            }
        } // se il movimento non è possibile allora continuo a guidare l'utente
        else {
            Log.e("MOVIMENTO","NON AVVENUTO PER DIREZIONE "+direzione);
            direzione=direzionePrecedente;
            parla("Non puoi andare da questa parte!", TextToSpeech.QUEUE_ADD);
            interazione = true;
        }
    }

    // gestisco l'intersezione
    protected void intersezione(int delay) {
        Log.e(TAG,"INTERSEZIONE");
        // preparo i dati per le performance
        inizioTS = System.currentTimeMillis();
        // genero un semaforo
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if(semaforo.getISemaphore()<=0)
                    semaforo.up();
            }
        },1,10); // PRIMA LO AVEVO A 2: adesso lo imposto a 10 per evitare che alcuni telefoni vadano in distorsione
        vincoloTemporale = false;
        // ritardo la creazione della guida all'orientamento per evitare il più possibile la sovrapposizione tra guida e comunicazioni vocali
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                continueRun = true;
                // creo una guida con una modalità scelta a caso, una direzione di aiuto, la presenza di un segnale acuto e le vie permmesse percorribili
                modality = audioFactory.getModality(modalita, getApplicationContext(), direzioneAiuto, angoliPermessi);
                if (modality != null) (new Thread(playModality)).start();
            }
        }, delay);
        // notifico la presenza di N vie percorribili dalla posizione attuale
        int nVie = vie.size();
        String frase= nVie==1 ? "Individua l'unica direzione disponibile":"Scegli una direzione fra le" + nVie + "disponibili";
        // notifico la presenza di un loop nel caso in cui l'utente sia già stato nell'attuale posizione
        if(loop || multiLoop){
            frase += ", Sei già stato qui";
        }
        if(multiLoop){
            frase += " almeno tre volte";
        }
        if(loop || multiLoop){
            frase += ", fai attenzione";
            loop=false;
            multiLoop=false;
        }
        // genero un timer per evitare che l'utente confermi una direzione prima che la guida inizi o la comunicazione vocale finisca
        new CountDownTimer(delay, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {}
            @Override
            public void onFinish() {
                vincoloTemporale=true;
            }
        }.start();
        // istruzione vocale
        parla(frase ,TextToSpeech.QUEUE_FLUSH);
    }

    // riproduco feedback basato su earcon
    private void play(int p, MediaPlayer mediaPlayer) {
        try {
            mediaPlayer = MediaPlayer.create(getApplicationContext(),getEarcon(p));
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                }
            });
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                    mp = null;
                }
            });
        }
        catch (IllegalStateException e){e.printStackTrace();}
        catch (NullPointerException e){Log.e("EARCON", "NULL eccezione");}
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_GAME_ROTATION_VECTOR && ORIENTAMENTO_TRAMITE_SENSORI) {
            mSensorManager.getRotationMatrixFromVector(K, event.values);             // ottengo la matrice di rotazione dal vettore event.values
            mSensorManager.getOrientation(K, orientation);                           // ottengo l'orientamento dalla matrice di rotazione
            angoloCorrenteAssoluto = (orientation[0] * 180 / Math.PI);               // converto in gradi la rotazione sull'asse 0
            // Per trovarmi nella direzione in cui sono devo raddrizzare il dispositivo.
            // aggiungo predirection che corrisponde all'angolo di direzione di percorrenza attuale del labirinto
            double angolo = (angoloCorrenteAssoluto - setOrientamento + predirection) + 540;
            angoloCorrente = angolo % 360 - 180; // converto fra 180 e -180 l'angolo corrente rispetto l'orientamento dei sensori.
            angoloSettoreCorrente = angolo % 90 - 45;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {}

    @Override
    public void onInit(int status) {
        if(status == TextToSpeech.SUCCESS)
            speaker.setLanguage(Locale.ITALY);
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        try {
            mSensorManager.registerListener(this, mRotationVector, SensorManager.SENSOR_DELAY_FASTEST);
        } catch (NullPointerException e){;}
        silence();
        interazione=true;
        // imposto l'assistente vocale
        speaker = new TextToSpeech(this,this);
        speaker.setSpeechRate(2);
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause");
        try{
            mSensorManager.unregisterListener(this);
        } catch (NullPointerException e){;}
        silence();
        if(timer!=null) timer.cancel();
        if(scadenza!=null) scadenza.cancel();
        silence();
        salvataggio.salvaMisure(this,misure);
        super.onPause();
    }

    @Override
    public void onStop() {
        speaker.shutdown();
        Log.d(TAG, "onStop");
        if(timer!=null) timer.cancel();
        if(scadenza!=null) scadenza.cancel();
        silence();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        speaker.shutdown();
        Log.d(TAG, "onDestroy");
        if(timer!=null) timer.cancel();
        if(scadenza!=null) scadenza.cancel();
        silence();
        super.onDestroy();
    }

    // ritorno l'earcon per la notifica di una posizione o azione
    private int getEarcon(int parametroS) {
        if(parametroS==0){
            // vicolo cieco
            return R.raw.vc;
        } else if(parametroS==1){
            // intersezione
            return R.raw.ping_1;
        } else if(parametroS==2){
            // cammino
            return R.raw.step2;
        }
        else if(parametroS==3){
            // cammino
            return R.raw.warningbell;
        }
        return parametroS;
    }

    // metodo per bloccare l'inizio del gioco in caso si stia ancora generando un labirinto
    private Runnable findLabirinth = new Runnable() {
        @Override
        public void run() {
            while (!labirintoTrovato) {
                Log.e("FINDLABIRINTH","CICLO");
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            Log.e("LABIRINTO","TROVATO");
            parla("Labirinto Trovato, puoi iniziare",TextToSpeech.QUEUE_ADD);
        }
    };

    // DEBUG per conoscere il numero di volte che viene applicata la guida sonora
    // long contatoreRunModality=0;
    // modalità di guida
    private Runnable playModality = new Runnable() {
        @Override
        public void run() {
            /*
            // DEBUG per conoscere il numero di volte che viene applicata la guida sonora
            Timer tm = new Timer();
            tm.scheduleAtFixedRate(new TimerTask(){
                @Override
                public void run() {
                    Log.e("modality eseguita", String.valueOf(contatoreRunModality));
                    contatoreRunModality=0;
                }
            },1,1000);
            */
            while (continueRun) {
                // contatoreRunModality++;
                // applico la guida
                if(modality!=null && continueRun) {
                    modality.applyMode(angoloSettoreCorrente,angoloCorrente);
                    semaforo.down();
                }
                // se passano più di 30 sec dall'inizio della guida allora la fermo per evitare che prosegua in modo indefinito
                if(System.currentTimeMillis()-inizioTS>60000){
                    silence();
                    interazione=true;
                    parla("Riprova",0);
                }
            }
            // tm.cancel();
            // silenzio la guida
            silence();
        }
    };

    // silenzio la guida
    private void silence() {
        Log.e("SILENCE","SILENZIO");
        continueRun=false;
        if(modality!=null) modality.setSilence();
        modality = null;
    }

    // metodo per comunicare messaggi vocali all'utente
    protected static void parla(String stringa, int mode){
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                int speak = speaker.speak(stringa, mode, null, null);
            } else {
                speaker.speak(stringa, mode, null);
            }
        } catch (NullPointerException e){;}
    }

    // DEBUG del labirinto
    protected void DEBUG(int[][] maze, int x, int y, int xu, int yu, char direzione) {
        for(int i=0 ; i < x ; i++) {
            StringBuilder riga = new StringBuilder();
            for(int j=0 ; j < y ; j++) {
                if(i==xu && j==yu) {
                    riga.append("X");
                } else {
                    riga.append(maze[i][j]);
                }
            }
            Log.e("lab", riga.toString());
        }
        Log.e("POSIZIONE",new Coordinata(xu,yu).toString());
        labirinthManager.getDirezione(direzione);
    }

    // restituisce il tempo in base alla difficoltà
    protected static long returnTempo(String difficolta){
        switch (difficolta){
            case "facile":
                return 600000;
            case "media":
                return 1200000;
            case "difficile":
                return 1800000;
            case "nessuna":
                return 1200000;
        }
        return 1200000;
    }

    // calcola l'angolo di tocco sullo schermo rispetto al centro.
    private double calcoloAngoloTramiteTocco() {
        double ab = 50;
        double ac = Math.pow(toccoX-widthHalfScreen,2)+Math.pow(toccoY-heightHalfScreen,2);
        double bc = Math.pow(toccoX-widthHalfScreen,2)+Math.pow(toccoY-heightHalfScreen-50,2);
        double degree = Math.acos((Math.pow(ab,2)+ac-bc)/(2*ab*Math.sqrt(ac)));
        angoloCorrenteAssoluto = 180 - (degree * 180 / Math.PI);
        return toccoX<widthHalfScreen? angoloCorrenteAssoluto*-1 : angoloCorrenteAssoluto;
    }
}