package com.example.labiorient;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.Locale;

public class MainActivity extends Activity implements TextToSpeech.OnInitListener {

    private final static String TAG = MainActivity.class.getCanonicalName();
    public static TextToSpeech speaker;
    public Button rigioca;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // richiedo i permessi di lettura e scrittura sullo storage interno ed esterno
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 101);
            }
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 101);
            }
        }

        // imposto l'assistente vocale
        speaker = new TextToSpeech(this,this);
        speaker.setSpeechRate(2);

        // menù interattivo
        Button gioca = findViewById(R.id.play);
        rigioca = findViewById(R.id.restart_play);
        Button prestazioni = findViewById(R.id.getPrestazioni);
        Button impostazioni = findViewById(R.id.setImpostazioni);
        Button tutorial = findViewById(R.id.tutorial);

        // contesto e oggetto per scrittura e lettura preferenze salvate
        Salvataggio salvataggio = new Salvataggio();
        Context context = this;

        // controllo se c'è qualche partita salvata. se c'è rendo visibile e utilizzabile il pultante per riprendere la partita
        Log.e("PARTITA SALVATA?", String.valueOf(salvataggio.presenzaPartitaSalvata(context)));
        if(salvataggio.presenzaPartitaSalvata(context)){
            rigioca.setCursorVisible(true);
            rigioca.setVisibility(View.VISIBLE);
            rigioca.setFocusable(true);
        } else {
            rigioca.setCursorVisible(false);
            rigioca.setVisibility(View.INVISIBLE);
            rigioca.setFocusable(false);
        }

        // GIOCA UNA NUOVA PARTITA
        gioca.setOnClickListener(v -> {
            if(salvataggio.presenzaPartitaSalvata(context)){
                // incremento il numero di sospensioni.
                salvataggio.incrementaPrestazione(context,"sospensione");
            }
            Log.e("GIOCA","CLICCATO");
            salvataggio.partitaMemorizzata(context,false);
            startActivity(new Intent(context, Gioco.class));
        });

        // RIPRENDI UNA PARTITA SOSPESA
        rigioca.setOnClickListener(v -> {
            if(salvataggio.presenzaPartitaSalvata(context))
                startActivity(new Intent(context, Gioco.class));
            else
                speaker.speak("Non c'è nessuna partita sospesa!",TextToSpeech.QUEUE_FLUSH,null);
        });

        // pulsante per impostare parametri di gioco e dell'applicazione
        impostazioni.setOnClickListener(v -> {
            startActivity(new Intent(context, SettingsActivity.class));
        });

        // pulsante per mostrare le prestazioni dell'utente globali
        prestazioni.setOnClickListener(v -> {
            startActivity(new Intent(context, ElencoPrestazioni.class));
        });

        // pulsante per ottenere spiegazioni sull'uso dell'applicazione
        tutorial.setOnClickListener(v -> {
            startActivity(new Intent(context, TutorialActivity.class));
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume");
        Salvataggio salvataggio = new Salvataggio();
        Log.e("PARTITA SALVATA?", String.valueOf(salvataggio.presenzaPartitaSalvata(this)));
        if(salvataggio.presenzaPartitaSalvata(this)){
            rigioca.setCursorVisible(true);
            rigioca.setVisibility(View.VISIBLE);
            rigioca.setFocusable(true);
        } else {
            rigioca.setCursorVisible(false);
            rigioca.setVisibility(View.INVISIBLE);
            rigioca.setFocusable(false);
        }
    }


    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        speaker.shutdown();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        speaker.shutdown();
        super.onDestroy();
    }

    @Override
    public void onInit(int status) {
        if(status == TextToSpeech.SUCCESS)
            speaker.setLanguage(Locale.ITALY);
    }
}
