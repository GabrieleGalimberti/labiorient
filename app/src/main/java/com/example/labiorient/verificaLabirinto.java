package com.example.labiorient;

import java.util.ArrayList;
import java.util.List;

class verificaLabirinto {

    // verifico se esiste un path dall'inizio alla fine
	static boolean pathExists(int[][] matrix, Coordinata inizio, int lungx, int lungy) {
        // mantengo una coda di coordinate per seguire vari percorsi
        List<Coordinata> queue = new ArrayList<>();
        queue.add(inizio);
        boolean pathExists = false;
        while(!queue.isEmpty()) {
            // prendo la coordinata e controllo che sia il punto di fine
            Coordinata current = queue.remove(0);
            if(matrix[current.x][current.y] == 9) {
                pathExists = true;
                break;
            }
            // marco la coordinata come visitata
            matrix[current.x][current.y] = 0;
            // ritorno nella coda i vicini alla coordinata
            List<Coordinata> neighbors = getNeighbors(matrix, current, lungx, lungy);
            queue.addAll(neighbors);
        }
        
        return pathExists;
    }
    // ritorno le altre coordinate percorribili da una coordinata
    private static List<Coordinata> getNeighbors(int[][] matrix, Coordinata c, int lungx, int lungy) {
        List<Coordinata> neighbors = new ArrayList<>();
        if(isValidPoint(matrix, c.x - 1, c.y, lungx, lungy)) {
            neighbors.add(new Coordinata(c.x - 1, c.y));
        }
        if(isValidPoint(matrix, c.x + 1, c.y, lungx, lungy)) {
            neighbors.add(new Coordinata(c.x + 1, c.y));
        }
        if(isValidPoint(matrix, c.x, c.y - 1, lungx, lungy)) {
            neighbors.add(new Coordinata(c.x, c.y - 1));
        }
        if(isValidPoint(matrix, c.x, c.y + 1, lungx, lungy)) {
            neighbors.add(new Coordinata(c.x, c.y + 1));
        }
        return neighbors;
    }
    // controllo che il punto nel labirinto sia valido e percorribile
    private static boolean isValidPoint(int[][] matrix, int x, int y, int lungx, int lungy) {
        return !(x < 0 || x >= lungx || y < 0 || y >= lungy) && (matrix[x][y] != 0);
    }
}
