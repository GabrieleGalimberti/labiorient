package com.example.labiorient;

import android.os.Build;
import android.os.VibrationEffect;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by gabriele_galimberti on 15/04/18.
 */

class VibrationModality extends Modality {

    private int a=1, b=14;

    public VibrationModality(double direzioneAiuto, ArrayList<Double> angoliPermessi) {
        super(angoliPermessi,direzioneAiuto);
        Log.e("VIBRATION MODALITY","START");
    }

    @Override
    public void applyMode(double angoloSettoreCorrente, double angoloCorrente) {

        diff = calcoloAngolo(a1,angoloCorrente,angoloSettoreCorrente);

        if (diff <= tolleranza) {
            if (non_ha_già_fatto_ping) {
                if (Build.VERSION.SDK_INT >= 26) {
                    Gioco.v.vibrate(VibrationEffect.createOneShot(300, VibrationEffect.DEFAULT_AMPLITUDE));
                } else {
                    Gioco.v.vibrate(300);
                }
                non_ha_già_fatto_ping = false;
            }
        } else if (diff > tolleranza && diff < Math.abs(a1)) {
            non_ha_già_fatto_ping = true;
        }
        if(aiutoGuida) zeroCrossingHelp(angoloCorrente);
    }

}
