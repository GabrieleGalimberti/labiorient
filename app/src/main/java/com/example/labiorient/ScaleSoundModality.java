package com.example.labiorient;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by gabriele_galimberti on 11/04/18.
 */

class ScaleSoundModality extends Modality {

    private double frammento,frammento2,frammento3,
            frammento4,frammento5, frammento6, frammento7;
    private boolean ha_gia_suonato1 = true, ha_gia_suonato2 = true,
            ha_gia_suonato3 = true, ha_gia_suonato4 = true, ha_gia_suonato5 = true,
            ha_gia_suonato6 = true, ha_gia_suonato7 = true, ha_gia_suonato8 = true;
    private MediaPlayer mp1=null, mp2=null, mp3=null,
            mp4=null, mp5=null, mp6=null, mp7=null, mp8=null;

    public ScaleSoundModality(Context applicationContext, double direzioneAiuto, boolean makePing, ArrayList<Double> angoliPermessi) {
        super(angoliPermessi,direzioneAiuto);
        mp1 = new MediaPlayer();
        mp1 = MediaPlayer.create(applicationContext, R.raw.c1);
        mp2 = new MediaPlayer();
        mp2 = MediaPlayer.create(applicationContext, R.raw.d1);
        mp3 = new MediaPlayer();
        mp3 = MediaPlayer.create(applicationContext, R.raw.e1);
        mp4 = new MediaPlayer();
        mp4 = MediaPlayer.create(applicationContext, R.raw.f1);
        mp5 = new MediaPlayer();
        mp5 = MediaPlayer.create(applicationContext, R.raw.g1);
        mp6 = new MediaPlayer();
        mp6 = MediaPlayer.create(applicationContext, R.raw.a1);
        mp7 = new MediaPlayer();
        mp7 = MediaPlayer.create(applicationContext, R.raw.b1);
        mp8 = new MediaPlayer();
        if(makePing)
            mp8 = MediaPlayer.create(applicationContext, R.raw.c2ping);
        else
            mp8 = MediaPlayer.create(applicationContext, R.raw.c2);
        setAngoloPrecedente(a1);
        frammento = Math.abs(a1 / 7);       // divido l'angolo target per 7 che sono pari al numero di note della scala prima della nota fondamentale
        calcoloFrammenti();                 // che identifica l'orientamento target. ogni frammento corrisponde ad un intervallo di distanza.
        this.makePing = makePing;

        Log.e("SCALE SOUND MODALITY","START");
    }

    private void calcoloFrammenti() {
        frammento2 = frammento * 2;
        frammento3 = frammento * 3;
        frammento4 = frammento * 4;
        frammento5 = frammento * 5;
        frammento6 = frammento * 6;
        frammento7 = frammento * 7;
    }

    @Override
    public void applyMode(double angoloSettoreCorrente, double angoloCorrente) {
        int distanza = (int) calcoloAngolo(a1,angoloCorrente,angoloSettoreCorrente);
        // dichiaro una variabile distanza pari all'errore rispetto all'angolo target
        try{
            scalaMusicale(distanza);
            if(makePing) zeroCrossing(diff, mp8);
            if(aiutoGuida) zeroCrossingHelp(angoloCorrente,mp8);
        }
        catch (IllegalStateException ie){
            ie.printStackTrace();
        }
    }

    private void scalaMusicale(int distanza) {
        if( distanza <= tolleranza){
            C2();
            //osc.frequency.set(523.3);
            //Log.i("NOTA","DO ALTO");
        } else if (distanza == 0 ) {
            ha_gia_suonato1 = true;
        } else if (distanza <= frammento7 && distanza >= frammento6) {
            C1();
            //osc.frequency.set(261.6);
            //Log.i("NOTA", "DO BASSO");
        } else if (distanza <= frammento6 && distanza >= frammento5) {
            D1();
            //osc.frequency.set(294.3);
            //Log.i("NOTA", "RE");
        } else if (distanza <= frammento5 && distanza >= frammento4) {
            E1();
            //osc.frequency.set(327);
            //Log.i("NOTA", "MI");
        } else if (distanza <= frammento4 && distanza >= frammento3) {
            F1();
            //osc.frequency.set(348.8);
            //Log.i("NOTA", "FA");
        } else if (distanza <= frammento3 && distanza >= frammento2) {
            G1();
            //osc.frequency.set(392.4);
            //Log.i("NOTA", "SOL");
        } else if (distanza <= frammento2 && distanza >= frammento) {
            A1();
            //osc.frequency.set(436);
            //Log.i("NOTA", "LA");
        } else if (distanza <= frammento && distanza > 1) {
            B1();
            //osc.frequency.set(490.5);
            //Log.i("NOTA", "SI");
        } else{ ha_gia_suonato1 = true; }
    }

    private void C2() {
        ha_gia_suonato1 = true;
        ha_gia_suonato2 = true;
        ha_gia_suonato3 = true;
        ha_gia_suonato4 = true;
        ha_gia_suonato5 = true;
        ha_gia_suonato6 = true;
        ha_gia_suonato7 = true;
        if(ha_gia_suonato8){
            mp8.start();
            ha_gia_suonato8 = false;
        }
    }

    private void B1() {
        ha_gia_suonato1 = true;
        ha_gia_suonato2 = true;
        ha_gia_suonato3 = true;
        ha_gia_suonato4 = true;
        ha_gia_suonato5 = true;
        ha_gia_suonato6 = true;
        ha_gia_suonato8 = true;
        if(ha_gia_suonato7){
            mp7.start();
            ha_gia_suonato7 = false;
        }
    }

    private void A1() {
        ha_gia_suonato1 = true;
        ha_gia_suonato2 = true;
        ha_gia_suonato3 = true;
        ha_gia_suonato4 = true;
        ha_gia_suonato5 = true;
        ha_gia_suonato7 = true;
        ha_gia_suonato8 = true;
        if(ha_gia_suonato6){
            mp6.start();
            ha_gia_suonato6 = false;
        }
    }

    private void G1() {
        ha_gia_suonato1 = true;
        ha_gia_suonato2 = true;
        ha_gia_suonato3 = true;
        ha_gia_suonato4 = true;
        ha_gia_suonato6 = true;
        ha_gia_suonato7 = true;
        ha_gia_suonato8 = true;
        if(ha_gia_suonato5){
            mp5.start();
            ha_gia_suonato5 = false;
        }
    }

    private void F1() {
        ha_gia_suonato1 = true;
        ha_gia_suonato2 = true;
        ha_gia_suonato3 = true;
        ha_gia_suonato5 = true;
        ha_gia_suonato6 = true;
        ha_gia_suonato7 = true;
        ha_gia_suonato8 = true;
        if(ha_gia_suonato4){
            mp4.start();
            ha_gia_suonato4 = false;
        }
    }

    private void E1() {
        ha_gia_suonato1 = true;
        ha_gia_suonato2 = true;
        ha_gia_suonato4 = true;
        ha_gia_suonato5 = true;
        ha_gia_suonato6 = true;
        ha_gia_suonato7 = true;
        ha_gia_suonato8 = true;
        if(ha_gia_suonato3){
            mp3.start();
            ha_gia_suonato3 = false;
        }
    }

    private void D1() {
        ha_gia_suonato1 = true;
        ha_gia_suonato3 = true;
        ha_gia_suonato4 = true;
        ha_gia_suonato5 = true;
        ha_gia_suonato6 = true;
        ha_gia_suonato7 = true;
        ha_gia_suonato8 = true;
        if(ha_gia_suonato2){
            mp2.start();
            ha_gia_suonato2 = false;
        }
    }

    private void C1() {
        ha_gia_suonato2 = true;
        ha_gia_suonato3 = true;
        ha_gia_suonato4 = true;
        ha_gia_suonato5 = true;
        ha_gia_suonato6 = true;
        ha_gia_suonato7 = true;
        ha_gia_suonato8 = true;
        if(ha_gia_suonato1){
            mp1.start();
            ha_gia_suonato1 = false;
        }
    }

    /*
    private void scalaTemperata() {
        if(a1 != a2) {
            s = Math.abs(a1 / (int) Math.abs(a1 - a2)) - 1;
            Log.i("nota scala", String.valueOf(s));
        }
        else{ s=12; }
        //osc.frequency.set(Math.pow(2,s/12.0) * 261.6);
    }
    */

    @Override
    public void setSilence() {
        ha_gia_suonato1 = true;
        ha_gia_suonato2 = true;
        ha_gia_suonato3 = true;
        ha_gia_suonato4 = true;
        ha_gia_suonato5 = true;
        ha_gia_suonato6 = true;
        ha_gia_suonato7 = true;
        ha_gia_suonato8 = true;
        if (mp1 != null && mp2 != null && mp3 != null && mp4 != null && mp5 != null && mp6 != null && mp7 != null && mp8 != null ) {
            try {
                /*
                mp1.stop();
                mp2.stop();
                mp3.stop();
                mp4.stop();
                mp5.stop();
                mp6.stop();
                mp7.stop();
                mp8.stop();
                */
                mp1.release();
                mp2.release();
                mp3.release();
                mp4.release();
                mp5.release();
                mp6.release();
                mp7.release();
                mp8.release();
            }
            catch (IllegalStateException e){;}
            catch (NullPointerException e){;}
            mp1 = null;
            mp2 = null;
            mp3 = null;
            mp4 = null;
            mp5 = null;
            mp6 = null;
            mp7 = null;
            mp8 = null;
        }
        super.setSilence();
        Log.d("SCALESOUND","Silence!");
    }

}
