package com.example.labiorient;

public class Controllo {

    // verifica che la posizione corrente sia una posizione senza intersezioni o vicoli ciechi
    protected static boolean controlloPercorso(int[][] maze, int x, int y) {
        return ((maze[x + 1][y] != 0 && maze[x - 1][y] != 0 && maze[x][y + 1] == 0 && maze[x][y - 1] == 0) || (maze[x + 1][y] == 0 && maze[x - 1][y] == 0 && maze[x][y + 1] != 0 && maze[x][y - 1] != 0));
    }

    // controllo se il numero di 1 attorno alla cella (i,j) è uguale alla tolleranza
    protected static boolean controlloCella(int[][] matrice, int i, int j, int tolleranza) {
        int somma = Math.min(matrice[i][j+1],1)+Math.min(matrice[i][j-1],1)+Math.min(matrice[i+1][j],1)+Math.min(matrice[i-1][j],1);
        return somma==tolleranza;
    }

    // verifico che mi trovo in una intersezione
    protected static boolean controlloIntersezione(int[][] matrice, int i, int j) {
        return (Math.min(matrice[i][j+1],1)+Math.min(matrice[i][j-1],1)+Math.min(matrice[i+1][j],1)+Math.min(matrice[i-1][j],1))>=2;
    }

    // metodo per scoprire la presenza di un loop
    protected static boolean controlloLoop(int[][] matrice, int q, int u, int x, int y){
        // controllo a destra
        int dx=0;
        for (int g=u+1; g<y-1; g++){
            if(matrice[q][g]!=0){
                if(controlloIntersezione(matrice,q,g) && !controlloPercorso(matrice,q,g)){
                    dx=g;
                    break;
                }
            } else {
                return false;
            }
        }
        // controllo verso il basso.
        int giu=0;
        for (int g=q+1; g<x-1; g++){
            if(matrice[g][u]!=0){
                if(controlloIntersezione(matrice,g,u) && !controlloPercorso(matrice,g,u)){
                    giu=g;
                    break;
                }
            } else {
                return false;
            }
        }
        // da Coordinata destra controllo verso il basso
        Coordinata destraSotto1 = null;
        for (int g = q + 1; g < x - 1; g++) {
            if (matrice[g][dx] != 0) {
                if (controlloIntersezione(matrice, g, dx) && !controlloPercorso(matrice, g, dx)) {
                    destraSotto1 = new Coordinata(g, dx);
                    break;
                }
            } else {
                return false;
            }
        }
        // da Coordinata in basso controllo verso destra
        Coordinata destraSotto2 = null;
        for (int g = u + 1; g < y - 1; g++) {
            if (matrice[giu][g] != 0) {
                if (controlloIntersezione(matrice, giu, g) && !controlloPercorso(matrice, giu, g)) {
                    destraSotto2 = new Coordinata(giu, g);
                    break;
                }
            } else {
                return false;
            }
        }
        return destraSotto1 != null && destraSotto2 != null && destraSotto1.getX() == destraSotto2.getX() && destraSotto1.getY() == destraSotto2.getY();
    }

    public static boolean stessaCoordinata(Coordinata init, Coordinata end) {
        return init.getX()==end.getX() && init.getY()==end.getY();
    }
}
