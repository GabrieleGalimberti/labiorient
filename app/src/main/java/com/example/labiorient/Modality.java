package com.example.labiorient;

import android.media.MediaPlayer;
import android.os.Build;
import android.os.VibrationEffect;
import android.util.Log;

import com.jsyn.JSyn;
import com.jsyn.Synthesizer;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.LinkedList;

import jsyn.devices.android.JSynAndroidAudioDevice;

/**
 * Created by gabriele_galimberti on 02/04/18.
 */

public abstract class Modality {

    protected LinkedList<Double> movingAvgDistance = null;
    private static final int MOVING_AVG_DISTANCE_LENGTH = 10;
    protected Synthesizer synthesizer;
    protected boolean makePing = false, non_ha_già_fatto_ping = true;
    private double denominatoreLogaritmico = Math.log(2);
    protected double gamma=4;
    protected int tolleranza = 1;
    protected double angoloPrecedente = 0;
    protected int a1=45;
    protected double diff;
    protected double angoloHelp = 0;
    protected  boolean aa, ai, ad, as, aiutoGuida, vibrazioneGuida;

    public Modality(ArrayList<Double> angoliPermessi, double angoloTarget){
        synthesizer = JSyn.createSynthesizer(new JSynAndroidAudioDevice());
        movingAvgDistance = new LinkedList<Double>();
        aa=false;
        ai=false;
        ad=false;
        as=false;
        setPossibleDirection(angoliPermessi);
        angoloHelp=angoloTarget;
        aiutoGuida=Gioco.permessoAiuto;
        vibrazioneGuida=Gioco.permessoVibrazione;
    }

    void applyMode(double angoloSettoreCorrente,double angoloCorrente){;}

    void setSilence(){
        try {
            movingAvgDistance.clear();
        } catch (NullPointerException e){
            e.printStackTrace();
        }
        movingAvgDistance=null;
    }

    double angleDiff(double a1, double a2){
        // 90 perchè sonifico ogni possibile direzione singolarmente (+-45°)
        double diff = getDiff(a1, a2,90);
        addDistance(diff);
        return avgLinkedList();
    }

    protected double getDiff(double a1, double a2, int max) {
        double diff;
        diff = Math.abs(a1-a2);
        diff = Math.min(diff,max-diff);
        return diff;
    }

    void zeroCrossing(double angoloCorrente, MediaPlayer mp) {
        if(diff < 1 && non_ha_già_fatto_ping){
            mp.start();
            Log.i("zerocross ang corrente",String.valueOf(angoloCorrente));
            non_ha_già_fatto_ping = false;

            if(vibrazioneGuida) {
                if (Build.VERSION.SDK_INT >= 26) {
                    Gioco.v.vibrate(VibrationEffect.createOneShot(200, VibrationEffect.DEFAULT_AMPLITUDE));
                } else {
                    Gioco.v.vibrate(200);
                }
            }
        }
    }

    void zeroCrossingHelp(double angoloCorrente, MediaPlayer mp) {
        if(vibrazioneGuida && getDiff(angoloHelp, angoloCorrente,360) < 1){
            mp.start();
            Log.i("zerocross ang aiuto",String.valueOf(angoloCorrente));

            if (Build.VERSION.SDK_INT >= 26) {
                Gioco.v.vibrate(VibrationEffect.createOneShot(300, VibrationEffect.DEFAULT_AMPLITUDE));
            } else {
                Gioco.v.vibrate(300);
            }
        }
    }

    void zeroCrossingHelp(double angoloCorrente) {
        if(vibrazioneGuida && getDiff(angoloHelp, angoloCorrente,360) < 1){
            if (Build.VERSION.SDK_INT >= 26) {
                Gioco.v.vibrate(VibrationEffect.createOneShot(300, VibrationEffect.DEFAULT_AMPLITUDE));
            } else {
                Gioco.v.vibrate(300);
            }
        }
    }

    void setAngoloPrecedente(int a1){
        if(a1>0)
            angoloPrecedente = a1 - 1;
        else
            angoloPrecedente = a1 + 1;
    }

    void addDistance(double newDegree){
        if (movingAvgDistance.size() == MOVING_AVG_DISTANCE_LENGTH)
            movingAvgDistance.remove();  // se la lista linkata è piena allora rimuovo il primo elemento, quindi applico FIFO
        movingAvgDistance.add(newDegree);
    }

    private double avgLinkedList() {
        try {
            double sum = 0;
            if (movingAvgDistance.size() != 0) {
                for (double d : movingAvgDistance) {
                    sum += d;
                }  // sommo tutte le misurazioni
                return sum / movingAvgDistance.size();
            }
        } catch (ConcurrentModificationException e){
            e.printStackTrace();
        } catch (NullPointerException e){
            e.printStackTrace();
        }
        return 45;
    }

    void setPossibleDirection(ArrayList<Double> angoliPermessi){
        for (double angolo : angoliPermessi) {
            if(angolo==0){
                aa=true;
            } else if(angolo==-90){
                as=true;
            } else if(angolo==90){
                ad=true;
            } else if(angolo==180){
                ai=true;
            }
        }
    }

    protected boolean permessoAvanti(double angolo){
        return aa && Math.abs(angolo)<=45;
    }

    protected boolean permessoIndietro(double angolo){
        return ai && Math.abs(angolo)>=135;
    }

    protected boolean permessoDS(boolean permesso, double angolo, int bordoDX, int bordoSX){
        return permesso && angolo<bordoDX && angolo>bordoSX;
    }

    public double calcoloAngolo(double a1, double angoloCorrente, double angoloSettoreCorrente){
        if(permessoAvanti(angoloCorrente)){
            return diff = angleDiff(a1, angoloSettoreCorrente);
        } else if(permessoDS(ad,angoloCorrente,135,45)){
            return diff = angleDiff(a1, angoloSettoreCorrente);
        } else if(permessoIndietro(angoloCorrente)){
            return diff = angleDiff(a1, angoloSettoreCorrente);
        } else if(permessoDS(as,angoloCorrente,-45,-135)){
            return diff = angleDiff(a1, angoloSettoreCorrente);
        }
        return 45;
    }

    public double setIntermittenzaLineare(double a1, double diff) {
        return Math.max(1-Math.abs(diff/a1),0);
    }

    public double setIntermittenzaEsponenziale(double a1, double diff){
        return Math.pow(1-Math.min(Math.abs(diff/45),1),gamma);
    }

    public double setIntermittenzaLogaritmica(double a1, double diff) {
        return (1 - Math.min(Math.log(1+Math.abs(diff/a1))/denominatoreLogaritmico , 1));
    }

    public double setIntermittenzaNormale(double a1, double diff) {
        return Math.abs(valoreNormale(a1, diff));     // normale standard con media = 0 e varianza = 5
    }

    public double valoreNormale(double a1, double diff){
        double valore =  (Math.exp(-(Math.pow(diff,2))/2*5) / Math.sqrt(2*Math.PI*5) / 0.2); // / 0.2 serve a scalare il valore estratto
        Log.i("valore Normale",Double.toString(valore));
        return valore;
    }
}
