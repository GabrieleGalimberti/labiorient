package com.example.labiorient;

import android.speech.tts.TextToSpeech;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.LinkedList;
import java.util.List;

public class TutorialActivity extends AppCompatActivity {

    public TextToSpeech sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        setupActionBar();

        ListView listViewTutorial = (ListView) findViewById(R.id.elenco_tutorial);
        List list = new LinkedList();

        list.add(new Tutorial("Cos'è Labiorient? ", " Labiorient è un gioco che consiste nella " +
                "risoluzione di labirinti mediante istruzioni audio e tattili."));

        list.add(new Tutorial("Cosa serve lo \"Storico delle prestazioni\"? ", " Serve a mostrare " +
                "una panoramica delle prestazioni generali di gioco passate."));

        list.add(new Tutorial("Che impostazioni posso cambiare con il pulsante \"Impostazioni\"? ", " Con tale " +
                "pulsante si può cambiare la difficoltà, vari aiuti e impostare disattivare e attivare feedback"));

        list.add(new Tutorial("Che livelli di difficoltà ci sono? ", " Ci sono 3 livelli di difficoltà, " +
                "facile, media e difficile. Inoltre c'è un livello chiamato nessuna che restituisce la " +
                "difficoltà che più si adatta al tuo gioco in base a partite precedenti."));

        list.add(new Tutorial("Come avvio una partita? ", " Si gioca una partita premendo " +
                "sul pulsante \"Gioca una nuova partita\" oppure, se hai già una partita in sospeso, " +
                "premendo \"Riprendi il gioco\"."));

        list.add(new Tutorial("Come si gioca? ", " Si gioca per mezzo di un " +
                "pulsante con cui è possibile svolgere tutte le funzioni del gioco. Il pulsante permette " +
                "di esplorare il labirinto step by step. Ogni nuova posizione " +
                "viene valutata a seconda che sia un vicolo cieco, una strada dritta " +
                "o una intersezione. In una intersezione viene data una istruzione di " +
                "navigazione con la quale mi muovo nel labirinto."));

        list.add(new Tutorial("Cosa fare prima e durante una istruzione? ","Prima di una istruzione " +
                "notificata da un suono acuto occorre tenere il dispositivo diritto come se fossi arrivato dritto in un punto. " +
                "Durante una istruzione, se si dispone di sensori che percepiscono la rotazione del dispositivo, " +
                "si può ruotare il disposivo per cercare una via percorribile. In tale caso viene prodotto " +
                "feedback audio e tattile a seconda dell'angolo e questo varia a seconda della vicinanza verso " +
                "una via. Se non si dispone di tali sensori allora si può orientarsi " +
                "tenendo premuto costantemente lo schermo. Il feedback varia a seconda delle coordinate " +
                "di dove tengo premuto lo schermo che vengono usate per calcolare l'angolo rispetto al centro " +
                "dello schermo. Lo schermo è paragonabile ad un radar."));

        list.add(new Tutorial("Come finisce una intersezione?","Nel caso della rotazione del " +
                "dispositivo basta premere il pulsante nella direzione desiderata che permette di muoversi " +
                "in quella direzione. Nel caso del tocco basta invece rilasciare il pulsante nel punto dello schermo " +
                "desiderato. Alla fine di una intersezione avviene un movimento della posizione attuale nel labirinto."));

        list.add(new Tutorial("Cosa succede se non capito in una intersezione? "," Se si arriva a un vicolo " +
                "cieco allora viene notificata la presenza con un suono cupo, viene invertita la direzione e viene rimosso alla prossima intersezione. " +
                "Se sono su una via diritta non ho notifiche e posso proseguire diritto premendo sullo schermo."));

        list.add(new Tutorial("Cosa faccio se non capito più volte in una intersezione? ", " " +
                "Se si gira in tondo viene notificato il fatto di essere già stato in quel punto."));

        list.add(new Tutorial("Cosa succede alla fine di un labirinto? ", " " +
                "Se si arriva alla fine mi viene notificata la vittoria e si torna al menù principale."));

        TutorialAdapter adapter = new TutorialAdapter(this, R.layout.row, list);
        listViewTutorial.setAdapter(adapter);

    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
