package com.example.labiorient;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;

import com.jsyn.unitgen.FilterLowPass;
import com.jsyn.unitgen.LineOut;
import com.jsyn.unitgen.PinkNoise;

import java.util.ArrayList;

/**
 * Created by gabriele_galimberti on 12/04/18.
 */

class ResonanceFilterModality extends Modality {

    PinkNoise pinkNoise;
    LineOut lineOut;
    FilterLowPass filterLowPass;

    public MediaPlayer mp = null;
    private boolean non_ha_già_fatto_ping = true;

    public ResonanceFilterModality(Context applicationContext, double direzioneAiuto, boolean makePing, ArrayList<Double> angoliPermessi){
        super(angoliPermessi,direzioneAiuto);
        synthesizer.add(lineOut = new LineOut());
        synthesizer.start();
        lineOut.start();

        filterLowPass = new FilterLowPass();
        synthesizer.add(filterLowPass);
        pinkNoise = new PinkNoise();
        synthesizer.add(pinkNoise);

        filterLowPass.frequency.setMaximum(10000);
        filterLowPass.frequency.setMinimum(0);
        filterLowPass.frequency.set(700);
        pinkNoise.output.connect(filterLowPass.input);

        filterLowPass.amplitude.set(0.8);
        pinkNoise.amplitude.set(0.52);

        filterLowPass.output.connect(0,lineOut.input,0);
        filterLowPass.output.connect(0,lineOut.input,1);

        this.makePing = makePing;
        mp = new MediaPlayer();
        mp = MediaPlayer.create(applicationContext, R.raw.ping_2);

        Log.e("RESONANCE FILT MODALITY","START");
    }

    @Override
    public void applyMode(double angoloSettoreCorrente, double angoloCorrente) {
        diff = calcoloAngolo(a1,angoloCorrente,angoloSettoreCorrente);

        if (diff <= tolleranza) {
            filterLowPass.Q.set(1000);  // faccio fischiare il filtro sull'angolo target con tolleranza di 2 gradi e ampiezza scalata
            filterLowPass.amplitude.set(0.52);
            if (non_ha_già_fatto_ping) {
                if(makePing)
                    mp.start();
                non_ha_già_fatto_ping = false;
            }
        } else {
            filterLowPass.Q.set(180 * setIntermittenzaEsponenziale(a1,diff)); // col variare della distanza aumento il fattore di qualità del filtro, ma ne controllo l'ampiezza
            filterLowPass.amplitude.set(0.8 / Math.sqrt(diff));
            non_ha_già_fatto_ping = true;
        }
        if(makePing) {
            zeroCrossing(diff, mp);
        }
        if(aiutoGuida) FilterHelp(angoloCorrente);
    }

    // col variare della distanza aumento il fattore di qualità del filtro e controllo l'ampiezza
    private void FilterHelp(double angoloCorrente) {
        if(getDiff(angoloHelp, angoloCorrente,360) < 1){
            filterLowPass.Q.set(2000);
            filterLowPass.amplitude.set(0.8);
        } else {
            filterLowPass.Q.set(1000);
            filterLowPass.amplitude.set(0.52);
        }
    }

    @Override
    public void setSilence() {
        try {
            filterLowPass.amplitude.set(0);
            pinkNoise.amplitude.set(0);
            synthesizer.stop();
            pinkNoise.stop();
            filterLowPass.stop();
            lineOut.stop();
            filterLowPass = null;
            pinkNoise = null;
            if(mp != null){
                mp.stop();
                mp.release();
                mp = null;
            }
        } catch (NullPointerException e){
            e.printStackTrace();
        } catch (IllegalStateException e){
            e.printStackTrace();
        }
        super.setSilence();

        Log.d("RESONANCE FILT MODALITY","Silence!");
    }
}