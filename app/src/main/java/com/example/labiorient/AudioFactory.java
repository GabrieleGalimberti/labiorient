package com.example.labiorient;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by gabriele_galimberti on 02/04/18.
 */

public class AudioFactory { // creo un Pattern Factory per impostare una modalità di guida con sonificazione e vibrazione in rapporto alla rotazione del dispositivo o del tocco sullo schermo.

    public Modality getModality(int mod, Context applicationContext, double direzioneAiuto, ArrayList<Double> angoliPermessi) {
        if(mod == 0)
            return new PingModality(applicationContext, direzioneAiuto, angoliPermessi);
        else if(mod == 1)
            return new DiscreteIntermittenceModality(applicationContext, direzioneAiuto,false, angoliPermessi);
        else if(mod == 2)
            return new DiscreteIntermittenceModality(applicationContext, direzioneAiuto,true, angoliPermessi);
        else if(mod == 3)
            return new ScaleSoundModality(applicationContext, direzioneAiuto,false, angoliPermessi);
        else if(mod == 4)
            return new ScaleSoundModality(applicationContext, direzioneAiuto,true, angoliPermessi);
        else if(mod == 5)
            return new ContinuousIntermittenceModality(applicationContext, direzioneAiuto,false, angoliPermessi);
        else if(mod == 6)
            return new ContinuousIntermittenceModality(applicationContext, direzioneAiuto,true, angoliPermessi);
        else if(mod == 7)
            return new BeatsModality(applicationContext, direzioneAiuto,false, angoliPermessi);
        else if(mod == 8)
            return new ResonanceFilterModality(applicationContext, direzioneAiuto,false, angoliPermessi);
        else if(mod == 9)
            return new TailPingModality(applicationContext, direzioneAiuto, angoliPermessi);
        else if(mod == 10)
            return new TargetSoundModality(applicationContext, direzioneAiuto, angoliPermessi);
        else if(mod == 11)
            return new AmplitudeModulation(direzioneAiuto, angoliPermessi);
        else if(mod == 12)
            return new GoDownModulation(applicationContext, direzioneAiuto, angoliPermessi);
        else if(mod == 13)
            return new VibrationModality(direzioneAiuto, angoliPermessi);
        /*
        else if(mod == 14)
            return new StereoSound(applicationContext, direzioneAiuto, angoliPermessi); // TODO applicare stereo.
        */
        /*
        else if(angoliPermessi.size()==1){
            Random r = new Random();
            return this.getModality(r.nextInt(12), applicationContext, direzioneAiuto, makePing, angoliPermessi);
        }*/
        return null;
    }
}
