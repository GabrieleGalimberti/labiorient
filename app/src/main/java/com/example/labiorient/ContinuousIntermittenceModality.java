package com.example.labiorient;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;

import com.jsyn.unitgen.LineOut;
import com.jsyn.unitgen.SineOscillator;

import java.util.ArrayList;

/**
 * Created by gabriele_galimberti on 06/04/18.
 */

class ContinuousIntermittenceModality extends Modality {

    // RING MODULATION

    SineOscillator oscillator1, oscillator2;
    LineOut lineOut;

    private int a = 1, b = 14;

    public MediaPlayer mp = null;

    public ContinuousIntermittenceModality(Context applicationContext, double direzioneAiuto, boolean makePing, ArrayList<Double> angoliPermessi) {
        super(angoliPermessi,direzioneAiuto);
        synthesizer.add(oscillator1 = new SineOscillator());
        synthesizer.add(oscillator2 = new SineOscillator());

        synthesizer.add(lineOut = new LineOut());

        oscillator1.output.connect(0, lineOut.input, 0);
        oscillator1.output.connect(0, lineOut.input, 1);
        oscillator1.frequency.set(440);

        oscillator2.output.connect(oscillator1.amplitude);
        oscillator2.frequency.set(1);

        setVolume(0.60);

        synthesizer.start();
        lineOut.start();

        this.makePing = makePing;
        mp = new MediaPlayer();
        mp = MediaPlayer.create(applicationContext, R.raw.ping_2);
        setAngoloPrecedente(a1);
        gamma = 4;

        Log.e("DISCR INTERMIT MODALITY","START");
    }

    @Override
    public void applyMode(double angoloSettoreCorrente, double angoloCorrente) {

        diff = calcoloAngolo(a1,angoloCorrente,angoloSettoreCorrente);

        if (diff <= tolleranza) {
            if (non_ha_già_fatto_ping) {
                oscillator2.frequency.set(b + a);
                if(makePing)
                    mp.start();
                non_ha_già_fatto_ping = false;

            }
        } else if (diff > tolleranza && diff < Math.abs(a1)){
            double minimo = setIntermittenzaEsponenziale(a1, diff);
            oscillator2.frequency.set(b * (minimo) + a);
            non_ha_già_fatto_ping = true;
        }
        if(makePing) zeroCrossing(diff, mp);
        if(aiutoGuida) FrequencyHelp(angoloCorrente,oscillator2);
    }

    private void FrequencyHelp(double angoloCorrente, SineOscillator oscillator2) {
        if(getDiff(angoloHelp, angoloCorrente,360) < 1){
            oscillator2.frequency.set(b * 20 + a);
        }
    }

    @Override
    public void setSilence() {
        try {
            setVolume(0);
            synthesizer.stop();
            lineOut.stop();
            super.setSilence();
            oscillator1=null;
            oscillator2=null;
            synthesizer=null;
            if(mp != null){
                mp.stop();
                mp.release();
                mp = null;
            }
        }
        catch (NullPointerException e){;}
        catch (IllegalStateException e){;}
        Log.d("CIM", "Silence!");
    }

    private void setVolume(double i) {
        oscillator1.amplitude.set(i);
        oscillator2.amplitude.set(i);
    }

}