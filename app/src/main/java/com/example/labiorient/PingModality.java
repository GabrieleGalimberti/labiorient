package com.example.labiorient;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by gabriele_galimberti on 04/04/18.
 */

class PingModality extends Modality {

    public MediaPlayer mp = null;

    public PingModality(Context applicationContext, double direzioneAiuto, ArrayList<Double> angoliPermessi) {
        super(angoliPermessi,direzioneAiuto);
        mp = new MediaPlayer();
        mp = MediaPlayer.create(applicationContext, R.raw.ping_2);
        setAngoloPrecedente(a1);
    }

    @Override
    public void applyMode(double angoloSettoreCorrente, double angoloCorrente) {

        diff = calcoloAngolo(a1,angoloCorrente,angoloSettoreCorrente);

        if (diff <= 20 && diff > tolleranza) {
            non_ha_già_fatto_ping = true;
        } else if (diff <= tolleranza) {
            if (non_ha_già_fatto_ping) {
                mp.start();
                non_ha_già_fatto_ping = false;
            }
        }
        zeroCrossing(diff, mp);
        if(aiutoGuida) zeroCrossingHelp(angoloCorrente,mp);
    }

    @Override
    public void setSilence() {
        super.setSilence();
        if(mp != null){
            try{
                mp.release();
            }
            catch (IllegalStateException e){;}
            catch (NullPointerException e){;}
            mp = null;
        }
        Log.d("PING MODALITY","Silence!");
    }

}