package com.example.labiorient;

import java.util.ArrayList;
import java.util.Random;

// Un altro algoritmo per la generazione dei labirinti (molto più basilare). NON UTILIZZATO NELL'APPLICAZIONE
public class Algoritmo {
	
	public static Random r = new Random();
	public static int x=0, intersezioni = 0, y=0;//x=r.nextInt(10), y=r.nextInt(10);
	public static ArrayList<Coordinata> coordinate = null, puntiDislocati = null;
	
	public static void main(String args[]) {
		
		generaLabirinto( r.nextInt(3)-1 , r.nextInt(3)-1 );
		
	}

	public static int[][] generaLabirinto(int parametro1 , int parametro2) {
		// genero le variabili che definiscono la dimensione e il numero di 1 inseriti a caso nel labirinto
		int precedente=-1;
		x = parametro1;
		y = parametro2;
		intersezioni = x*4;
		
		coordinate = new ArrayList<Coordinata>();
		
		// Creo il labirinto
		int [][] labirinto = new int[x][y];
		for(int i=0 ; i < x ; i++) {
			for(int j=0 ; j < y ; j++) {
				inserisciZero(labirinto, i, j);
			}
		}
		
		// genero la partenza e l'arrivo.
		int finex=r.nextInt(x-1)+1;
		int finey=r.nextInt(y-1)+1;
		int iniziox= r.nextInt(x-1)+1;
		int inizioy= r.nextInt(y-1)+1;
		coordinate.add(new Coordinata(iniziox,inizioy));
		coordinate.add(new Coordinata(finex,finey));
		
		// inserisco i punti nel labirinto
		inserimentoIntersezioni(r, precedente, labirinto);

		do { 
			// 1 per ogni coppia all'interno di una arraylist
			// 2 prendo una coppia di 1 qualsiasi
			// 3 ne calcolo la distanza x e y
			// 4 collego gli uno
			// 5 gestisco delle possibili intersezioni particolari
			int index = r.nextInt(coordinate.size());
			Coordinata uno = coordinate.get(index);
			coordinate.remove(index);
			Coordinata due = null;
			if(coordinate.size()!=0) {
				index = r.nextInt(coordinate.size());
				due = coordinate.get(index);
				coordinate.remove(index);
				collegaUno(labirinto, uno, due);
			} else {
				collegaUno(labirinto, uno, new Coordinata(r.nextInt(x),r.nextInt(y)));
			}
			inserisciIntersezioniParticolari(labirinto);
			rimuovi1duplicati(labirinto);
		}while(coordinate.size()>0);
		
		// inserisco il punto di inizio e fine
		labirinto[iniziox][inizioy]=8;
		labirinto[finex][finey]=9;
		
		// trovo i punti dislocati (punti con tutti 0 attorno)
		// collego i punti dislocati con un 1 qualsiasi.
		puntiDislocati=new ArrayList<Coordinata>();
		trovaPuntiDislocati(labirinto);
		int nDislocati = puntiDislocati.size();
		for(int m=0 ; m<nDislocati ; m++) {
			collegaUno(labirinto, puntiDislocati.get(0), trovaUnoNonInPuntiDislocati(labirinto));
			puntiDislocati.remove(0);
		}
		
		// System.out.println("COORDINATE MANCANTI");
		// stampaCoordinate(coordinate);
		// stampaCoordinate(puntiDislocati);
		
		// gestisco aree di 1 (tre righe di 1 adiacenti inserendo  
		for(int i=1 ; i < x-1 ; i++) {
			for(int j=1 ; j < y-1 ; j++) {
				if(eccezione4(labirinto, i, j)) {
					inserisciZero(labirinto, i, j);
				}
				if(j<y-4 && i<x-4 && eccezione5(labirinto, i, j)) {
					inserisciZero(labirinto, i+1, j+2);
				}
			}
		}
		// inserisco il punto di inizio e fine
		labirinto[iniziox][inizioy]=8;
		labirinto[finex][finey]=9;;
		
		// se l'inizio e la fine non sono collegate ad almeno un uno qualsiasi allora le collego
		collegaInizioFine(labirinto, iniziox, inizioy, finex, finey);
		
		// evito che ci siano righe fatte da solo 1
		evitaRigheDiSoli1(labirinto);
		
		// stampo il labirinto con eventuali duplicati
		printLabirinto(labirinto);
		System.out.println();
		
		// cencello i duplicati
		rimuovi1duplicati(labirinto);
		
		inserisciIntersezioniParticolari(labirinto);
		
		rimuoviQuadriDi1(labirinto);
		
		rimuoviIncertezza(labirinto);
		
		collegaInizioNelCasoNonLoSia(labirinto, iniziox , inizioy);
		
		System.out.println();
		
		// verifico che esista almeno un percorso nel labirinto che parta 
		// dall'inizio e arrivi alla fine.
		int [][] matrice = new int[x][y];
		for(int i=0 ; i < Algoritmo.x ; i++) {
			for(int j=0 ; j < Algoritmo.y ; j++) {
				matrice[i][j]=labirinto[i][j];
			}
		}
		boolean verifica = verificaLabirinto.pathExists(matrice, new Coordinata(iniziox,inizioy), x, y );
		
		// stampo il labirinto 
		printLabirinto(labirinto);
		
		if(verifica) {
			System.out.println("Il labirinto prevede un percorso dall'inizio alla fine!\n");
			return labirinto;
		} else {
			System.out.println("Il labirinto NON prevede un percorso dall'inizio alla fine!\n");
			return generaLabirinto( parametro1 , parametro2 );
		}
	}


	private static void collegaInizioNelCasoNonLoSia(int[][] labirinto, int iniziox, int inizioy) {
		if(eccezione3(labirinto, iniziox , inizioy, 0)) {
			
		}
	}

	private static void rimuoviIncertezza(int[][] labirinto) {
		for(int i=0 ; i < x-1; i++) {
			for(int j=0 ; j < y-1 ; j++) {
				if(labirinto[i][j]!=0 && labirinto[i][j+1]==0 && labirinto[i+1][j]==0 && labirinto[i+1][j+1]!=0) {
					inserisciUno(labirinto, i, j+1);
				} else if(labirinto[i][j]==0 && labirinto[i][j+1]!=0 && labirinto[i+1][j]!=0 && labirinto[i+1][j+1]==0){
					inserisciUno(labirinto, i+1, j+1);
				}
			}
		}
	}

	private static void rimuoviQuadriDi1(int[][] labirinto) {
		for(int i=2 ; i < x-2; i++) {
			for(int j=2 ; j < y-2 ; j++) {
				if(labirinto[i][j]==1 && labirinto[i][j+1]==0) {
					if(labirinto[i+1][j]==1 && labirinto[i+1][j+1]==1 && labirinto[i+1][j+2]==0 ) {
						inserisciZero(labirinto, i+1, j+1);
					}
				}
				if(labirinto[i][j]==1 && labirinto[i][j-1]==0) {
					if(labirinto[i-1][j]==1 && labirinto[i-1][j+1]==1 && labirinto[i-1][j+2]==0 ) {
						inserisciZero(labirinto, i-1, j+1);
					}
				}
				if(labirinto[i][j]==0 && labirinto[i][j+1]==1) {
					if(labirinto[i+1][j]==1 && labirinto[i+1][j-1]==1 && labirinto[i+1][j-2]==0 ) {
						inserisciZero(labirinto, i+1, j-1);
					}
				}
				if(labirinto[i][j]==0 && labirinto[i][j-1]==1) {
					if(labirinto[i-1][j]==1 && labirinto[i-1][j-1]==1 && labirinto[i-1][j-2]==0 ) {
						inserisciZero(labirinto, i-1, j-1);
					}
				}
			}
		}
		
	}

/*
	private static void cancellaIsole(int[][] labirinto, int fine) {
		int [][] A = labirinto;
		for(int i=0 ; i < x ; i++) {
			for(int j=0 ; j < y ; j++) {
				if(labirinto[i][j]!=0) {
					if(!verificaLabirinto.pathExists(A, new Coordinata(i,j) , new Coordinata(fine,y-1), x, y ))
						inserisciZero(labirinto, i, j);
				}
			}
		}
	}
*/
	
	
	private static void rimuovi1duplicati(int[][] labirinto) {
		for(int i=0 ; i < x-3 ; i++) {
			for(int j=0 ; j < y-3 ; j++) {
				// 101*    101*
				// 0110    0010
				// .... -->....
				// 0110    0010
				// **1*    **1*
				if(labirinto[i][j+2]==1) {
					int contatore=0;
					while(true) {
						if(i+contatore+1<x && (labirinto[i+1+contatore][j]==0 && labirinto[i+1+contatore][j+1]==1 
								&& labirinto[i+1+contatore][j+2]==1 && labirinto[i+1+contatore][j+3]==0)) {
							contatore++;
						} else {
							break;
						}
					}
					if(i+1+contatore<x && labirinto[i+contatore+1][j+2]==1) {
						for(int l=0 ; l < contatore ; l++) {
							System.out.println("1 sono arrivato a x="+(i+l)+" y="+(j+1));
							inserisciZero(labirinto, i+l+1, j+1);
						}
					}
				}
				// 110*    110*
				// 0110    0100
				// .... -->....
				// 0110    0100
				// *1**    *1**
				if(labirinto[i][j+1]==1) {
					int contatore=0;
					while(true) {
						if(i+contatore+1<x && (labirinto[i+1+contatore][j]==0 && labirinto[i+1+contatore][j+1]==1 
								&& labirinto[i+1+contatore][j+2]==1 && labirinto[i+1+contatore][j+3]==0)) {
							contatore++;
						} else {
							break;
						}
					}
					if(i+1+contatore<x && labirinto[i+contatore+1][j+1]==1) {
						for(int l=0 ; l < contatore ; l++) {
							System.out.println("2 sono arrivato a x="+(i+l)+" y="+(j+1));
							inserisciZero(labirinto, i+l+1, j+2);
						}
					}			
				}
				// 100.01     100.01
				// 111.11 --> 111.11
				// 011.10     000.0*
				// 100.01     100.0*
				
				// 100.01     100.01
				// 011.11 --> 000.01
				// 111.10     111.1*
				// 100.01     100.0*
				if(labirinto[i][j]==1 && (labirinto[i+1][j]==1 || labirinto[i+2][j]==1)) {
					int contatore=0;
					while(true) {
						if(j+contatore+1<y-1 && (labirinto[i][j+1+contatore]==0 && labirinto[i+1][j+1+contatore]==1 
								&& labirinto[i+2][j+1+contatore]==1 && labirinto[i+3][j+1+contatore]==0)) {
							contatore++;
						} else {
							break;
						}
					}
					if(j+1+contatore<y-1 && labirinto[i+1][j+contatore+1]!=0 && labirinto[i+1][j]==1) {
						for(int l=0 ; l < contatore ; l++) {
							System.out.println("3 sono arrivato a x="+(i+1)+" y="+(j+l));
							inserisciZero(labirinto, i+1, j+l+1);
						}
					}
					else if(j+1+contatore<y-1 && labirinto[i+2][j+contatore+1]!=0 && labirinto[i+2][j]==1) {
						for(int l=0 ; l < contatore ; l++) {
							System.out.println("4 sono arrivato a x="+(i+1)+" y="+(j+l));
							inserisciZero(labirinto, i+2, j+l+1);
						}
					}
				}
				// 000.01     000.01
				// 111.11 --> 111.11
				// 111.10     100.0*
				// 100.01     100.0*
				
				// 000.01     000.01
				// 011.1* --> 000.0*
				// 111.11     111.11
				// 100.01     100.0*
				if(labirinto[i][j]==0 && (labirinto[i+1][j]==1 || labirinto[i+2][j]==1)) {
					int contatore=0;
					while(true) {
						if(j+contatore+1<y-1 && (labirinto[i][j+1+contatore]==0 && labirinto[i+1][j+1+contatore]==1 
								&& labirinto[i+2][j+1+contatore]==1 && labirinto[i+3][j+1+contatore]==0)) {
							contatore++;
						} else {
							break;
						}
					}
					if(j+1+contatore<y-1 && labirinto[i+1][j+contatore+1]!=0 && labirinto[i+1][j]!=0) {
						for(int l=0 ; l < contatore ; l++) {
							System.out.println("5 sono arrivato a x="+(i+1)+" y="+(j+l));
							inserisciZero(labirinto, i+1, j+l+1);
						}
					}
					else if(j+1+contatore<y-2 && labirinto[i+1][j+contatore+2]!=0 && labirinto[i+2][j]!=0) {
						for(int l=0 ; l < contatore ; l++) {
							System.out.println("6 sono arrivato a x="+(i+1)+" y="+(j+l));
							inserisciZero(labirinto, i+2, j+l+1);
						}
					}
				}
				
				// gestione bordi
				
			}
		}
	}


	// 11111111...111111 evito le righe che siano costituite da solo 1
	public static void evitaRigheDiSoli1(int[][] labirinto) {
		for(int i=0 ; i < x ; i++) {
			int somma=0;
			for(int j=0 ; j < y ; j++) {
				if(labirinto[i][j]==1) {
					somma+=1;
				}
			}
			try {
				if(somma >= y-2 && i%2==0) {
					inserisciZero(labirinto, i, y/2);
					if(i<y-1) { 
						inserisciUno(labirinto, i+1, y/2);
						inserisciUno(labirinto, i+1, y/2-1);
						inserisciUno(labirinto, i+1, y/2+1);
					}
				}
			} catch(ArrayIndexOutOfBoundsException e) {;}
		}
	}


	//  110
	//  111
	//  110
	public static boolean eccezione1(int[][] labirinto, int i, int j) {
		boolean sopra = labirinto[i-1][j-1]==1 && labirinto[i-1][j]==1 && labirinto[i-1][j+1]==0 , 
		centro = labirinto[i][j-1]==1 && labirinto[i][j+1]==1 && labirinto[i][j]==1 ,
		sotto = labirinto[i+1][j-1]==1 && labirinto[i+1][j]==1 && labirinto[i+1][j+1]==0 ;
		return sopra && centro && sotto;
	}

	//  011
	//  111
	//  011
	public static boolean eccezione2(int[][] labirinto, int i, int j) {
		boolean sopra = labirinto[i-1][j-1]==0 && labirinto[i-1][j]==1 && labirinto[i-1][j+1]==1 , 
		centro = labirinto[i][j-1]==1 && labirinto[i][j+1]==1 && labirinto[i][j]==1 ,
		sotto = labirinto[i+1][j-1]==0 && labirinto[i+1][j]==1 && labirinto[i+1][j+1]==1 ;
		return sopra && centro && sotto;
	}
	
	//  *1*
	//  111
	//  *1*
	public static boolean eccezione3(int[][] labirinto, int i, int j, int tolleranza) {
		return (labirinto[i][j+1]+labirinto[i][j-1]+labirinto[i+1][j]+labirinto[i-1][j])==tolleranza;
	}
	
	//  111
	//  111
	//  111
	public static boolean eccezione4(int[][] labirinto, int i, int j) {
		boolean sopra = labirinto[i-1][j-1]==1 && labirinto[i-1][j]==1 && labirinto[i-1][j+1]==1 , 
		centro = labirinto[i][j-1]==1 && labirinto[i][j+1]==1 && labirinto[i][j]==1 ,
		sotto = labirinto[i+1][j-1]==1 && labirinto[i+1][j]==1 && labirinto[i+1][j+1]==1 ;
		return sopra && centro && sotto;
	}
	
	//  11111
	//  10101
	//  11111
	private static boolean eccezione5(int[][] labirinto, int i, int j) {
		boolean sopra = labirinto[i][j-1]==1 && labirinto[i][j]==1 && labirinto[i][j+1]==1 && labirinto[i][j+2]==1 && labirinto[i][j+3]==1, 
				centro = labirinto[i+1][j-1]==1 && labirinto[i+1][j]==0 && labirinto[i+1][j+1]==1 && labirinto[i+1][j+2]==0 && labirinto[i+1][j+3]==1,
				sotto = labirinto[i+2][j-1]==1 && labirinto[i+2][j]==1 && labirinto[i+2][j+1]==1 && labirinto[i+2][j+2]==1 && labirinto[i+2][j+3]==1;
		return sopra && centro && sotto;
	}
	
	//  111
	//  101
	//  111
	//  101
	//  111
	private static boolean eccezione6(int[][] labirinto, int i, int j) {
		boolean a = labirinto[i-4][j-2]==1 && labirinto[i-4][j-1]==1 && labirinto[i-4][j]==1, 
				b = labirinto[i-3][j-2]==1 && labirinto[i-3][j-1]==0 && labirinto[i-3][j]==1,
				c = labirinto[i-2][j-2]==1 && labirinto[i-2][j-1]==1 && labirinto[i-2][j]==1,
				d = labirinto[i-1][j-2]==1 && labirinto[i-1][j-1]==0 && labirinto[i-1][j]==1,
				e = labirinto[i][j-2]==1 && labirinto[i][j-1]==1 && labirinto[i][j]==1;
		return a && b && c && d && e;
	}

	//  1*1
	//  ***
	//  1*1
	public static boolean eccezione7(int[][] labirinto, int i, int j, int tolleranza) {
		return (labirinto[i+1][j+1]+labirinto[i+1][j-1]+labirinto[i+1][j-1]+labirinto[i-1][j-1])==tolleranza;
	}

	// cerco un punto del labirinto che non sia uno 0 o un punto dislocato nella lista.
	private static Coordinata trovaUnoNonInPuntiDislocati(int[][] labirinto) {
		int g = r.nextInt(x), l=r.nextInt(y);
		Coordinata nuova = new Coordinata(g,l);
		boolean b=false;
		if( labirinto[g][l]==1 ) {
			for(int k=0 ; k<puntiDislocati.size(); k++) {
				if(nuova.diversa(puntiDislocati.get(k))){
					b=true;
					break;
				}	
			}
			if(!b) {
				return nuova;
			}
			else {
				return trovaUnoNonInPuntiDislocati(labirinto); 
			}
		} else {
			return nuova;
		}
	}


	// cerco gli 1 dislocati
	private static void trovaPuntiDislocati(int[][] labirinto) {
		for(int i=0 ; i < x ; i++) {
			for(int j=0 ; j < y ; j++) {
				try {
					boolean angoli = angleLeftUp(j, i) || (i==0 && j==y-1) || ( i==x-1 && j==0) || angleRightDown(i, j);
					// bordo in alto
					if(i==0 && !angoli) {
						boolean c = labirinto[i+1][j]==0 && labirinto[i][j+1]==0 && labirinto[i][j-1]==0 && labirinto[i+1][j+1]==0 && labirinto[i+1][j-1]==0 && labirinto[i][j]==1;
						if( c ) {
							puntiDislocati.add(new Coordinata(i,j));
						}
					} //bordo in basso
					else if(i==x-1 && !angoli){
						boolean c = labirinto[i-1][j]==0 && labirinto[i][j-1]==0 && labirinto[i][j+1]==0 && labirinto[i-1][j+1]==0 && labirinto[i-1][j-1]==0 && labirinto[i][j]==1;
						if( c ) {
							puntiDislocati.add(new Coordinata(i,j));
						}
					} // bordo a sinistra
					else if(j==0 && !angoli){
						boolean c = labirinto[i-1][j]==0 && labirinto[i+1][j]==0 && labirinto[i][j+1]==0 && labirinto[i+1][j+1]==0 && labirinto[i-1][j+1]==0 && labirinto[i][j]==1;
						if( c ) {
							puntiDislocati.add(new Coordinata(i,j));
						}
					} // bordo a destra
					else if(j==y-1 && !angoli){
						boolean c = labirinto[i-1][j]==0 && labirinto[i+1][j]==0 && labirinto[i][j-1]==0 && labirinto[i-1][j-1]==0 && labirinto[i+1][j-1]==0 && labirinto[i][j]==1;
						if( c ) {
							puntiDislocati.add(new Coordinata(i,j));
						}
					} // gestione punti isolati sul centro
					//else if(angoli){
					//	puntiDislocati.add(new Coordinata(i,j));
					//}
					else if(!angoli){
						boolean sopra = labirinto[i-1][j-1]==0 && labirinto[i-1][j]==0 && labirinto[i-1][j+1]==0 , 
								centro = labirinto[i][j-1]==0 && labirinto[i][j+1]==0 && labirinto[i][j]==1 ,
								sotto = labirinto[i+1][j-1]==0 && labirinto[i+1][j]==0 && labirinto[i+1][j+1]==0 ;
						if( sopra && centro && sotto ) {
							puntiDislocati.add(new Coordinata(i,j));
						}
					}
				} catch(ArrayIndexOutOfBoundsException e) {
					e.printStackTrace();
				}
			}
		}
	}


	// se la finex e l'inizio non sono collegati allora li collego.
	private static void collegaInizioFine(int labirinto [][], int iniziox, int inizioy, int finex, int finey) {
		
		if(labirinto[iniziox][inizioy]==0 || (iniziox<x-1 && labirinto[iniziox+1][inizioy]!=1) || (iniziox>0 && labirinto[iniziox-1][inizioy]!=1)) {
			int p = individua1vicinoDX(labirinto,iniziox,2);
			collegaUno(labirinto, new Coordinata(iniziox,0), new Coordinata(iniziox,p));
		}
		if(labirinto[finex][finey-1]==0 || (finex<x-1 && labirinto[finex+1][finey]!=1) || (finex>0 && labirinto[finex-1][finey]!=1)) {
			int p = individua1vicinoSX(labirinto,finex,finey-2);
			collegaUno(labirinto, new Coordinata(finex,p), new Coordinata(finex,finey));
		}
	}

	private static int individua1vicinoSX(int[][] labirinto, int fine, int j) {
		if(j<=0) {
			return 0;
		}
		if(labirinto[fine][j]==1 && j>3) {
			return j;
		} else {
			if(j>3)
				individua1vicinoSX(labirinto, fine, j-1);
		}
		return y/3-1;
	}



	private static int individua1vicinoDX(int[][] labirinto, int inizio, int j) {
		if(labirinto[inizio][j]==1 && j<y-3) {
			return j;
		} else {
			if(j<y-3)
				individua1vicinoDX(labirinto, inizio, j+1);
		}
		return y*1/3-1;
	}

	// trasformo in 1 le caselle all'interno delle distanze x e y in modo da definire un percorso
	public static void collegaUno(int[][] labirinto, Coordinata uno, Coordinata due) {
		int distanzax = distanza(due.getX(), uno.getX());
		int distanzay = distanza(due.getY(), uno.getY());
		for(int m=0 ; m<Math.abs(distanzay); m++) {
			if(distanzay < 0) {
				inserisciUno(labirinto, uno.getX(),due.getY()+m);
			}
			else if(distanzay > 0){
				inserisciUno(labirinto,uno.getX(),uno.getY()+m);
			}
		}
		for(int m=0 ; m<Math.abs(distanzax); m++) {
			if(distanzax < 0) {
				inserisciUno(labirinto, due.getX()+m, uno.getY());
			}
			else if(distanzax > 0){
				inserisciUno(labirinto,uno.getX()+m,uno.getY());
			}
		}
	}

	public static void printLabirinto(int[][] labirinto) {
		for(int i=0 ; i < x ; i++) {
			for(int j=0 ; j < y ; j++) {
				System.out.printf("%3s", labirinto[i][j]);
			}
			System.out.println();
		}
		System.out.println();
	}

	public static void inserimentoIntersezioni(Random r, int precedente, int[][] labirinto) {
		for(int j=0 ; j < intersezioni ; j=j+2) {
			setUnoByX(labirinto, r, r.nextInt(y-1)+1, precedente);
			setUnoByX(labirinto, r, r.nextInt(y-1)+1, precedente);
		}
		
		for(int j=0 ; j < x ; j++) {
			boolean soloZero = true;
			for(int t=0 ; t < y ; t++) {
				if(labirinto[j][t]==1)
					soloZero=false;
			}
			if(!soloZero){
				setUnoByY(labirinto, r, j);
			}
		}
		if(coordinate.size()%2==1) {
			setUnoByY(labirinto, r, r.nextInt(y-1)+1);
		}
	}
	
	private static void setUnoByX(int[][] labirinto, Random r, int j, int precedente) {
		int xn = r.nextInt(x-1);
		if(labirinto[xn][0] == 9 || labirinto[xn][y-1] == 8 || labirinto[xn][j]==1) {
			setUnoByX(labirinto, r , j , precedente);
		} else if(xn-1 != precedente && xn+1 != precedente && xn!= y-1) {
			precedente = xn;
			inserisciUno(labirinto, xn+1, j);
			try {
				coordinate.add(new Coordinata(xn,j));
			} catch(NullPointerException e) {;}
		} else {
			setUnoByX(labirinto, r , j , precedente);
		}
	}
	
	private static void setUnoByY(int[][] labirinto, Random r, int riga) {
		int yn = r.nextInt(y-1);
		try {
			if(labirinto[riga][yn] == 9 || labirinto[riga][yn] == 8 || labirinto[riga][yn]==1) {
				setUnoByY(labirinto, r , riga);
			} else if(yn!= y-1) {
				labirinto[riga][yn]=1;
				try {
					coordinate.add(new Coordinata(riga,yn));
				} catch(NullPointerException e) {;}
			}
		} catch(ArrayIndexOutOfBoundsException e) {setUnoByY(labirinto, r , riga-1);}
	}
	
	// verifico la presenza di eccezioni particolari
	private static void inserisciIntersezioniParticolari(int[][] labirinto) {
		for(int i=0 ; i < x ; i++) {
			for(int j=0 ; j < y ; j++) {
				
					try {
						// intersezione sinistra in alto e intersezione destra in basso
						if(angleLeftUp(i, j) || angleRightDown(i, j)) {
							if(labirinto[i][j]==0) {
								if(angleLeftUp(i, j)) {
									if(labirinto[i][j+1]==1 || labirinto[i+1][j]==1) {
										inserisciUno(labirinto, i, j);
									}
								}
						
								if(angleRightDown(i, j)) {
									if(labirinto[i][j-1]==1 || labirinto[i-1][j]==1) {
										inserisciUno(labirinto, i, j);
									}
								}
							} else {
								if(angleLeftUp(i, j)) {
									if(labirinto[i+1][j+1]==1) {
										inserisciUno(labirinto, i, j+1);
									}
								}
						
								if(angleRightDown(i, j)) {
									if(labirinto[i-1][j-1]==1) {
										inserisciUno(labirinto, i, j-1);
									}
								}
							}
							
						} else if((i==0 && i==x-1 && j!=y-1 && j!=0 ) && (labirinto[i][j-1]==1 || labirinto[i-1][j]==1 || labirinto[i+1][j]==1)){
							labirinto[i][y]=1;
						} 
						if(i>0 && i<x-1 && j>0 && j<y-1) {
							if(eccezione1(labirinto, i, j) || eccezione2(labirinto, i, j)) {
								inserisciZero(labirinto, i, j);
							}
							
							if(eccezione3(labirinto,i,j,3)) {
								inserisciUno(labirinto, i, j);
							}
							// 11111
							// 10101
							// 11111
							if(j<y-4 && i<x-4 && eccezione5(labirinto, i, j)) {
								inserisciZero(labirinto, i+1, j+2);
							}
							if(j>3 && i>3 && j<y-2 && i<x-2 && eccezione6(labirinto, i, j)) {
								inserisciZero(labirinto, i-2, j-1);
							}
							// 11
							// 11
							if(labirinto[i][j]==1 && labirinto[i+1][j]==1 && labirinto[i][j+1]==1 && labirinto[i+1][j+1]==1) {
								if(labirinto[i-1][j]==1 || labirinto[i][j-1]==1) {
									inserisciZero(labirinto, i-1, j-1);
								}
									
								if(( i<x-2 && labirinto[i+2][j]==1) || (j<y-2 && labirinto[i+1][j+2]==1)) {
									inserisciZero(labirinto, i, j+1);
								}
							}
						}
					} catch(NullPointerException e) {;} catch(ArrayIndexOutOfBoundsException e) {;}
					
				}	
			
		}
	}

	// angolo sinistro alto
	public static boolean angleLeftUp(int i, int j) {
		return j==0 && i==0;
	}

	// angolo destro basso
	public static boolean angleRightDown(int i, int j) {
		return i==x-1 && j==y-1;
	}

	// se non sto sostituendo l'inizio o la fine allora metto 1.
	public static void inserisciUno(int[][] labirinto, int i, int j) {
		if(labirinto[i][j]!=8 && labirinto[i][j]!=-1)
			labirinto[i][j]=1;
	}
	
	public static void inserisciZero(int[][] labirinto, int i, int j) {
		if(labirinto[i][j]!=8 && labirinto[i][j]!=-1)
			labirinto[i][j]=0;
	}

	// calcolo la distanza da una cordinata a un altra
	private static int distanza(int i, int j) {
		return i-j;
	}
	
	
}
