package com.example.labiorient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class Labirinto {
    protected int x,y,parametro;
    protected int[][] maze;
    protected Coordinata inizio=new Coordinata(0,0), fine=new Coordinata(0,0);
    protected int [] caratteristiche = new int[4];
    public Random r = new Random();

    Labirinto(int x, int y) {
        this.x = x*2+2;
        this.y = y*2+2;
        maze = new int[this.x][this.y];
        generateMaze(0, 0);
    }

    // algoritmo per la generazione di labirinti con dimensione doppia rispetto a x e y e bordo dell'array a 0.
    private int[][] display(int limiteLoop) {
        int lunghezzax = this.x, lunghezzay = this.y;
        Random r = new Random();
        int[][] matrice = new int[lunghezzax][lunghezzay];
        for(int i=0 ; i < lunghezzax ; i++) {
            for(int j=0 ; j < lunghezzay; j++) {
                matrice[i][j]=0;
            }
        }
        for (int m = 0; m < (this.x-2)/2; m++) {
            StringBuilder riga = new StringBuilder("0");
            StringBuilder riga2 = new StringBuilder("0");
            for (int n = 0; n < (this.y-2)/2; n++) {
                if(limiteLoop!=0) {
                    String aggiunta = (maze[m][n] & r.nextInt(9)) == 0 ? "00" : "01";
                    riga.append(aggiunta);
                    aggiunta = (maze[m][n] & r.nextInt(9)) == 0 ? "10" : "11";
                    riga2.append(aggiunta);
                } else {
                    String aggiunta = (maze[m][n] & r.nextInt(9)) == 0 ? "10" : "10";
                    riga.append(aggiunta);
                    aggiunta = (maze[m][n] & r.nextInt(9)) == 0 ? "00" : "11";
                    riga2.append(aggiunta);
                }
            }
            // draw the north edge
            riga.append("0");
            // draw the west edge
            riga2.append("0");

            for(int k=0 ; k<riga.length(); k++) {
              matrice[m*2+1][k] = Integer.parseInt(riga.substring(k,k+1));
              matrice[m*2+2][k] = Integer.parseInt(riga2.substring(k,k+1));
            }
        }

        this.inizio = definePosizione(matrice,8);
        this.fine = definePosizione(matrice,9);

        return matrice;
    }

    private static boolean controlloLoop(int[][] labirinto, int i, int j) {
        boolean sopra = labirinto[i][j]==1 && labirinto[i][j+1]==1 && labirinto[i][j+2]==1,
                centro = labirinto[i+1][j]==1 && labirinto[i+1][j+1]==0 && labirinto[i+1][j+2]==1,
                sotto = labirinto[i+2][j]==1 && labirinto[i+2][j+1]==1 && labirinto[i+2][j+2]==1;
        return sopra && centro && sotto;
    }

    private static void inserisciZero(int[][] labirinto, int i, int j){
        if(labirinto[i][j]==1) labirinto[i][j]=0;
    }

    // algoritmo per la generazione di labirinti senza vicoli ciechi
    private int[][] display2(int distanzaMinima) {
        int lunghezzax = this.x, lunghezzay = this.y;
        Random r = new Random();
        int[][] matrice = new int[lunghezzax][lunghezzay];
        for(int i=0 ; i < lunghezzax ; i++) {
            for(int j=0 ; j < lunghezzay; j++) {
                matrice[i][j]=0;
            }
        }
        /*
        0000|0000
        0000|0000
        0000|0000
        ---------   // considero un labrinto come se fosse diviso in 4 quadranti
        0000|0000
        0000|0000
        0000|0000
        */
        int cx1, cy1;
        if(distanzaMinima<7) {
            // quadrante 1
            cx1 = r.nextInt(lunghezzax / 2 - 1) + 1;
            cy1 = r.nextInt(lunghezzay / 2 - 1) + 1;
        } else {
            // quadrante 1
            cx1 = r.nextInt(lunghezzax / 4 - 1) + 1;
            cy1 = r.nextInt(lunghezzay / 4 - 1) + 1;
        }
        // quadrante 2
        int cx2 = r.nextInt(lunghezzax / 2 - 1) + lunghezzax / 2, cy2 = r.nextInt(lunghezzay / 2 - 1) + 1;
        // quadrante 3
        int cx3 = r.nextInt(lunghezzax / 2 - 1) + 1, cy3 = r.nextInt(lunghezzay / 2 - 1) + lunghezzay / 2;
        // quadrante 4
        int cx4 = r.nextInt(lunghezzax / 2 - 1) + lunghezzax / 2, cy4 = r.nextInt(lunghezzay / 2 - 1) + lunghezzay / 2;

        collegaUno(matrice,new Coordinata(cx1,cy1),new Coordinata(cx2,cy2));
        collegaUno(matrice,new Coordinata(cx2,cy2),new Coordinata(cx1,cy1));
        collegaUno(matrice,new Coordinata(cx2,cy2),new Coordinata(cx3,cy3));
        collegaUno(matrice,new Coordinata(cx3,cy3),new Coordinata(cx2,cy2));
        collegaUno(matrice,new Coordinata(cx4,cy4),new Coordinata(cx3,cy3));
        collegaUno(matrice,new Coordinata(cx3,cy3),new Coordinata(cx4,cy4));
        collegaUno(matrice,new Coordinata(cx4,cy4),new Coordinata(cx1,cy1));
        collegaUno(matrice,new Coordinata(cx1,cy1),new Coordinata(cx4,cy4));

        this.inizio = definePosizione(matrice,8);
        this.fine = definePosizione(matrice,9);

        //stampaLabDisplay2(lunghezzax, lunghezzay, matrice);

        return matrice;
    }

    // collega le coordinate nel labirinto senza vicoli ciechi
    private void collegaUno(int[][] labirinto, Coordinata uno, Coordinata due) {
        int distanzax = due.getX()-uno.getX();
        int distanzay = due.getY()-uno.getY();
        for(int m=0 ; m<=Math.abs(distanzay); m++) {
            if(distanzay < 0) {
                inserisciUno(labirinto, uno.getX(),due.getY()+m);
            }
            else if(distanzay > 0){
                inserisciUno(labirinto,uno.getX(),uno.getY()+m);
            }
        }
        for(int m=0 ; m<=Math.abs(distanzax); m++) {
            if(distanzax < 0) {
                inserisciUno(labirinto,due.getX()+m, uno.getY());
            }
            else if(distanzax > 0){
                inserisciUno(labirinto,uno.getX()+m,uno.getY());
            }
        }
    }

    private static void inserisciUno(int[][] labirinto, int i, int j) {
        if(labirinto[i][j]!=8 && labirinto[i][j]!=9)
            labirinto[i][j]=1;
    }

    // calcolo la distanza da una cordinata a un altra
    private static int distanza(int i, int j) { return Math.abs(i-j); }

    private void generateMaze(int cx, int cy) {
        DIR[] dirs = DIR.values();
        Collections.shuffle(Arrays.asList(dirs));
        for (DIR dir : dirs) {
          int nx = cx + dir.dx;
          int ny = cy + dir.dy;
          if (between(nx, x) && between(ny, y) && (maze[nx][ny] == 0)) {
            maze[cx][cy] |= dir.bit;
            maze[nx][ny] |= dir.opposite.bit;
            generateMaze(nx, ny);
          }
        }
    }

    private static boolean between(int v, int upper) {
    return (v >= 0) && (v < upper);
    }

    private enum DIR {
        N(1, 0, -1), S(2, 0, 1), E(4, 1, 0), W(8, -1, 0);
        private final int bit;
        private final int dx;
        private final int dy;
        private DIR opposite;
        // use the static initializer to resolve forward references
        static {
          N.opposite = S;
          S.opposite = N;
          E.opposite = W;
          W.opposite = E;
        }

        DIR(int bit, int dx, int dy) {
          this.bit = bit;
          this.dx = dx;
          this.dy = dy;
        }
    }

    protected Coordinata definePosizione(int[][] l, int posizione) {
        int iniziox = r.nextInt(this.x-1)+1;
        ArrayList<Coordinata> listaPossibiliPosizioni = new ArrayList<>();
        for (int p=1; p<(this.y-1)+1; p++){
            if(l[iniziox][p]==1) listaPossibiliPosizioni.add(new Coordinata(iniziox,p));
        }
        int lengthLista = listaPossibiliPosizioni.size();
        if (lengthLista==0){
            listaPossibiliPosizioni=null;
            return definePosizione(l,posizione);
        } else if(lengthLista>0){
            Coordinata c = listaPossibiliPosizioni.get(r.nextInt(lengthLista));
            l[iniziox][c.getY()]=posizione;
            listaPossibiliPosizioni=null;
            return c;
        } else {
            listaPossibiliPosizioni = null;
            return definePosizione(l,posizione);
        }
    }

    Coordinata getInizio() {
        return inizio;
    }

    Coordinata getFine() {
        return fine;
    }

    // algoritmo di estrazione dei labirinti con richiamo delle verifiche
    public static Labirinto estraiLabirinto(String difficolta, int limiteVicoli, int limiteLoop, int distanzaMinima) {
        // imposto i parametri di creazione del labirinto in base alla difficoltà impostata
        Random rand = new Random();
        int a = 0, b = 0;
        switch (difficolta){
            case "facile":
                if (distanzaMinima<7) {
                    a = rand.nextInt(3) + 3;
                    b = rand.nextInt(3) + 3;
                }
                else {
                    a = rand.nextInt(2) + 4;
                    b = rand.nextInt(2) + 4;
                }
                break;
            case "media":
                if(distanzaMinima<12) {
                    a = rand.nextInt(8) + 5;
                    b = rand.nextInt(8) + 5;
                } else {
                    a = rand.nextInt(5) + 8;
                    b = rand.nextInt(5) + 8;
                }
                break;
            case "difficile":
                a = rand.nextInt(5) + 15;
                b = rand.nextInt(5) + 15;
                break;
            case "nessuna":
                a = rand.nextInt(15)+1;
                b = rand.nextInt(15)+1;
        }
        // incremento la dimensione nel caso in cui si richieda una distanza minima della fine dall'inizio abbastanza ampia
        if(distanzaMinima>7 && (difficolta.equals("facile") || limiteVicoli==0 || limiteLoop==0)) {
            a+=difficolta.equals("facile")? 2 : 6;
            b+=difficolta.equals("facile")? 2 : 6;
        }

        Labirinto labirinto = new Labirinto(a,b);
        try {
            // creo il labirinto
            if (limiteVicoli>0) {
                labirinto.maze = labirinto.display(limiteLoop);
            } else {
                labirinto.maze = labirinto.display2(distanzaMinima);
            }
            // genero un labirinto secondario
            int ly = labirinto.y;
            int lx = labirinto.x;
            int [][] matrice = new int[lx][ly];
            int [][] matrice2 = new int[lx][ly];
            for(int i = 0; i < lx; i++) {
                if (ly >= 0){
                    System.arraycopy(labirinto.maze[i], 0, matrice[i], 0, ly);
                    System.arraycopy(labirinto.maze[i], 0, matrice2[i], 0, ly);
                }
            }
            // imposto le verifiche
            boolean verifica = false;
            boolean verifica2 = false;
            // il parametro definisce la minima distanza dell'inizio dalla fine.
            switch (difficolta){
                case "facile":
                    labirinto.parametro=Math.min(Math.max(distanzaMinima,1),10);
                    break;
                case "media":
                    labirinto.parametro= Math.max(distanzaMinima,getParametroMediaDiff(labirinto));
                    break;
                case "difficile":
                    labirinto.parametro= Math.max(distanzaMinima,getParametroDifficileDiff(labirinto));
                    break;
                case "nessuna":
                    labirinto.parametro= Math.max(distanzaMinima,getParametroMediaDiff(labirinto));
                    break;
            }
            // imposto inizio e fine
            Coordinata i = labirinto.getInizio();
            Coordinata e = labirinto.getFine();

            // verifico che ci sia un percorso dall'inizio alla fine
            if(distanzaRispettataInizioFine(i,e,labirinto.parametro)) {
                labirintoSenzaLoop(limiteLoop, matrice);
                verifica = verificaLabirinto.pathExists(matrice, i, lx, ly);
            }
            // controlli di creazione del labirinto
            if(verifica) {
                System.out.println("Il labirinto prevede un percorso valido dall'inizio alla fine!\n");
                labirintoSenzaLoop(limiteLoop, labirinto.maze);
                // estraggo le caratteristiche del labirinto
                labirinto.caratteristiche = getCaratteristiche(labirinto.maze);
                switch (difficolta){
                    case "facile":
                        verifica2 = condizioneFacile(labirinto.caratteristiche,labirinto.x,labirinto.y);
                        break;
                    case "media":
                        verifica2 = condizioneMedia(labirinto.caratteristiche,labirinto.x,labirinto.y);
                        break;
                    case "difficile":
                        verifica2 = condizioneDifficile(labirinto.caratteristiche);
                        break;
                    case "nessuna":
                        verifica2=true;
                        break;
                }
                // passate entrambe le verifiche ritorno il labirinto, altrimenti ne creo un altro da capo.
                // Se passo come limite di vicoli un valore molto basso e la distanza minima è alta, allora
                // genero labirinti più grandi considerando che il limite dei vicoli sia molto maggiore della distanza minima o del limite di vicoli.
                // se infatti il valore di distanza minima è grande non potrò mai generare labirinti piccoli che abbiano pochi vicoli, dovrei diminuirne la difficoltà, ma non è quanto ci si aspetta.
                //System.out.println(verifica2 + "   " + labirinto.caratteristiche[0]+"<="+ Math.min(limiteVicoli,getLimiteVicoli(difficolta)) + "   " + labirinto.caratteristiche[1]+"<="+limiteLoop  + "   " + labirinto.parametro + "<=" + distanzaMinima);
                if(verifica2 && labirinto.caratteristiche[0]<=Math.min(limiteVicoli,getLimiteVicoli(difficolta)) && labirinto.caratteristiche[1]<=limiteLoop) {
                    // se la difficoltà è "nessuna" devo dire che tipo di labirinto ho generato.
                    if(difficolta.equals("nessuna")){
                        difficolta = getDifficolta(labirinto);
                    }
                    Gioco.labirintoTrovato=true;
                    labirinto.inizio = labirinto.getInizio();
                    labirinto.fine = labirinto.getFine();
                    Gioco.xu = labirinto.getInizio().getX();
                    Gioco.yu = labirinto.getInizio().getY();
                    labirinto.maze[e.getX()][e.getY()]=9;
                    return labirinto;
                }
                else {
                    System.out.println("Caratteristiche non soddisfatte!\n");
                    labirinto=null;
                    return estraiLabirinto(difficolta,limiteVicoli,limiteLoop,distanzaMinima);
                }
            } else {
                // System.out.println("Il labirinto NON prevede un percorso valido dall'inizio alla fine!\n");
                labirinto=null;
                return estraiLabirinto(difficolta,limiteVicoli,limiteLoop,distanzaMinima);
            }
        } catch( StackOverflowError e) { return estraiLabirinto(difficolta,limiteVicoli,limiteLoop,distanzaMinima); }
    }

    private static int getParametroDifficileDiff(Labirinto labirinto) {
        return Math.max(Math.max(labirinto.x,labirinto.y)/2,20);
    }

    private static int getParametroMediaDiff(Labirinto labirinto) {
        return Math.min(Math.max(labirinto.x, labirinto.y)/2,15);
    }

    private static String getDifficolta(Labirinto labirinto) {
        if(condizioneFacile(labirinto.caratteristiche,labirinto.x,labirinto.y)){
            return "facile";
        } else if(condizioneMedia(labirinto.caratteristiche,labirinto.x,labirinto.y)){
            return  "media";
        } else if(condizioneDifficile(labirinto.caratteristiche)){
            return "difficile";
        } else {
            return "media";
        }
    }

    private static void labirintoSenzaLoop(int limiteLoop, int[][] matrice) {
        if(limiteLoop==0) {
            for (int k = 0; k < matrice.length - 2; k++) {
                for (int h = 0; h < matrice[0].length - 2; h++) {
                    if (controlloLoop(matrice, k, h)) {
                        System.out.println("RIMUOVO LOOP");
                        inserisciZero(matrice, k, h);
                    }
                }
            }
        }
    }

    protected static boolean distanzaRispettataInizioFine(Coordinata i,Coordinata e,int p) {
        return Math.abs(i.getX()-e.getX())>=p && Math.abs(i.getY()-e.getY())>=p;
    }

    protected static boolean condizioneFacile(int[] caratteristiche, int dimx, int dimy) {
        return dimx<=16 && dimy<=16 && caratteristiche[0]<=getLimiteVicoli("facile") && caratteristiche[1]<=3;
    }

    protected static boolean condizioneMedia(int[] caratteristiche, int dimx, int dimy) {
        return dimx>=12 && dimy>=12 && dimx<32 && dimy<32 && caratteristiche[0]<=getLimiteVicoli("media") && caratteristiche[1]<=9 && caratteristiche[1]>=3;
    }

    protected static boolean condizioneDifficile(int[] caratteristiche) {
        return caratteristiche[0]>70 && caratteristiche[1]>9;
    }

    // restituisco il limite di vicoli ciechi per la difficoltà
    protected static int getLimiteVicoli(String difficolta) {
        switch (difficolta){
            case "facile":
                return 35;
            case "media":
                return 70;
            case "difficile":
                return 300;
        }
        return 300;
    }

    // restituisco il limite di loop per la difficoltà
    protected static int getLimiteLoop(String difficolta) {
        switch (difficolta){
            case "facile":
                return 3;
            case "media":
                return 9;
            case "difficile":
                return 100;
        }
        return 100;
    }

    // ritorno le caratteristiche principali del labirinto sottoforma di vettore di contatori.
    protected static int[] getCaratteristiche(int[][] matrice) {
        int [] conta = new int[4];
        // uso un altra matrice per evitare di sovrascrivere il labirinto
        int [][] m = matrice;
        for (int h=1; h<m.length-1; h++){
            // in ogni riga controllo ogni cella (x,y).
            for (int i=1; i<m[0].length-1; i++){
                if(m[h][i]!=0) {
                    conta[2] += 1; // conto il numero di 1
                    if (Controllo.controlloCella(m, h, i, 1)) conta[0] += 1; // conto i vicoli ciechi
                    if(Controllo.controlloIntersezione(m,h,i)) {
                        if (!Controllo.controlloPercorso(m,h,i))
                            conta[3] += 1; // conto il numero di intersezioni
                        if (h < m.length - 2 && i < m[0].length - 2 && Controllo.controlloLoop(m, h, i, m.length, m[0].length))
                            conta[1] += 1; // conto la presenza di un loop in una cella
                    }
                }
                System.out.print(m[h][i]);
            }
            System.out.println();
        }
        System.out.println("NUM VC: " + String.valueOf(conta[0]));
        System.out.println("NUM LOOP: " + String.valueOf(conta[1]));
        System.out.println("NUM 1: " + String.valueOf(conta[2]));
        System.out.println("NUM INTERSECTIONS: " + String.valueOf(conta[3]));
        return conta;
    }

    protected static Coordinata posizioneValore(int valore, int[][] labirinto){
        int p1=0, p2=0;
        for(int i=0 ; i < labirinto.length ; i++) {
            for(int j=0 ; j < labirinto[0].length; j++) {
                if(labirinto[i][j]==1) {
                    p1 = i;
                    p2 = j;
                }
                if(labirinto[i][j]==valore)
                    return new Coordinata(i,j);
            }
        }
        return new Coordinata(p1,p2);
    }

    public static int[][] copiaLabirinto(int[][] matrice){
        int [][] lab = new int[matrice.length][matrice[0].length];
        for(int i=0 ; i < matrice.length; i++) {
            if (matrice[0].length >= 0){
                System.arraycopy(matrice[i], 0, lab[i], 0, matrice[0].length);
            }
        }
        return lab;
    }

    public static int returnParametroDistance(int[][] matrice){
        return distanzaMinimaFraCoordinate(posizioneValore(9,matrice),posizioneValore(8,matrice));
    }

    private static int distanzaMinimaFraCoordinate(Coordinata a, Coordinata b){
        return Math.min(distanza(b.getX(),a.getX()),distanza(b.getY(),a.getY()));
    }
}