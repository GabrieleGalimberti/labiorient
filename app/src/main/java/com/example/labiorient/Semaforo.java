package com.example.labiorient;

/**
 * Created by gabriele_galimberti on 03/05/18.
 */

public class Semaforo {

    int semaphore;
    public Semaforo(int i) {
        this.semaphore = i;
    }

    public synchronized void up() {
        semaphore++;
        notify();
    }


    public synchronized void down() {
        if(semaphore<=0){
            try{
                wait();
            }
            catch(InterruptedException ie){
                ;
            }
        }
        semaphore--;
    }

    public int getISemaphore() {
        return semaphore;
    }
}