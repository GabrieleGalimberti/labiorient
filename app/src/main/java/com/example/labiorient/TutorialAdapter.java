package com.example.labiorient;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

class TutorialAdapter extends ArrayAdapter<Tutorial> {

    public TutorialAdapter(@NonNull Context context, @LayoutRes int resource, List textViewResourceId) {
        super(context, resource, textViewResourceId);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.row, null);
        TextView tutorial = (TextView)convertView.findViewById(R.id.tutorialN);
        TextView spiegazione = (TextView)convertView.findViewById(R.id.spiegazione);
        Tutorial c = getItem(position);
        tutorial.setText(c.getDescrizione());
        spiegazione.setText(c.getSpiegazione());
        return convertView;
    }
}
