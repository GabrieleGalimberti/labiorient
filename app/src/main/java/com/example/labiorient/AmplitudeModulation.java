package com.example.labiorient;

import android.media.MediaPlayer;
import android.util.Log;

import com.jsyn.unitgen.LineOut;
import com.jsyn.unitgen.SineOscillator;

import java.util.ArrayList;

/**
 * Created by gabriele_galimberti on 06/04/18.
 */

class AmplitudeModulation extends Modality {

    // AM

    SineOscillator oscillator1, oscillator2;
    LineOut lineOut;

    private int a = 11, b = 440;
    private double minimo = 0;

    public MediaPlayer mp = null;

    public AmplitudeModulation(double direzioneAiuto, ArrayList<Double> angoliPermessi) {
        super(angoliPermessi,direzioneAiuto);
        synthesizer.add(oscillator1 = new SineOscillator());
        synthesizer.add(oscillator2 = new SineOscillator());

        synthesizer.add(lineOut = new LineOut());

        oscillator1.output.connect(0, lineOut.input, 0);
        oscillator1.output.connect(0, lineOut.input, 1);
        oscillator1.frequency.set(440);

        oscillator2.output.connect(oscillator1.amplitude);
        oscillator2.frequency.set(1);

        setVolume(0.60);

        synthesizer.start();
        lineOut.start();
        setAngoloPrecedente(a1);

        Log.e("AM MODALITY","START");
    }

    @Override
    public void applyMode(double angoloSettoreCorrente, double angoloCorrente) {

        diff = calcoloAngolo(a1,angoloCorrente,angoloSettoreCorrente);

        if (diff <= tolleranza) {
            if (non_ha_già_fatto_ping) {
                oscillator2.frequency.set(b + a);
                non_ha_già_fatto_ping = false;

            }
        } else if (diff > tolleranza && diff < Math.abs(a1)){
            minimo = setIntermittenzaEsponenziale(a1, diff);
            oscillator2.frequency.set(b * (minimo) + a);
            non_ha_già_fatto_ping = true;
        }
        if(aiutoGuida) FrequencyHelp(angoloCorrente,oscillator2);
    }

    private void FrequencyHelp(double angoloCorrente, SineOscillator oscillator2) {
        if(getDiff(angoloHelp, angoloCorrente,360) < 1){
            oscillator2.frequency.set(b * 1.05 + a);
        }
    }

    @Override
    public void setSilence() {
        try {
            setVolume(0);
            synthesizer.stop();
            lineOut.stop();
            super.setSilence();
            oscillator1=null;
            oscillator2=null;
            synthesizer=null;
        }
        catch (NullPointerException e){;}
        catch (IllegalStateException e){;}
        Log.d("CIM", "Silence!");
    }

    private void setVolume(double i) {
        oscillator1.amplitude.set(i);
        oscillator2.amplitude.set(i);
    }

}