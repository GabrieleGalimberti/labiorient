package com.example.labiorient;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

public class CustomDecisionTree {

    private double sufficienza = 0.6;

    public CustomDecisionTree(double sufficienza) {
        this.sufficienza = sufficienza;
    }

    String difficoltaUtente(String cartella){
        // PerformanceLabiorient cartella dell'applicazione
        File folder = new File(Environment.getExternalStorageDirectory() + "/"+cartella);
        // utilizzo 2 hash map per tenere traccia delle valutazioni delle performance e dei contatori
        HashMap<String,Integer> valuations = new HashMap<>();
        HashMap<String,Integer> counters = new HashMap<>();
        int maxcontatore = 0, maxContatorePrestazioni = 0;
        boolean insuccessoTotale=true, neutroTotale=true, successoTotale=true;
        // tengo traccia della maggiore e minore difficoltà presente fra i file
        String diffPlusFreq="";
        // leggo i file e interpreto i dati
        for (File fileEntry : folder.listFiles()) {
            try {
                if (fileEntry.getName().contains("partita") && folder.length() >= 10) {
                    Scanner sc = new Scanner(fileEntry);
                    String difficolta = "";
                    // guardo le prestazioni,
                    // scelta: per ogni lab conto i successi nelle prestazioni. definisco successo, neutro, insuccesso per la difficoltà. conto il numero di successi o insuccessi per difficoltà.
                    while (sc.hasNextLine()) {
                        String s = sc.nextLine();
                        Log.i("RIGA",s);
                        int contatore = 0;
                        if (s.length() >= 10) {
                            // Estraggo la prestazioni dell'utente nel labirinto cui si riferisce il file (seconda riga)
                            for (int i = 0; i < s.length(); i++) {
                                // trovo il valore
                                String valore = "";
                                for (int puntoEvirgola = i; puntoEvirgola < s.length(); puntoEvirgola++) {
                                    System.out.print(s.charAt(puntoEvirgola));
                                    if (s.charAt(puntoEvirgola) == ';') {
                                        valore = s.substring(i, puntoEvirgola).replace(',','.');
                                        i = puntoEvirgola;
                                        break;
                                    }
                                }
                                // determino se è successo, neutro, insuccesso per la prestazione.
                                // aumento il contatore per successo, dimunisco per insuccesso
                                Log.e("SUFFICIENZA ", String.valueOf(Double.parseDouble(valore) <= 1 - sufficienza));
                                if (Double.parseDouble(valore) <= this.sufficienza) {
                                    contatore++;
                                } else if (Double.parseDouble(valore) > 1) {
                                    contatore--;
                                }
                            }
                            System.out.println("  contatore="+contatore);
                            // guardo il valore del contatore e determino l'esito della partita e più in generale di tutte le altre
                            String esito;
                            if (contatore > 3) { // successo
                                esito = "successo";
                                insuccessoTotale = false;
                                neutroTotale = false;
                            } else if (contatore == 3) { // neutro
                                esito = "neutro";
                                successoTotale = false;
                                insuccessoTotale = false;
                            } else { // insuccesso
                                esito = "insuccesso";
                                successoTotale = false;
                                neutroTotale = false;
                            }

                            String key = difficolta + " " + esito;
                            // inserisco l'esito come valutazione in una hash map contenente anche il contatore di tali valutazioni
                            if (valuations.containsKey(key)) {
                                int aggiornamento = valuations.get(key) + 1;
                                valuations.put(key, aggiornamento);
                                if(esito.equals("successo")) {
                                    if (aggiornamento > maxcontatore) {
                                        diffPlusFreq = difficolta;  // al momento non restituisce la diffPlusFreq minore
                                    } else if (aggiornamento == maxcontatore) {
                                        diffPlusFreq = interpretaDifficolta(difficolta) < interpretaDifficolta(diffPlusFreq) ? difficolta : diffPlusFreq;
                                    }
                                }
                                maxcontatore = Math.max(aggiornamento, maxcontatore);
                            } else {
                                valuations.put(key, 1);
                            }
                            contatore = Math.abs(contatore);
                            // inserisco l'esito come stringa in una hash map il cui valore è il contatore di performance sufficienti (usato in caso di parità)
                            if (counters.containsKey(difficolta + " " + esito)) {
                                counters.put(difficolta + " " + esito, counters.get(difficolta + " " + esito) + contatore);
                                if (counters.get(difficolta + " " + esito) + contatore > maxContatorePrestazioni) {
                                    maxContatorePrestazioni = counters.get(difficolta + " " + esito) + contatore;
                                }
                            } else {
                                counters.put(difficolta + " " + esito, contatore);
                            }
                        } else {
                            // Estraggo la difficoltà del labirinto cui si riferisce il file (prima riga)
                            difficolta=s;
                        }
                    }
                    Log.e("valuations", valuations.toString());
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        // Prendo una decisione
        return decision(maxcontatore, valuations,counters,insuccessoTotale,successoTotale,neutroTotale, diffPlusFreq,maxContatorePrestazioni);

    }

    protected String decision(int maxcontatore, HashMap<String, Integer> valuations, HashMap<String, Integer> counters, boolean insuccessoTotale, boolean successoTotale, boolean neutroTotale, String diffPlusFreq, int maxContatorePrestazioni){
        // considero i seguenti casi per determinare se aumentare, diminuire o invariare la difficoltà.
        if(maxcontatore>=3){
            Iterator it = valuations.entrySet().iterator();
            // Verifico con il metodo hasNext() che nella hashmap ci siano altri elementi su cui ciclare
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry)it.next();
                String v = (String) entry.getKey();
                // Stampa a schermo la coppia chiave-valore;
                System.out.println("Key = " + v);
                System.out.println("Value = " + entry.getValue());
                // se >= 3 allora aumento la difficoltà rispetto alla difficoltà più frequente (con maggior contatore), aumento la difficoltà se successo o diminuisco se insuccesso
                if (valuations.get(v)>=3){
                    return findDiff(v.substring(0,v.indexOf(' ')),v.substring(v.indexOf(' ')+1));
                }
            }
        } else {
            if (insuccessoTotale) {
                // se sono andati tutti male allora restituisco il lab con difficoltà minore fra essi. riduco la difficoltà
                System.out.println("TUTTI INSUCCESSI, DIMINUISCO LA DIFFICOLTÀ RISPETTO A " + diffPlusFreq);
                return difficoltaPrecedente(diffPlusFreq);
            } else if (successoTotale) {
                // se sono tutti uguali ma ho tutti successi aumento di 1 la difficoltà rispetto alla difficoltà più frequente e minore in caso di parità
                System.out.println("TUTTI SUCCESSI, AUMENTO LA DIFFICOLTÀ RISPETTO A " + diffPlusFreq);
                return difficoltaSuccessiva(diffPlusFreq);
            } else if (neutroTotale) {
                System.out.println("RITORNO LA DIFFICOLTÀ MINORE PIÙ FREQUENTE " + diffPlusFreq);
                return diffPlusFreq;
            } else {
                // la difficoltà viene restituita in base alla somma delle prestazioni (contatore) per ogni esito
                Iterator it = valuations.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry entry = (Map.Entry)it.next();
                    String v = (String) entry.getKey();
                    String difficolta = v.substring(0,v.indexOf(' '));
                    // controllo se le prestazioni corrispondono al contatore,
                    // controllo se il numero di valutazioni è pari al massimo numero per quella difficolà con quell'esito,
                    // controllo che il numero di prestazioni sia >3,
                    if(counters.get(v)==maxContatorePrestazioni && valuations.get(v)==maxcontatore && maxContatorePrestazioni>3 && difficolta.equals(diffPlusFreq)){
                        System.out.println("RESTITUISCO LA DIFFICOLTÀ IN BASE AL MAGGIOR NUMERO DI PRESTAZIONI " + difficolta);
                        return difficolta; // NON CONSIDERO L'ESITO
                    }
                }
            }
        }
        System.out.println("NESSUN RISULTATO");
        return "nessuna";
    }

    private String findDiff(String difficolta, String esito) {
        switch (esito) {
            case "successo":
                System.out.println("AUMENTO LA DIFFICOLTÀ RISPETTO A " + difficolta);
                return difficoltaSuccessiva(difficolta);
            case "insuccesso":
                System.out.println("DIMINUISCO LA DIFFICOLTÀ RISPETTO A " + difficolta);
                return difficoltaPrecedente(difficolta);
            case "neutro":
                System.out.println("LASCIO INVARIATA LA DIFFICOLTÀ RISPETTO A " + difficolta);
                return difficolta;
        }
        return difficolta;
    }

    private String difficoltaPrecedente(String d){
        if(d.equals("difficile")){
            return "media";
        } else if(d.equals("media")){
            return "facile";
        }
        return "facile";
    }

    private String difficoltaSuccessiva(String d){
        if(d.equals("facile")){
            return "media";
        } else if(d.equals("media")){
            return "difficile";
        }
        return "difficile";
    }

    private int interpretaDifficolta(String diff){
        if(diff.equals("difficile"))
            return 2;
        else if(diff.equals("media"))
            return 1;
        return 0;
    }
}
