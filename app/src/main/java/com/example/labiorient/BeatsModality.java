package com.example.labiorient;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;

import com.jsyn.unitgen.LineOut;
import com.jsyn.unitgen.SineOscillator;

import java.util.ArrayList;

/**
 * Created by gabriele_galimberti on 10/04/18.
 */

class BeatsModality extends Modality {

    SineOscillator oscillator1,oscillator2;
    LineOut lineOut;

    private double a2 = 0;
    private double freq = 0;

    public MediaPlayer mp = null;

    public BeatsModality(Context applicationContext, double direzioneAiuto, boolean makePing, ArrayList<Double> angoliPermessi) {
        super(angoliPermessi,direzioneAiuto);
        synthesizer.add(oscillator1 = new SineOscillator());
        synthesizer.add(oscillator2 = new SineOscillator());
        synthesizer.add(lineOut = new LineOut());
        oscillator1.output.connect(0,lineOut.input,0);
        oscillator1.output.connect(0,lineOut.input,1);
        oscillator1.amplitude.set(0.5);
        oscillator1.frequency.set(327);
        oscillator2.output.connect(0,lineOut.input,0);
        oscillator2.output.connect(0,lineOut.input,1);
        oscillator2.amplitude.set(0.7);
        oscillator2.frequency.set(freq);
        synthesizer.start();
        lineOut.start();

        this.makePing = makePing;
        mp = new MediaPlayer();
        mp = MediaPlayer.create(applicationContext, R.raw.ping_2);

        gamma = 4;

    }

    @Override
    public void applyMode(double angoloSettoreCorrente, double angoloCorrente) {

        diff = calcoloAngolo(a1,angoloCorrente,angoloSettoreCorrente);

        if (((int) angoloSettoreCorrente) != (int) a2) {
            a2 = angoloSettoreCorrente;
            if (diff >= tolleranza) {
                calcoloFreq();
                non_ha_già_fatto_ping = true;
            } else {
                if (non_ha_già_fatto_ping) {
                    oscillator2.frequency.set(327);
                    if(makePing)
                        mp.start();
                    non_ha_già_fatto_ping = false;
                }
            }
        }
        if(makePing) zeroCrossing(diff, mp);
        if(aiutoGuida) FrequencyHelp(angoloCorrente,oscillator2, oscillator1);
    }

    private void FrequencyHelp(double angoloCorrente, SineOscillator oscillator, SineOscillator oscillator2) {
        if(getDiff(angoloHelp, angoloCorrente,360) < 1){
            oscillator.frequency.set(0);
            oscillator2.frequency.set(0);
        }
        else {
            oscillator2.frequency.set(327);
        }
    }

    private void calcoloFreq() {
        freq = 327 - 15 * (1-setIntermittenzaEsponenziale(a1,diff));
        oscillator2.frequency.set(freq);
    }

    @Override
    public void setSilence() {
        try {
            oscillator1.amplitude.set(0);
            oscillator2.amplitude.set(0);
            synthesizer.stop();
            lineOut.stop();
            oscillator1=null;
            oscillator2=null;
            synthesizer=null;
            super.setSilence();
            if(mp != null){
                mp.stop();
                mp.release();
                mp = null;
            }
            Log.d("BEATS MODALITY","Silence!");
        }
        catch (IllegalStateException e){;}
        catch (NullPointerException e){;}
    }
}
