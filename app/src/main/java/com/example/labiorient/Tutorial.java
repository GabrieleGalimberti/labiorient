package com.example.labiorient;

class Tutorial {

    String descrizione;
    String spiegazione;

    public Tutorial(String descrizione, String spiegazione) {
        this.descrizione = descrizione;
        this.spiegazione = spiegazione;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public String getSpiegazione() {
        return spiegazione;
    }

    public void setSpiegazione(String spiegazione) {
        this.spiegazione = spiegazione;
    }
}
