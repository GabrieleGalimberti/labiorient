package com.example.labiorient;

class Prestazione {

    String descrizione;
    double valuePrestazioneDouble;

    public Prestazione(String descrizione, double valuePrestazioneDouble) {
        this.descrizione = descrizione;
        this.valuePrestazioneDouble = valuePrestazioneDouble;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public double getValuePrestazioneDouble() {
        return valuePrestazioneDouble;
    }

}
