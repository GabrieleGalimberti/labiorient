package com.example.labiorient;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;

import com.jsyn.unitgen.LineOut;
import com.jsyn.unitgen.SineOscillator;

import java.util.ArrayList;

/**
 * Created by gabriele_galimberti on 12/04/18.
 */

class TailPingModality extends Modality {

    SineOscillator oscillator2;
    LineOut lineOut;

    public MediaPlayer mp = null;

    public TailPingModality(Context applicationContext, double direzioneAiuto, ArrayList<Double> angoliPermessi) {
        super(angoliPermessi,direzioneAiuto);
        synthesizer.add(lineOut = new LineOut());
        synthesizer.add(oscillator2 = new SineOscillator());
        oscillator2.output.connect(0,lineOut.input,0);
        oscillator2.output.connect(0,lineOut.input,1);
        oscillator2.amplitude.set(0);
        oscillator2.frequency.set(350);

        synthesizer.start();
        lineOut.start();
        mp = new MediaPlayer();
        mp = MediaPlayer.create(applicationContext, R.raw.ping_2);

        Log.e("AHEAD PING MODALITY","START");
    }

    @Override
    public void applyMode(double angoloSettoreCorrente, double angoloCorrente) {

        diff = calcoloAngolo(a1,angoloCorrente,angoloSettoreCorrente);

        if (diff <= 15 && diff > tolleranza) playQueuePing();
        else if (diff <= tolleranza) playPing();
        else {
            setAmplitude(0);
            non_ha_già_fatto_ping = true;
        }
        if(makePing) zeroCrossing(diff, mp);
        if(aiutoGuida) zeroCrossingHelp(angoloCorrente,mp);
    }

    private void playQueuePing() {
        non_ha_già_fatto_ping = true;
        oscillator2.amplitude.set(1 - diff/15);
    }

    private void playPing() {
        setAmplitude(0.1);
        if(non_ha_già_fatto_ping) {
            mp.start();
            non_ha_già_fatto_ping = false;
        }
    }

    private void setAmplitude(double amplitude) {
        oscillator2.amplitude.set(amplitude);
    }

    @Override
    public void setSilence() {
        try {
            setAmplitude(0);
            synthesizer.stop();
            lineOut.stop();
            synthesizer = null;
            super.setSilence();
            if (mp != null) {
                mp.stop();
                mp.release();
                mp = null;
            }
            Log.d("PRE-PING MODALITY", "Silence!");
        }
        catch (NullPointerException e){;}
        catch (IllegalStateException e){;}
    }

}