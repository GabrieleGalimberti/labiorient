package com.example.labiorient;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;

public class LabirinthManager {

    private final static String TAG = LabirinthManager.class.getCanonicalName();
    Context context;

    public LabirinthManager(Context applicationContext) {
        this.context = applicationContext;
    }

    // gestisco la presenza di un loop controllando le coordinate degli ultimi punti visitati.
    protected int gestioneLoop(int delay, ArrayList<Coordinata> intersezioniRecenti, HashMap<String, Integer> intersezioniPassate, int xu, int yu) {
        Coordinata posizioneAttuale = new Coordinata(xu,yu);
        // controllo se l'utente è finito in un loop
        intersezioniRecenti.add(posizioneAttuale);
        if(intersezioniRecenti.size()>=5){
            if(intersezioniRecenti.get(0).getX() == intersezioniRecenti.get(intersezioniRecenti.size()-1).getX() && intersezioniRecenti.get(0).getY() == intersezioniRecenti.get(intersezioniRecenti.size()-1).getY()){
                Gioco.loop = true;
                Gioco.conteggioLoop+=1;
                Log.e("LOOP","PRIMA E ULTIMA COORDINATA UGUALI");
                Log.e("ULTIMA POS. VISITATA",(intersezioniRecenti.get(intersezioniRecenti.size()-1)).toString());
                Log.e("PRIMA POS. VISITATA",(intersezioniRecenti.get(0)).toString());
                delay += 1500;
            }
            intersezioniRecenti.remove(intersezioniRecenti.size()-1);
        }
        // controllo se l'utente è finito molte volte in quella posizione
        String coordinata = posizioneAttuale.toString();
        if(intersezioniPassate.containsKey(coordinata)) {
            int contatore = intersezioniPassate.get(coordinata) + 1;
            intersezioniPassate.put(coordinata, contatore);
            Log.e("OCCORRENZA POSIZIONE","SEI GIÀ STATO QUI");
            Log.e("OCCORRENZA POSIZIONE",coordinata);
            if(contatore%3==0){
                Gioco.multiLoop = true;
                delay += 800;
            }
        }
        else {
            intersezioniPassate.put(coordinata,1);
        }

        return delay;
    }

    // gestisco il vicolo cieco
    protected void gestioneVicoloCieco(Labirinto lab, int xu, int yu, char direzione) {
        Coordinata vicolo = trovaVicolo(lab.maze, xu, yu);
        invertiDirezione(vicolo,xu,yu,direzione);
        move(lab,xu,yu,direzione);
    }

    // applico il movimento nel labirinto a seconda della direzione controllando se è possibile percorrerla
    protected boolean move(Labirinto lab, int xu, int yu, char direzione) {
        boolean MOVIMENTO_AVVENUTO = true;
        System.out.println(TAG+" MOVIMENTO");
        if(direzione=='a' && lab.maze[xu-1][yu]!=0){
            Gioco.xu -= 1;
            Gioco.predirection = 0;
            System.out.println(TAG+" AVANTI");
        } else if(direzione=='i' && lab.maze[xu+1][yu]!=0){
            Gioco.xu += 1;
            Gioco.predirection = 180;
            System.out.println(TAG+" INDIETRO");
        } else if(direzione=='d' && lab.maze[xu][yu+1]!=0){
            Gioco.yu += 1;
            Gioco.predirection = 90;
            System.out.println(TAG+" DESTRA");
        } else if(direzione=='s' && lab.maze[xu][yu-1]!=0){
            Gioco.yu -= 1;
            Gioco.predirection = 270;
            System.out.println(TAG+" SINISTRA");
        } else {
            MOVIMENTO_AVVENUTO = false;
            System.out.println(TAG+" NON AVVENUTO");
        }
        return MOVIMENTO_AVVENUTO;
    }

    // trovo dove è il vicolo cieco rispetto la posizione dell'utente. serve a localizzare il vicolo per la successiva rimozione
    protected Coordinata trovaVicolo(int[][] maze, int xu, int yu) {
        if(maze[xu-1][yu]!=0){
            return new Coordinata(xu+1,yu);
        } else if(maze[xu][yu-1]!=0){
            return new Coordinata(xu,yu+1);
        } else if(maze[xu+1][yu]!=0){
            return new Coordinata(xu-1,yu);
        } else if(maze[xu][yu+1]!=0) {
            return new Coordinata(xu, yu - 1);
        }
        return null;
    }

    // inverto la direzione dell'utente se è finito in un vicolo cieco.
    protected void invertiDirezione(Coordinata vicolo, int xu, int yu, char direzione) {
        System.out.println(TAG+" INVERSIONE DIREZIONE");
        try {
            int yVicolo = vicolo.getY(), xVicolo = vicolo.getX();
            if(xVicolo==xu){
                if(yVicolo-yu<0){
                    Gioco.direzione='d';
                } else {
                    Gioco.direzione='s';
                }
            } else if(yVicolo==yu){
                if(xVicolo-xu<0){
                    Gioco.direzione='i';
                } else {
                    Gioco.direzione='a';
                }
            }
        } catch (NullPointerException e){
            e.printStackTrace();
        }
        getDirezione(Gioco.direzione);
    }

    // valuto l'angolo di rotazione o tocco attuale per valutare se l'utente si è direzionato correttamente
    protected boolean valutaDirezione(double angoloCorrente,int[][] maze, int xu, int yu, char direzione) {
        if(Math.abs(angoloCorrente)<=10){
            return controllaDirezione(maze[xu - 1][yu],'a');
        }
        else if(angoloCorrente<=-80 && angoloCorrente>=-100){
            return controllaDirezione(maze[xu][yu - 1],'s');
        }
        else if(angoloCorrente>=80 && angoloCorrente<=100){
            return controllaDirezione(maze[xu][yu + 1],'d');
        }
        else if(Math.abs(angoloCorrente)>=170){
            return controllaDirezione(maze[xu + 1][yu],'i');
        }
        return false;
    }

    // controllo il valore della cella del labirinto nelle direzione scelta per muoversi
    protected boolean controllaDirezione(int valore, char nuovaDirezione) {
        if (valore != 0) {
            Gioco.direzione=nuovaDirezione;
            return true;
        }
        Gioco.parla("Non puoi andare di qua,", TextToSpeech.QUEUE_FLUSH);
        return false;
    }

    // DEBUG utile per vedere la direzione su Log
    protected void getDirezione(char direzione){
        if(direzione=='a'){
            System.out.println("DIREZIONE: avanti");
        }
        if(direzione=='i'){
            System.out.println("DIREZIONE: indietro");
        }
        if(direzione=='s'){
            System.out.println("DIREZIONE: sinistra");
        }
        if(direzione=='d'){
            System.out.println("DIREZIONE: destra");
        }
    }

    // rimuovo il percorso di 1 da un vicolo cieco alla intersezione in verticale
    protected void remove1Vertical(Labirinto lab, int xu, int yu, Coordinata vicolo) {
        boolean dxsx = vicolo.getX()-xu > 0;
        for (int e=0; e<Math.abs(vicolo.getX()-xu); e++) {
            if (dxsx){
                lab.maze[xu + e+1][yu] = 0;
                System.out.println("rimuovo: "+new Coordinata(xu+e+1,yu).toString());
            }
            else {
                lab.maze[vicolo.getX() + e][yu] = 0;
                System.out.println("rimuovo: "+new Coordinata(vicolo.getX() + e,yu).toString());
            }
        }
    }

    // rimuovo il percorso di 1 da un vicolo cieco alla intersezione in orizzontale
    protected void remove1Horizontal(Labirinto lab, int xu, int yu, Coordinata vicolo) {
        boolean dxsx = vicolo.getY()-yu > 0;
        for (int e=0; e<Math.abs(vicolo.getY()-yu); e++){
            if(dxsx) {
                lab.maze[xu][yu + e+1] = 0;
                System.out.println("rimuovo: "+new Coordinata(xu,yu+e+1).toString());
            }
            else{
                lab.maze[xu][vicolo.getY()+e]=0;
                System.out.println("rimuovo: "+new Coordinata(xu,vicolo.getY()+e).toString());
            }
        }
    }

    // valuto la vie disponibili nella posizione dell'utente nel labirinto (valutando il numero di 1 attorno alla posizione)
    protected ArrayList<Coordinata> vieDisponibili(Labirinto lab, int px, int py, Coordinata end, boolean aiuto) {
        // preparo una lista vuota che riempio con le direzioni possibili (vie disponibili)
        ArrayList<Coordinata> list = new ArrayList<>();
        double distanza=360;
        Gioco.angoliPermessi = new ArrayList<Double>();

        System.out.println("LISTA + DIREZIONI");
        int endx = end.getX();
        int endy = end.getY();
        double direzioneAiuto = 0;

        // per ogni casella attorno alla posizione attuale dell'utente controllo se può essere percorsa.
        // se si allora inserisco la cella nella lista, indico quale direzione è permessa per il calcolo dell'angolo e per definire su quali angoli fornire feedback da parte della guida sonora e tattile
        // se i permessi per il feedback di aiuto sono Abilitati allora calcolo la distanza tra il punto e la fine del labirinto per definire quale sia la direzione di aiuto

        if(lab.maze[px-1][py]!=0){
            list.add(new Coordinata(px-1,py));
            Gioco.angoliPermessi.add(0.0);
            System.out.println("AGGIUNTO AVANTI");
            if(aiuto) {
                double d = distanzaAiuto(px - 1, py, endx, endy);
                if (d < distanza) {
                    distanza = d;
                    direzioneAiuto = 0;
                }
            }
        }
        if(lab.maze[px][py+1]!=0){
            list.add(new Coordinata(px,py+1));
            Gioco.angoliPermessi.add(90.0);
            System.out.println("AGGIUNTO DESTRA");
            if(aiuto) {
                double d = distanzaAiuto(px, py + 1, endx, endy);
                if (d < distanza) {
                    distanza = d;
                    direzioneAiuto = 90;
                }
            }
        }
        if(lab.maze[px+1][py]!=0){
            list.add(new Coordinata(px+1,py));
            Gioco.angoliPermessi.add(180.0);
            System.out.println("AGGIUNTO INDIETRO");
            if(aiuto) {
                double d = distanzaAiuto(px + 1, py, endx, endy);
                if (d < distanza) {
                    distanza = d;
                    direzioneAiuto = 180;
                }
            }
        }
        if(lab.maze[px][py-1]!=0){
            list.add(new Coordinata(px,py-1));
            Gioco.angoliPermessi.add(-90.0);
            System.out.println("AGGIUNTO SINISTRA");
            if(aiuto) {
                double d = distanzaAiuto(px, py - 1, endx, endy);
                if (d < distanza) {
                    direzioneAiuto = -90;
                }
            }
        }
        // imposto la direzione di aiuto
        Gioco.direzioneAiuto=direzioneAiuto;
        // restituisco la lista della vie disponibili.
        return list;
    }

    // calcolo la distanza tra il punto di fine del labirinto e un punto del labirinto. serve a trovare la via più veloce per arrivare alla fine
    private double distanzaAiuto(int posizioneX, int posizioneY, int fineX, int fineY){
        return Math.sqrt(Math.pow(fineX-posizioneX,2) + Math.pow(fineY-posizioneY,2));
    }

    // restituisco una stringa che informi a quale categoria di distanza mi trovo (3 livelli).
    // livello A = poco importante
    // livello AA = abbastanza importante
    // livello AAA = molto importante
    String getCategoriaDistanza(int xu, int yu, Coordinata end) {
        double d = distanzaAiuto(xu,yu,end.getX(),end.getY());
        if(d>=10){
            return "A";
        } else if (d>=5){
            return "AA";
        }
        return "AAA";
    }
}
