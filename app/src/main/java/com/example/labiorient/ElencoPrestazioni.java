package com.example.labiorient;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;

public class ElencoPrestazioni extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_elenco_prestazioni);
        setupActionBar();
        Salvataggio salvataggio = new Salvataggio();
        ListView listViewPrestazioni = (ListView) findViewById(R.id.elenco_prestazioni);
        ArrayList<Prestazione> prestazioni = listPrestazioni(salvataggio);
        PrestazioniAdapter prestazioniAdapter = new PrestazioniAdapter(this, android.R.layout.list_content, prestazioni);
        listViewPrestazioni.setAdapter(prestazioniAdapter);
    }

    private ArrayList<Prestazione> listPrestazioni(Salvataggio salvataggio) {

        String [] livelli = {"facile","media","difficile"};

        ArrayList<Prestazione> prestaziones = new ArrayList<>();

        int percorsi=0;
        for (String livello : livelli) {
            int labPercorsi = salvataggio.ottieniPrestazione(this, "labirinti_diff_" + livello + "_percorsi");
            prestaziones.add(new Prestazione("Labirinti difficoltà " + livello + " percorsi", labPercorsi));
            percorsi += labPercorsi;
        }
        prestaziones.add(new Prestazione("Labirinti percorsi",percorsi));

        int conclusi=0;
        for (String livello : livelli) {
            int labConclusi = salvataggio.ottieniPrestazione(this, "labirinti_diff_" + livello + "_conclusi");
            prestaziones.add(new Prestazione("Labirinti difficoltà " + livello + " conclusi", labConclusi));
            conclusi += labConclusi;
        }
        prestaziones.add(new Prestazione("Labirinti conclusi",conclusi));

        int fall=0;
        for (String livello : livelli){
            int fallimenti = salvataggio.ottieniPrestazione(this,"fallimenti_diff_"+livello);
            prestaziones.add(new Prestazione("Fallimenti Labirinti di difficoltà "+livello,fallimenti));
            fall += fallimenti;
        }
        prestaziones.add(new Prestazione("Fallimenti totali",fall));

        double tempoTotalePercorrenza=0;
        for (String livello : livelli){
            try {
                Log.e("TEMPO PERC "+livello,String.valueOf(salvataggio.ottieniPrestazioneLong(this, "tempo_percorrenza_"+livello)));
                Log.e("CONT TEMPO "+livello,String.valueOf(salvataggio.ottieniPrestazione(this, "contatore_tempo_percorrenza_"+livello)));
                double tempoPercorrenza = ((double)salvataggio.ottieniPrestazioneLong(this, "tempo_percorrenza_"+livello) / (salvataggio.ottieniPrestazione(this, "contatore_tempo_percorrenza_"+livello)*1.0));
                tempoTotalePercorrenza += tempoPercorrenza;
                prestaziones.add(new Prestazione("Tempo medio di percorrenza Labirinti di difficoltà "+livello,tempoPercorrenza));
            } catch (ArithmeticException e){;}
        }
        prestaziones.add(new Prestazione("Tempo medio di percorrenza", tempoTotalePercorrenza/3000));

        prestaziones.add(new Prestazione("Sospensioni Totali",salvataggio.ottieniPrestazione(this,"sospensione")));
        prestaziones.add(new Prestazione("Abbandoni Totali",salvataggio.ottieniPrestazione(this,"abbandoni")));

        return prestaziones;
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
