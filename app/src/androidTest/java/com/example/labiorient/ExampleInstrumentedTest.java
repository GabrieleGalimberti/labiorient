package com.example.labiorient;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.HashMap;
import java.util.Random;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    private Context appContext = InstrumentationRegistry.getTargetContext();

    private LabirinthManager labirinthManager = new LabirinthManager(appContext);

    @Test
    public void useAppContext() {
        // Context of the app under test.
        assertEquals("com.example.labiorient", appContext.getPackageName());
    }

    // Test6.1, Test6.2, Test6.3, Test6.5
    @Test
    public void testGestioneVicoloCieco() {
        int[][] matrice = new int[][]{
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 8, 1, 1, 1, 1, 0},
                {0, 0, 0, 1, 0, 1, 0, 0},
                {0, 0, 0, 1, 0, 1, 0, 0},
                {0, 1, 1, 1, 1, 9, 1, 0},
                {0, 0, 0, 0, 0, 0, 0, 0}
        };
        Gioco.xu = 4;
        Gioco.yu = 2;
        Labirinto labirinto = new Labirinto(2,3);
        labirinto.maze = Labirinto.copiaLabirinto(matrice);
        Gioco.direzione = 's';
        Coordinata end = new Coordinata(4,5);
        labirinthManager.move(labirinto,Gioco.xu,Gioco.yu,Gioco.direzione);
        Coordinata posizioneVicolo = new Coordinata(Gioco.xu,Gioco.yu);
        Coordinata vicolo = labirinthManager.trovaVicolo(labirinto.maze, Gioco.xu, Gioco.yu);
        labirinthManager.invertiDirezione(vicolo,Gioco.xu,Gioco.yu,Gioco.direzione);
        assertEquals(new Coordinata(4,1).toString(),posizioneVicolo.toString()); // Test6.1 Controllo se ho trovato il vicolo presso quella coordinata
        assertEquals('d',Gioco.direzione);  // Test6.1 Cambio direzione rispetto al vicolo
        assertFalse(labirinthManager.vieDisponibili(labirinto,Gioco.xu,Gioco.yu,end,false).size() >=2); // Test6.3 Verifico che la posizione attuale non sia una intersezione
        assertEquals(1,labirinthManager.vieDisponibili(labirinto,Gioco.xu,Gioco.yu,end,false).size()); // mostro che invece è un vicolo cieco
        DEBUG(labirinto.maze,labirinto.maze.length,labirinto.maze[0].length,Gioco.xu,Gioco.yu);

        // sposto la posizione fino alla prossima intersezione.
        labirinthManager.move(labirinto,Gioco.xu,Gioco.yu,Gioco.direzione);
        labirinthManager.move(labirinto,Gioco.xu,Gioco.yu,Gioco.direzione);
        DEBUG(labirinto.maze,labirinto.maze.length,labirinto.maze[0].length,Gioco.xu,Gioco.yu);
        assertEquals(3,labirinthManager.vieDisponibili(labirinto,Gioco.xu,Gioco.yu,end,false).size()); //Test6.2 Controllo se mi trovo in una intersezione dal numero di vie disponibili a partire da quel punto

        // rimuovo il vicolo cieco
        labirinthManager.remove1Horizontal(labirinto,Gioco.xu,Gioco.yu,posizioneVicolo); // Test6.5 Rimuovo il vicolo cieco distante due passi in orizzontale.
        assertArrayEquals(new int[][]{
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 8, 1, 1, 1, 1, 0},
                {0, 0, 0, 1, 0, 1, 0, 0},
                {0, 0, 0, 1, 0, 1, 0, 0},
                {0, 0, 0, 1, 1, 9, 1, 0},
                {0, 0, 0, 0, 0, 0, 0, 0}
        },labirinto.maze); // confronto il labirinto attuale con quello che vorrei ottenere.
    }

    // Test6.4, Test6.6, Test6.7, Test6.8
    @Test
    public void testPosizioni() {
        int[][] matrice = new int[][]{
                {0, 0, 0, 0, 0, 0},
                {0, 0, 1, 1, 1, 0},
                {0, 0, 0, 1, 0, 0},
                {0, 8, 0, 1, 0, 0},
                {0, 1, 1, 9, 1, 0},
                {0, 0, 0, 0, 0, 0}
        };
        Gioco.xu = 3;
        Gioco.yu = 1;
        Labirinto labirinto = new Labirinto(2,2);
        labirinto.maze = Labirinto.copiaLabirinto(matrice);
        Gioco.direzione = 's';

        assertFalse(labirinthManager.move(labirinto, Gioco.xu, Gioco.yu, Gioco.direzione)); // Test6.6 l'utente cerca di andare su una cella del labirinto non percorribile
        Coordinata posizioneVicolo = new Coordinata(Gioco.xu,Gioco.yu);
        Coordinata vicolo = labirinthManager.trovaVicolo(labirinto.maze, Gioco.xu, Gioco.yu);
        labirinthManager.invertiDirezione(vicolo,Gioco.xu,Gioco.yu,Gioco.direzione);

        // Test6.4 Rimuovo il vicolo cieco distante un passo in verticale.
        labirinthManager.move(labirinto,Gioco.xu,Gioco.yu,Gioco.direzione);
        labirinthManager.remove1Vertical(labirinto,Gioco.xu,Gioco.yu,posizioneVicolo);
        assertArrayEquals(new int[][]{
                {0, 0, 0, 0, 0, 0},
                {0, 0, 1, 1, 1, 0},
                {0, 0, 0, 1, 0, 0},
                {0, 0, 0, 1, 0, 0},
                {0, 1, 1, 9, 1, 0},
                {0, 0, 0, 0, 0, 0}
        },labirinto.maze);
        Gioco.direzione='d';
        assertTrue(labirinthManager.move(labirinto, Gioco.xu, Gioco.yu, Gioco.direzione)); // Test6.7 l'utente cerca di andare su una cella del labirinto percorribile

        labirinthManager.move(labirinto,Gioco.xu,Gioco.yu,Gioco.direzione);
        DEBUG(labirinto.maze,labirinto.maze.length,labirinto.maze[0].length,Gioco.xu,Gioco.yu);
        assertEquals(9,labirinto.maze[Gioco.xu][Gioco.yu]); // Test6.8 l'utente arriva al punto finale del labirinto.
    }

    // 7.1 7.2 7.3
    @Test
    public void testMovimentiNelleIstruzioni(){
        int[][] matrice = new int[][]{
                {0, 0, 0, 0, 0, 0},
                {0, 1, 1, 8, 1, 0},
                {0, 1, 0, 1, 0, 0},
                {0, 1, 0, 0, 1, 0},
                {0, 1, 1, 9, 1, 0},
                {0, 0, 0, 1, 0, 0},
                {0, 1, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0}
        };
        Gioco.xu = 1;
        Gioco.yu = 3;
        Labirinto labirinto = new Labirinto(3,2);
        labirinto.maze = Labirinto.copiaLabirinto(matrice);
        Gioco.direzione = 'a';
        boolean continueRun = true, scadenzaTimer=false;
        long inizioTS = System.currentTimeMillis();
        // Test 7.1 l'utente cerca di intraprendere una via non permessa verso una certa direzione.
        // per 10 secondi controllo che scegliendo sempre quella via non permessa l'utente non riuscirà mai a prenderla.
        // controllo poi che il timer scada
        while(continueRun){
            if(labirinthManager.valutaDirezione(0,labirinto.maze,Gioco.xu,Gioco.yu,Gioco.direzione)){
                continueRun=false;
                System.out.println(Gioco.direzione);
            }
            if(System.currentTimeMillis()-inizioTS>10000){
                continueRun=false;
                scadenzaTimer=true;
                System.out.println(Gioco.direzione+ " PRIMO TEST");
            }
        }
        assertFalse(continueRun);
        assertTrue(scadenzaTimer);
        assertEquals('a',Gioco.direzione);
        // Test 7.2 l'utente cerca di intraprendere una via permessa verso una certa direzione.
        // per 10 secondi controllo che scegliendo tra varie vie non permesse e permesse l'utente riuscirà a prenderne una.
        // controllo poi che il timer non scada
        continueRun = true;
        scadenzaTimer=false;
        inizioTS = System.currentTimeMillis();
        Random r = new Random();
        while(continueRun){
            if((System.currentTimeMillis()-inizioTS) > 1000 && labirinthManager.valutaDirezione(r.nextInt(2)*90,labirinto.maze,Gioco.xu,Gioco.yu,Gioco.direzione)){
                continueRun=false;
                System.out.println(Gioco.direzione + " SECONDO TEST");
            }
            if(System.currentTimeMillis()-inizioTS>10000){
                continueRun=false;
                scadenzaTimer=true;
            }
        }
        assertFalse(continueRun);
        assertFalse(scadenzaTimer);
        assertEquals('d',Gioco.direzione);
        // Test 7.3 L’istruzione deve fornire il feedback dove è presente una via in cui andare.
        // devo ottenere i percorsi possibili (direzioni) da una posizione x e y. Non ci interessa che il timer scada
        continueRun = true;
        Gioco.direzione = 'a';
        inizioTS = System.currentTimeMillis();
        HashMap<Character,Boolean> direzioniPossibili = new HashMap<>();
        int [] angoli = {0,90,180,-90};
        while(continueRun){
            boolean b = labirinthManager.valutaDirezione(angoli[r.nextInt(4)],labirinto.maze,Gioco.xu,Gioco.yu,Gioco.direzione);
            direzioniPossibili.put(Gioco.direzione,b);
            if(System.currentTimeMillis()-inizioTS>10000){
                System.out.println(Gioco.direzione + " TERZO TEST");
                continueRun=false;
            }
            Gioco.direzione='a';
        }
        assertFalse(continueRun);
        assertEquals(true,direzioniPossibili.get('i'));
        assertEquals(false,direzioniPossibili.get('a'));
        assertEquals(true,direzioniPossibili.get('d'));
        assertEquals(true,direzioniPossibili.get('s'));
        // 0 0 0
        // 1 1 1 ^ in avanti non è permesso
        // 0 1 0

    }

    // 8.1
    @Test
    public void testMemorizzazioneRecuperoLabirinto(){
        int [][] matrice = {
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,1,0,0,0,0,0,0,0,1,0,1,0,0,0,1,0,1,0,0,0,1,0,1,0,0,0,0,0,1,0,1,0,1,0,1,0},
            {0,1,0,1,0,1,1,1,0,1,0,1,0,1,1,1,1,1,1,1,1,1,9,1,1,1,0,1,1,1,1,1,1,1,0,1,0,1,1,0},
            {0,0,1,0,1,0,1,0,0,0,1,0,0,0,0,0,1,0,1,0,0,0,0,0,1,0,0,0,0,0,1,0,1,0,0,0,1,0,1,0},
            {0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,0,1,1,1,0,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,0},
            {0,0,1,0,1,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,1,0,1,0,0,0,1,0,1,0,0,0,1,0,1,0,1,0},
            {0,1,1,1,1,1,0,1,1,1,0,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0},
            {0,0,1,0,0,0,1,0,0,0,0,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,0,0,0,0,1,0,1,0,1,0,1,0,0,0},
            {0,1,0,1,1,1,1,1,0,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,0},
            {0,0,0,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,0,0,1,0,1,0,0,0,1,0,0,0,0,0,1,0,1,0,1,0,1,0},
            {0,1,1,1,1,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,0},
            {0,0,1,0,1,0,0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0,1,0,0,0,0,0,1,0,0,0,1,0,1,0,1,0},
            {0,1,1,1,1,1,0,1,0,1,1,1,0,1,0,1,0,1,1,1,0,1,0,1,1,1,1,1,1,1,0,1,1,1,1,1,0,1,1,0},
            {0,0,1,0,0,0,1,0,1,0,0,0,0,0,0,0,1,0,1,0,0,0,1,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0,1,0},
            {0,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,0,1,1,1,0,1,0,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,0,0},
            {0,0,0,0,0,0,0,0,1,0,1,0,1,0,1,0,0,0,1,0,0,0,1,0,1,0,0,0,1,0,1,0,0,0,1,0,0,0,1,0},
            {0,1,1,1,1,1,0,1,1,1,0,1,0,1,1,1,1,1,1,1,0,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,0,1,1,0},
            {0,0,1,0,1,0,0,0,1,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0,1,0},
            {0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,0,1,1,1,0,1,0,0},
            {0,0,0,0,1,0,1,0,0,0,1,0,0,0,1,0,1,0,0,0,0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0},
            {0,1,0,1,1,1,1,1,0,1,0,1,1,1,0,1,0,1,0,1,0,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,0,0},
            {0,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0,0,0,1,0,0,0,1,0,1,0,1,0,1,0,0,0,0,0,0,0,1,0,0,0},
            {0,1,1,1,1,1,0,1,1,1,1,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,0,1,0,1,0,0},
            {0,0,0,0,1,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,1,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,1,0,1,0},
            {0,1,8,1,1,1,0,1,0,1,1,1,1,1,0,1,1,1,0,1,0,1,1,1,1,1,1,1,0,1,1,1,0,1,0,1,1,1,0,0},
            {0,0,1,0,1,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0},
            {0,1,0,1,1,1,1,1,0,1,0,1,1,1,0,1,1,1,1,1,0,1,1,1,0,1,1,1,0,1,1,1,1,1,1,1,0,1,0,0},
            {0,0,1,0,0,0,1,0,1,0,1,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,1,0,1,0,1,0,0,0,1,0,1,0,0,0},
            {0,1,0,1,1,1,1,1,1,1,1,1,0,1,1,1,0,1,1,1,1,1,1,1,0,1,1,1,0,1,1,1,1,1,1,1,1,1,1,0},
            {0,0,1,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,1,0,1,0,1,0,1,0},
            {0,1,1,1,0,1,1,1,1,1,1,1,0,1,0,1,1,1,0,1,0,1,1,1,0,1,1,1,0,1,0,1,1,1,0,1,0,1,0,0},
            {0,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,0,0,1,0,1,0,0,0,1,0,1,0,0,0,0,0,1,0,0,0,0,0,1,0},
            {0,1,1,1,1,1,0,1,0,1,1,1,0,1,1,1,1,1,0,1,0,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,0,1,1,0},
            {0,0,0,0,0,0,1,0,0,0,1,0,1,0,1,0,0,0,0,0,1,0,0,0,1,0,1,0,0,0,0,0,0,0,1,0,1,0,1,0},
            {0,1,1,1,0,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,0,1,1,1,1,1,0,1,1,0},
            {0,0,1,0,1,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,0,0,1,0,0,0,1,0,0,0,0,0,1,0,0,0,1,0,0,0},
            {0,1,1,1,0,1,0,1,1,1,0,1,1,1,0,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,0,1,1,1,0,1,0,1,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
        };
        Labirinto labirinto = new Labirinto((matrice.length-2)/2,(matrice[0].length-2)/2);
        labirinto.maze = Labirinto.copiaLabirinto(matrice);
        Gioco.xu=labirinto.inizio.getX();
        Gioco.yu=labirinto.inizio.getY();
        String difficolta = "difficile";
        long timer = Gioco.returnTempo(difficolta);
        Salvataggio salvataggio = new Salvataggio();
        salvataggio.memorizzaStatoDelGioco(labirinto, appContext, timer, difficolta);
        assertTrue(salvataggio.presenzaPartitaSalvata(appContext));
        Labirinto otherLabirinth = salvataggio.recuperaPartita(appContext);
        assertArrayEquals(matrice,otherLabirinth.maze);
        assertEquals(labirinto.x,otherLabirinth.x);
        assertEquals(labirinto.y,otherLabirinth.y);
        salvataggio.partitaMemorizzata(appContext,false);
    }

    private void DEBUG(int[][] maze, int x, int y, int xu, int yu) {
        for(int i=0 ; i < x ; i++) {
            StringBuilder riga = new StringBuilder();
            for(int j=0 ; j < y ; j++) {
                if(i==xu && j==yu) {
                    riga.append("X");
                } else {
                    riga.append(maze[i][j]);
                }
            }
            System.out.println("lab: "+riga.toString()+"");
        }
    }

}
