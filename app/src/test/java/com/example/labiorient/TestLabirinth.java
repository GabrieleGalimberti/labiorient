package com.example.labiorient;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.lang.*;

import static org.junit.Assert.*;

public class TestLabirinth {

    // Test singoli per funzionalità

    // Test1.1 Test per controllare se Inizio e Fine corrispondono alla stessa Coordinata
    @Test
    public void testStessaCoordinataInizioFine() {
        int[][] matrice = new int[][]{
                {0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0}
        };
        Labirinto labirinto = new Labirinto(2,2);
        labirinto.maze = Labirinto.copiaLabirinto(matrice);
        Coordinata inizio = labirinto.definePosizione(labirinto.maze,8);
        Coordinata fine = labirinto.definePosizione(matrice,9);
        assertTrue(Controllo.stessaCoordinata(inizio,fine)); // Test 1.1
    }

    // Test1.2 Test per controllare se Inizio e Fine non corrispondono alla stessa Coordinata
    @Test
    public void testCoordinateInizioFineDiverse() {
        int[][] matrice = new int[][]{
                {0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0, 0},
                {0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0}
        };
        Labirinto labirinto = new Labirinto(2,2);
        labirinto.maze = Labirinto.copiaLabirinto(matrice);
        Coordinata inizio = labirinto.definePosizione(labirinto.maze,8);
        Coordinata fine = labirinto.definePosizione(labirinto.maze,9);
        assertTrue(inizio.getX()!=fine.getX() && inizio.getY()!=fine.getY()); // Test 1.2
    }

    // Test1.3 , Test1.4 Test per controllare se Inizio e Fine non corrispondono alla stessa Coordinata e distano di almeno 1 fra le loro coordinate di X e Y
    @Test
    public void testCoordinateInizioFineDiverseMaNonRispettoIlParametro() {
        int[][] matrice = new int[][]{
                {0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0},
                {0, 0, 1, 1, 0, 0},
                {0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0}
        };
        Labirinto labirinto = new Labirinto(2,2);
        labirinto.maze = Labirinto.copiaLabirinto(matrice);
        int parametro = 1;
        Coordinata inizio = labirinto.definePosizione(labirinto.maze,8);
        Coordinata fine = labirinto.definePosizione(labirinto.maze,9);
        assertFalse(Labirinto.distanzaRispettataInizioFine(inizio, fine, parametro)); // Test 1.3

        matrice = new int[][]{
                {0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0, 0},
                {0, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0}
        };
        labirinto = new Labirinto(2,2);
        labirinto.maze = Labirinto.copiaLabirinto(matrice);
        inizio = labirinto.definePosizione(labirinto.maze,8);
        fine = labirinto.definePosizione(labirinto.maze,9);
        assertFalse(Labirinto.distanzaRispettataInizioFine(inizio, fine, parametro)); // Test 1.3

        matrice = new int[][]{
                {0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0, 0},
                {0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0}
        };
        labirinto = new Labirinto(2,2);
        labirinto.maze = Labirinto.copiaLabirinto(matrice);
        inizio = labirinto.definePosizione(labirinto.maze,8);
        fine = labirinto.definePosizione(labirinto.maze,9);
        assertTrue(Labirinto.distanzaRispettataInizioFine(inizio,fine,parametro)); // Test 1.4
    }

    // Test Funzionalità 2 Test su distanza corretta pari a k>=3
    @Test
    public void testCoordinateInizioFineDiverseDistanzaMaggioreDelParametro() {
        int[][] matrice = new int[][]{
                {0, 0, 0, 0, 0, 0},
                {0, 1, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0}
        };
        Labirinto labirinto = new Labirinto(2,2);
        labirinto.maze = Labirinto.copiaLabirinto(matrice);
        Coordinata inizio = labirinto.definePosizione(labirinto.maze,8);
        Coordinata fine = labirinto.definePosizione(labirinto.maze,9);
        int parametro = 4;
        assertFalse(Labirinto.distanzaRispettataInizioFine(inizio, fine, parametro)); // Test 2.1
        parametro = 3;
        assertTrue(Labirinto.distanzaRispettataInizioFine(inizio, fine, parametro)); // Test 2.2
        parametro = 2;
        assertTrue(Labirinto.distanzaRispettataInizioFine(inizio, fine, parametro)); // Test 2.3
        parametro = 5;
        assertFalse(Labirinto.distanzaRispettataInizioFine(inizio, fine, parametro)); // Test 2.4
        parametro = 1;
        assertTrue(Labirinto.distanzaRispettataInizioFine(inizio, fine, parametro)); // Test 2.5
    }


    // Test3.1, Test3.4
    @Test
    public void testCaratteristicheLabirinto() {
        int[][] matrice = new int[][]{
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 8, 1, 1, 1, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 1, 0, 0},
                {0, 0, 0, 1, 1, 1, 9, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}
        };
        int[] caratteristiche = Labirinto.getCaratteristiche(matrice);
        assertEquals(1,caratteristiche[0]); // riconoscimento di vicoli ciechi Test3.1
        assertEquals(1,caratteristiche[1]); // riconoscimento di loop Test3.4
    }

    // Test5.1, Test5.3, Test5.4
    @Test
    public void testVerificaEsistenzaPercorso() {
        int[][] matrice = new int[][]{
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
        };
        Labirinto labirinto = new Labirinto(3,4);
        labirinto.maze = Labirinto.copiaLabirinto(matrice);
        Coordinata inizio = labirinto.definePosizione(labirinto.maze,8);
        labirinto.definePosizione(labirinto.maze,9);
        assertFalse(verificaLabirinto.pathExists(labirinto.maze, inizio, labirinto.maze.length, labirinto.maze[0].length));

        matrice = new int[][]{
                {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
        };
        labirinto = new Labirinto(3,4);
        labirinto.maze = Labirinto.copiaLabirinto(matrice);
        inizio = labirinto.definePosizione(labirinto.maze,8);
        labirinto.definePosizione(labirinto.maze,9);
        assertTrue(verificaLabirinto.pathExists(labirinto.maze, inizio, labirinto.maze.length, labirinto.maze[0].length));

        labirinto = new Labirinto(3,4);
        labirinto.maze = Labirinto.copiaLabirinto(matrice);
        inizio = labirinto.definePosizione(labirinto.maze,8);
        assertFalse(verificaLabirinto.pathExists(labirinto.maze, inizio, labirinto.maze.length, labirinto.maze[0].length));
    }

    // Test in cui verifico che le prestazioni precedenti portino alla costruzione di un labirinto con difficoltà
    // scelta in base agli esiti.

    private HashMap<String,Integer> valuations = new HashMap<>();
    private HashMap<String,Integer> counters = new HashMap<>();
    private int maxcontatore = 0, maxContatorePrestazioni = 0;
    private boolean insuccessoTotale=true, neutroTotale=true, successoTotale=true;
    private String diffPlusFreq="";
    private String difficolta = "";
    private CustomDecisionTree customDecisionTree = null;

    @Before
    public void setup(){
        valuations.clear();
        counters.clear();
        maxcontatore = 0;
        maxContatorePrestazioni = 0;
        insuccessoTotale=true;
        neutroTotale=true;
        successoTotale=true;
        diffPlusFreq="";
        difficolta = "";
        customDecisionTree = new CustomDecisionTree(0.6);
    }

    @Test
    public void testCustomDecisionTree1() {
        // Test 9.1   3 successi Facile
        valuations.put("facile successo", 3);
        valuations.put("media successo", 1);
        valuations.put("difficile insuccesso", 1);
        maxcontatore = 3;
        counters.put("facile successo", 12);
        counters.put("media successo", 4);
        counters.put("difficile insuccesso", 1);
        successoTotale = false;
        insuccessoTotale = false;
        neutroTotale = false;
        CustomDecisionTree customDecisionTree = new CustomDecisionTree(0.6);
        String difficolta = customDecisionTree.decision(maxcontatore, valuations, counters, insuccessoTotale, successoTotale, neutroTotale, diffPlusFreq, maxContatorePrestazioni);
        assertEquals("media", difficolta);
    }
    @Test
    public void testCustomDecisionTree2() {
        // Test 9.2   3 successi Media
        valuations.put("facile insuccesso", 1);
        valuations.put("media successo", 3);
        valuations.put("difficile insuccesso", 1);
        maxcontatore = 3;
        counters.put("facile insuccesso", 2);
        counters.put("media successo", 13);
        counters.put("difficile insuccesso", 1);
        successoTotale = false;
        insuccessoTotale = false;
        neutroTotale = false;
        customDecisionTree = new CustomDecisionTree(0.6);
        difficolta = customDecisionTree.decision(maxcontatore, valuations, counters, insuccessoTotale, successoTotale, neutroTotale, diffPlusFreq, maxContatorePrestazioni);
        assertEquals("difficile", difficolta);
    }
    @Test
    public void testCustomDecisionTree3() {
        // Test 9.3   3 successi Difficile
        valuations.put("facile insuccesso", 1);
        valuations.put("media insuccesso", 1);
        valuations.put("difficile successo", 3);
        maxcontatore = 3;
        counters.put("facile insuccesso", 2);
        counters.put("media successo", 4);
        counters.put("difficile successo", 15);
        successoTotale = false;
        insuccessoTotale = false;
        neutroTotale = false;
        customDecisionTree = new CustomDecisionTree(0.6);
        difficolta = customDecisionTree.decision(maxcontatore, valuations, counters, insuccessoTotale, successoTotale, neutroTotale, diffPlusFreq, maxContatorePrestazioni);
        assertEquals("difficile", difficolta);
    }

    @Test
    public void testCustomDecisionTree4() {
        // Test 9.4   3 insuccessi Facile
        valuations.put("facile insuccesso", 3);
        valuations.put("media successo", 1);
        valuations.put("difficile insuccesso", 1);
        maxcontatore = 3;
        counters.put("facile insuccesso", 12);
        counters.put("media successo", 4);
        counters.put("difficile insuccesso", 1);
        successoTotale = false;
        insuccessoTotale = false;
        neutroTotale = false;
        CustomDecisionTree customDecisionTree = new CustomDecisionTree(0.6);
        String difficolta = customDecisionTree.decision(maxcontatore, valuations, counters, insuccessoTotale, successoTotale, neutroTotale, diffPlusFreq, maxContatorePrestazioni);
        assertEquals("facile", difficolta);
    }


    @Test
    public void testCustomDecisionTree5() {
        // Test 9.5   3 insuccessi Media
        valuations.put("facile insuccesso", 1);
        valuations.put("media insuccesso", 3);
        valuations.put("difficile successo", 1);
        maxcontatore = 3;
        counters.put("facile insuccesso", 2);
        counters.put("media insuccesso", 13);
        counters.put("difficile successo", 4);
        successoTotale = false;
        insuccessoTotale = false;
        neutroTotale = false;
        customDecisionTree = new CustomDecisionTree(0.6);
        difficolta = customDecisionTree.decision(maxcontatore, valuations, counters, insuccessoTotale, successoTotale, neutroTotale, diffPlusFreq, maxContatorePrestazioni);
        assertEquals("facile", difficolta);
    }

    @Test
    public void testCustomDecisionTree6() {
        // Test 9.6   3 insuccessi Difficile
        valuations.put("facile successo", 1);
        valuations.put("media insuccesso", 1);
        valuations.put("difficile insuccesso", 3);
        maxcontatore = 3;
        counters.put("facile insuccesso", 2);
        counters.put("media insuccesso", 1);
        counters.put("difficile insuccesso", 15);
        successoTotale = false;
        insuccessoTotale = false;
        neutroTotale = false;
        customDecisionTree = new CustomDecisionTree(0.6);
        difficolta = customDecisionTree.decision(maxcontatore, valuations, counters, insuccessoTotale, successoTotale, neutroTotale, diffPlusFreq, maxContatorePrestazioni);
        assertEquals("media", difficolta);
    }

    @Test
    public void testCustomDecisionTree7() {
        // Test 9.7   3 neutro Facile
        valuations.put("facile neutro", 3);
        valuations.put("media successo", 1);
        valuations.put("difficile insuccesso", 1);
        maxcontatore = 3;
        counters.put("facile neutro", 9);
        counters.put("media successo", 4);
        counters.put("difficile insuccesso", 1);
        successoTotale = false;
        insuccessoTotale = false;
        neutroTotale = false;
        CustomDecisionTree customDecisionTree = new CustomDecisionTree(0.6);
        String difficolta = customDecisionTree.decision(maxcontatore, valuations, counters, insuccessoTotale, successoTotale, neutroTotale, diffPlusFreq, maxContatorePrestazioni);
        assertEquals("facile", difficolta);

    }

    @Test
    public void testCustomDecisionTree8() {
        // Test 9.8   3 neutro Media
        valuations.put("facile insuccesso", 1);
        valuations.put("media neutro", 3);
        valuations.put("difficile successo", 1);
        maxcontatore = 3;
        counters.put("facile insuccesso", 2);
        counters.put("media neutro", 9);
        counters.put("difficile successo", 4);
        successoTotale = false;
        insuccessoTotale = false;
        neutroTotale = false;
        customDecisionTree = new CustomDecisionTree(0.6);
        difficolta = customDecisionTree.decision(maxcontatore, valuations, counters, insuccessoTotale, successoTotale, neutroTotale, diffPlusFreq, maxContatorePrestazioni);
        assertEquals("media", difficolta);
    }

    @Test
    public void testCustomDecisionTree9() {
        // Test 9.9   3 neutro Difficile
        valuations.put("facile insuccesso", 1);
        valuations.put("media insuccesso", 1);
        valuations.put("difficile neutro", 3);
        maxcontatore = 3;
        counters.put("facile insuccesso", 2);
        counters.put("media insuccesso", 1);
        counters.put("difficile neutro", 9);
        successoTotale = false;
        insuccessoTotale = false;
        neutroTotale = false;
        customDecisionTree = new CustomDecisionTree(0.6);
        difficolta = customDecisionTree.decision(maxcontatore, valuations, counters, insuccessoTotale, successoTotale, neutroTotale, diffPlusFreq, maxContatorePrestazioni);
        assertEquals("difficile", difficolta);
    }

    @Test
    public void testCustomDecisionTree10() {
        // Test 9.10   2 successi Facile, 2 insuccessi Media, 1 * *
        // restituisco rispetto al successo su una difficoltà facile.
        valuations.put("facile successo", 2);
        valuations.put("media insuccesso", 2);
        valuations.put("difficile neutro", 1);
        maxcontatore = 2;
        counters.put("facile successo", 8);
        counters.put("media insuccesso", 4);
        counters.put("difficile neutro", 3);
        successoTotale = false;
        insuccessoTotale = false;
        neutroTotale = false;
        diffPlusFreq = "facile";
        maxContatorePrestazioni = 8;
        customDecisionTree = new CustomDecisionTree(0.6);
        difficolta = customDecisionTree.decision(maxcontatore, valuations, counters, insuccessoTotale, successoTotale, neutroTotale, diffPlusFreq, maxContatorePrestazioni);
        assertEquals("facile", difficolta);
    }

    @Test
    public void testCustomDecisionTree11() {
        // Test 9.10   2 successi Difficile, 2 insuccessi Media, 1 * *
        // restituisco rispetto al successo su una difficoltà difficile.
        valuations.put("difficile successo", 2);
        valuations.put("media insuccesso", 2);
        valuations.put("difficile neutro", 1);
        maxcontatore = 2;
        counters.put("difficile successo", 8);
        counters.put("media insuccesso", 4);
        counters.put("difficile neutro", 3);
        successoTotale = false;
        insuccessoTotale = false;
        neutroTotale = false;
        diffPlusFreq = "difficile";
        maxContatorePrestazioni = 8;
        customDecisionTree = new CustomDecisionTree(0.6);
        difficolta = customDecisionTree.decision(maxcontatore, valuations, counters, insuccessoTotale, successoTotale, neutroTotale, diffPlusFreq, maxContatorePrestazioni);
        assertEquals("difficile", difficolta);
    }

    @Test
    public void testCustomDecisionTree12() {
        // Test 9.12   2 * Media, 2 * Facile, 1 * *
        // restituisco rispetto al successo su una difficoltà facile.
        valuations.put("media successo", 2);
        valuations.put("facile insuccesso", 2);
        valuations.put("difficile neutro", 1);
        maxcontatore = 2;
        counters.put("media successo", 8);
        counters.put("facile insuccesso", 4);
        counters.put("difficile neutro", 3);
        successoTotale = false;
        insuccessoTotale = false;
        neutroTotale = false;
        diffPlusFreq = "media";
        maxContatorePrestazioni = 8;
        customDecisionTree = new CustomDecisionTree(0.6);
        difficolta = customDecisionTree.decision(maxcontatore, valuations, counters, insuccessoTotale, successoTotale, neutroTotale, diffPlusFreq, maxContatorePrestazioni);
        assertEquals("media", difficolta);
    }

    @Test
    public void testCustomDecisionTree13() {
        // Test 9.13   2 * Facile, 2 * Difficile, 1 * *
        valuations.put("media successo", 1);
        valuations.put("difficile insuccesso", 2);
        valuations.put("facile neutro", 2);
        maxcontatore = 2;
        counters.put("media successo", 4);
        counters.put("difficile insuccesso", 2);
        counters.put("facile neutro", 3);
        successoTotale = false;
        insuccessoTotale = false;
        neutroTotale = false;
        diffPlusFreq = "facile";
        maxContatorePrestazioni = 4;
        customDecisionTree = new CustomDecisionTree(0.6);
        difficolta = customDecisionTree.decision(maxcontatore, valuations, counters, insuccessoTotale, successoTotale, neutroTotale, diffPlusFreq, maxContatorePrestazioni);
        assertEquals("nessuna", difficolta);
    }

    @Test
    public void testCustomDecisionTree14() {
        // Test 9.14   2 * Media, 2 * Difficile, 1 * *
        valuations.put("media insuccesso", 2);
        valuations.put("difficile insuccesso", 2);
        valuations.put("facile neutro", 1);
        maxcontatore = 2;
        counters.put("media insuccesso", 2);
        counters.put("difficile insuccesso", 2);
        counters.put("facile neutro", 3);
        successoTotale = false;
        insuccessoTotale = false;
        neutroTotale = false;
        diffPlusFreq = "media";
        maxContatorePrestazioni = 3;
        customDecisionTree = new CustomDecisionTree(0.6);
        difficolta = customDecisionTree.decision(maxcontatore, valuations, counters, insuccessoTotale, successoTotale, neutroTotale, diffPlusFreq, maxContatorePrestazioni);
        assertEquals("nessuna", difficolta);
    }

    @Test
    public void testCustomDecisionTree15() {
        // Test 9.15 5 * * Tutti diversi return risultato != nessuna
        valuations.put("media insuccesso",1);
        valuations.put("difficile insuccesso",1);
        valuations.put("facile successo",1);
        valuations.put("media successo",1);
        valuations.put("difficile successo",1);

        maxcontatore = 1;

        counters.put("media insuccesso",2);
        counters.put("difficile insuccesso",2);
        counters.put("facile successo",4);
        counters.put("media successo",4);
        counters.put("difficile successo",4);

        successoTotale = false;
        insuccessoTotale = false;
        neutroTotale = false;
        diffPlusFreq = "facile";
        maxContatorePrestazioni = 4;

        customDecisionTree = new CustomDecisionTree(0.6);
        difficolta = customDecisionTree.decision(maxcontatore, valuations, counters, insuccessoTotale, successoTotale, neutroTotale, diffPlusFreq, maxContatorePrestazioni);
        assertNotEquals("nessuna",difficolta);
        assertEquals("facile", difficolta);
    }

    @Test
    public void testCustomDecisionTree16() {
        // Test 9.16 5 * * Tutti diversi return risultato == nessuna.
        valuations.put("media insuccesso", 1);
        valuations.put("difficile insuccesso", 1);
        valuations.put("facile successo", 1);
        valuations.put("media successo", 1);
        valuations.put("difficile neutro", 1);

        maxcontatore = 1;

        counters.put("media insuccesso", 2);
        counters.put("difficile insuccesso", 2);
        counters.put("facile successo", 4);
        counters.put("media successo", 5);
        counters.put("difficile neutro", 3);

        successoTotale = false;
        insuccessoTotale = false;
        neutroTotale = false;
        diffPlusFreq = "facile";
        maxContatorePrestazioni = 5;

        customDecisionTree = new CustomDecisionTree(0.6);
        difficolta = customDecisionTree.decision(maxcontatore, valuations, counters, insuccessoTotale, successoTotale, neutroTotale, diffPlusFreq, maxContatorePrestazioni);
        assertEquals("nessuna", difficolta);

    }

    @Test
    public void testCustomDecisionTree17() {
        // Test 9.17   Tutti successi difficoltà *
        successoTotale = true;
        insuccessoTotale = false;
        neutroTotale = false;
        maxcontatore = 2;
        customDecisionTree = new CustomDecisionTree(0.6);

        valuations.put("facile successo", 1);
        valuations.put("media successo", 2);
        valuations.put("difficile successo", 2);
        counters.put("facile successo", 6);
        counters.put("media successo", 9);
        counters.put("difficile successo", 10);
        maxContatorePrestazioni = 10;
        diffPlusFreq = "media";
        difficolta = customDecisionTree.decision(maxcontatore, valuations, counters, insuccessoTotale, successoTotale, neutroTotale, diffPlusFreq, maxContatorePrestazioni);
        assertEquals("difficile", difficolta);

        valuations.clear();
        counters.clear();

        valuations.put("facile successo", 2);
        valuations.put("media successo", 1);
        valuations.put("difficile successo", 2);
        counters.put("facile successo", 10);
        counters.put("media successo", 4);
        counters.put("difficile successo", 10);
        diffPlusFreq = "facile";
        difficolta = customDecisionTree.decision(maxcontatore, valuations, counters, insuccessoTotale, successoTotale, neutroTotale, diffPlusFreq, maxContatorePrestazioni);
        assertEquals("media", difficolta);

        valuations.clear();
        counters.clear();

        valuations.put("facile successo", 2);
        valuations.put("media successo", 2);
        valuations.put("difficile successo", 1);
        counters.put("facile successo", 10);
        counters.put("media successo", 9);
        counters.put("difficile successo", 4);
        diffPlusFreq = "facile";
        difficolta = customDecisionTree.decision(maxcontatore, valuations, counters, insuccessoTotale, successoTotale, neutroTotale, diffPlusFreq, maxContatorePrestazioni);
        assertEquals("media", difficolta);
    }

    @Test
    public void testCustomDecisionTree18() {
        // Test 9.18   Tutti insuccessi difficoltà *
        successoTotale = false;
        insuccessoTotale = true;
        neutroTotale = false;
        maxcontatore = 2;
        customDecisionTree = new CustomDecisionTree(0.6);

        valuations.put("facile insuccesso", 1);
        valuations.put("media insuccesso", 2);
        valuations.put("difficile insuccesso", 2);
        counters.put("facile insuccesso", 4);
        counters.put("media insuccesso", 2);
        counters.put("difficile insuccesso", 2);
        diffPlusFreq = "media";
        difficolta = customDecisionTree.decision(maxcontatore, valuations, counters, insuccessoTotale, successoTotale, neutroTotale, diffPlusFreq, maxContatorePrestazioni);
        assertEquals("facile", difficolta);

        valuations.clear();
        counters.clear();

        valuations.put("facile insuccesso", 2);
        valuations.put("media insuccesso", 1);
        valuations.put("difficile insuccesso", 2);
        counters.put("facile insuccesso", 10);
        counters.put("media insuccesso", 4);
        counters.put("difficile insuccesso", 10);
        diffPlusFreq = "facile";
        difficolta = customDecisionTree.decision(maxcontatore, valuations, counters, insuccessoTotale, successoTotale, neutroTotale, diffPlusFreq, maxContatorePrestazioni);
        assertEquals("facile", difficolta);

        valuations.clear();
        counters.clear();

        valuations.put("facile successo", 2);
        valuations.put("media successo", 2);
        valuations.put("difficile successo", 1);
        counters.put("facile successo", 10);
        counters.put("media successo", 9);
        counters.put("difficile successo", 4);
        diffPlusFreq = "facile";
        difficolta = customDecisionTree.decision(maxcontatore, valuations, counters, insuccessoTotale, successoTotale, neutroTotale, diffPlusFreq, maxContatorePrestazioni);
        assertEquals("facile", difficolta);
    }

    @Test
    public void testCustomDecisionTree19() {
        // Test 9.19 Tutti neutro *
        successoTotale = false;
        insuccessoTotale = false;
        neutroTotale = true;
        maxcontatore = 2;
        customDecisionTree = new CustomDecisionTree(0.6);

        valuations.put("facile neutro", 2);
        valuations.put("media neutro", 2);
        valuations.put("difficile neutro", 1);
        counters.put("facile successo", 6);
        counters.put("media successo", 6);
        counters.put("difficile successo", 3);
        diffPlusFreq = "facile";
        difficolta = customDecisionTree.decision(maxcontatore, valuations, counters, insuccessoTotale, successoTotale, neutroTotale, diffPlusFreq, maxContatorePrestazioni);
        assertEquals("facile", difficolta);

        valuations.clear();
        counters.clear();

        valuations.put("facile neutro", 1);
        valuations.put("media neutro", 2);
        valuations.put("difficile neutro", 2);
        counters.put("facile successo", 3);
        counters.put("media successo", 6);
        counters.put("difficile successo", 6);
        diffPlusFreq = "media";
        difficolta = customDecisionTree.decision(maxcontatore, valuations, counters, insuccessoTotale, successoTotale, neutroTotale, diffPlusFreq, maxContatorePrestazioni);
        assertEquals("media", difficolta);

        valuations.clear();
        counters.clear();

        valuations.put("facile neutro", 2);
        valuations.put("media neutro", 1);
        valuations.put("difficile neutro", 2);
        counters.put("facile successo", 6);
        counters.put("media successo", 3);
        counters.put("difficile successo", 6);
        diffPlusFreq = "facile";
        difficolta = customDecisionTree.decision(maxcontatore, valuations, counters, insuccessoTotale, successoTotale, neutroTotale, diffPlusFreq, maxContatorePrestazioni);
        assertEquals("facile", difficolta);
    }



    // Pairwise Tests

    @Test
    public void testPairwise1(){
        // 1.2 coordinata inizio != coordinata fine
        // 2.2 fine a distanza k dall'inizio
        // 3.2 0 vicoli ciechi
        // 4.1 labirinto di difficoltà facile
        for (int k=1; k<=10; k++) {
            String difficolta = "facile";
            Labirinto labirinto = Labirinto.estraiLabirinto(difficolta, 0, Labirinto.getLimiteLoop(difficolta), k);
            assertTrue(labirinto.inizio.getY() != labirinto.fine.getY() && labirinto.inizio.getX() != labirinto.fine.getX());
            assertTrue(k <= labirinto.parametro);
            assertEquals(0, labirinto.caratteristiche[0]);
            assertTrue(Labirinto.condizioneFacile(labirinto.caratteristiche, labirinto.x, labirinto.y));
            DEBUG(labirinto.maze, labirinto.x, labirinto.y, Gioco.xu, Gioco.yu);
        }

        int[][] matrice = new int[][]{
                {0,0,0,0,0,0,0,0,0,0,0,0},
                {0,1,1,9,0,0,0,0,0,0,0,0},
                {0,1,0,1,0,1,1,1,1,1,1,0},
                {0,1,1,1,1,1,0,0,0,0,1,0},
                {0,0,0,0,0,1,1,1,1,1,1,0},
                {0,0,0,0,0,1,0,0,1,0,1,0},
                {0,1,1,1,0,1,0,0,1,1,1,0},
                {0,1,0,1,0,1,0,0,1,0,1,0},
                {0,1,0,1,1,1,1,1,1,0,1,0},
                {0,1,0,0,0,8,0,0,0,1,1,0},
                {0,1,1,1,1,1,1,1,1,1,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0}
        };
        Labirinto labirinto = new Labirinto((matrice.length-2)/2,(matrice[0].length-2)/2);
        labirinto.maze = Labirinto.copiaLabirinto(matrice);
        labirinto.parametro = Labirinto.returnParametroDistance(matrice);
        int k = 2;
        labirinto.caratteristiche = Labirinto.getCaratteristiche(matrice);
        assertEquals(k, labirinto.parametro);
        assertEquals(0, labirinto.caratteristiche[0]);
        assertTrue(Labirinto.condizioneFacile(labirinto.caratteristiche, labirinto.x, labirinto.y));
    }


    @Test
    public void testPairwise2(){
        // 1.2 coordinata inizio != coordinata fine
        // 2.3 fine a distanza >=k+1 dall'inizio
        // 3.3 # vicoli ciechi <= Max
        // 4.2 labirinto di difficoltà media
        String difficolta = "media"; // 70
        int limiteVC = Labirinto.getLimiteVicoli("media");
        for (int k=0; k<=14; k++) {
            Labirinto labirinto = Labirinto.estraiLabirinto(difficolta, limiteVC, Labirinto.getLimiteLoop(difficolta), k + 1);
            assertTrue(labirinto.inizio.getY() != labirinto.fine.getY() && labirinto.inizio.getX() != labirinto.fine.getX());
            assertTrue(k+1 <= labirinto.parametro);
            assertTrue(labirinto.caratteristiche[0]<=limiteVC);
            assertTrue(Labirinto.condizioneMedia(labirinto.caratteristiche, labirinto.x, labirinto.y));
            DEBUG(labirinto.maze, labirinto.x, labirinto.y, Gioco.xu, Gioco.yu);
        }

        int[][] matrice = new int[][]{
                {0,0,0,0,0,0,0,0,0,0,0,0},
                {0,1,1,0,1,0,0,0,9,0,0,0},
                {0,1,0,1,0,1,1,1,1,1,1,0},
                {0,1,1,1,1,1,0,0,0,0,1,0},
                {0,0,1,0,0,1,1,1,0,1,0,0},
                {0,0,1,0,0,1,0,0,1,0,1,0},
                {0,1,1,1,0,1,0,0,1,1,1,0},
                {0,1,0,1,0,1,0,0,1,0,1,0},
                {0,1,1,1,8,1,1,1,1,0,0,0},
                {0,1,0,1,0,1,0,0,1,0,1,0},
                {0,1,1,1,1,1,1,1,1,0,1,0},
                {0,0,0,0,0,0,0,0,0,0,0,0}
        };
        Labirinto labirinto = new Labirinto((matrice.length-2)/2,(matrice[0].length-2)/2);
        labirinto.maze = Labirinto.copiaLabirinto(matrice);
        labirinto.parametro = Labirinto.returnParametroDistance(matrice);
        int k = 3;
        labirinto.caratteristiche = Labirinto.getCaratteristiche(matrice);
        labirinto.inizio = Labirinto.posizioneValore(8,matrice);
        labirinto.fine = Labirinto.posizioneValore(9,matrice);
        assertTrue(labirinto.inizio.getY() != labirinto.fine.getY() && labirinto.inizio.getX() != labirinto.fine.getX());
        assertTrue(k <= labirinto.parametro);
        assertTrue(labirinto.caratteristiche[0]<=limiteVC);
        assertTrue(Labirinto.condizioneMedia(labirinto.caratteristiche, labirinto.x, labirinto.y));
    }

    @Test
    public void testPairwise3(){
        // 1.2 coordinata inizio != coordinata fine
        // 2.4 fine a distanza >k+1 dall'inizio
        // 4.3 labirinto difficile
        String difficolta = "difficile";

        for (int k=0; k<=19; k++) {
            Labirinto labirinto = Labirinto.estraiLabirinto(difficolta, 300, Labirinto.getLimiteLoop(difficolta), k + 1);
            assertTrue( labirinto.inizio.getY() != labirinto.fine.getY() && labirinto.inizio.getX() != labirinto.fine.getX());
            assertTrue( k <= labirinto.parametro-1);
            assertTrue(300>= labirinto.caratteristiche[0]);
            assertTrue(Labirinto.condizioneDifficile(labirinto.caratteristiche));
            DEBUG(labirinto.maze, labirinto.x, labirinto.y, Gioco.xu, Gioco.yu);
        }
        int [][] matrice = {
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,1,0,0,0,1,0,0,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0},
            {0,1,1,1,1,1,0,1,1,1,0,1,1,1,0,1,1,1,1,1,1,1,0,1,0,1,1,1,1,1,0,1,0,1,1,1,1,1,0,0},
            {0,0,0,0,0,0,1,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,0,0,1,0},
            {0,1,0,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,0,0},
            {0,0,0,0,1,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0,1,0,1,0,0,0,1,0,0,0,0,0,1,0,1,0,1,0,1,0},
            {0,1,1,1,1,1,0,1,9,1,1,1,0,1,0,1,0,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,1,1,0,1,1,0},
            {0,0,0,0,0,0,0,0,1,0,1,0,1,0,0,0,1,0,0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0},
            {0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,0,1,1,1,1,1,1,1,0,1,0,1,0,1,1,1,1,1,1,0},
            {0,0,1,0,1,0,0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,0,0,1,0,0,0,0,0},
            {0,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,0,1,1,1,1,1,0,0},
            {0,0,1,0,0,0,1,0,1,0,0,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,0,0,0,0,1,0,1,0,1,0,0,0,1,0},
            {0,1,0,1,0,1,0,1,0,1,1,1,1,1,0,1,1,1,0,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,1,1,1,1,1,0},
            {0,0,1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,1,0,1,0,1,0,1,0,1,0},
            {0,1,1,1,1,1,0,1,1,1,1,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0},
            {0,0,0,0,1,0,0,0,0,0,1,0,1,0,0,0,1,0,0,0,1,0,0,0,0,0,1,0,1,0,0,0,1,0,1,0,1,0,0,0},
            {0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,0,1,1,1,0,1,0,1,0,1,1,1,1,1,1,0},
            {0,0,0,0,1,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0,0,0,1,0,1,0,1,0},
            {0,1,1,1,0,1,0,1,1,1,1,1,1,1,0,1,1,1,0,1,0,1,0,1,1,1,1,1,0,1,0,1,1,1,0,1,1,1,1,0},
            {0,0,1,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,1,0,1,0,1,0,1,0,0,0},
            {0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,0,0},
            {0,0,0,0,1,0,0,0,0,0,1,0,0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0,1,0,0,0,0,0,1,0},
            {0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,1,1,1,1,0,1,1,1,0,1,1,1,0,0},
            {0,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0,1,0,0,0,1,0,1,0,0,0,1,0,1,0,1,0},
            {0,1,1,1,1,1,1,1,0,1,0,1,0,1,1,1,1,1,0,1,1,1,1,1,1,1,0,1,1,1,0,1,0,1,1,1,0,1,1,0},
            {0,0,1,0,0,0,0,0,1,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,1,0,0,0,1,0,1,0,1,0,1,0,1,0,1,0},
            {0,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,0},
            {0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,1,0,0,0,1,0,1,0,0,0,1,0},
            {0,1,0,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,1,0,1,0,1,0,0,0,1,0,0,0,8,0,1,0,1,0,1,0},
            {0,1,1,1,0,1,1,1,0,1,1,1,0,1,1,1,0,1,1,1,0,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,1,1,1,0},
            {0,0,1,0,0,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0,1,0,1,0,0,0,1,0,1,0},
            {0,1,0,1,0,1,1,1,0,1,0,1,0,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
        };
        Labirinto labirinto = new Labirinto((matrice.length-2)/2,(matrice[0].length-2)/2);
        labirinto.maze = Labirinto.copiaLabirinto(matrice);
        labirinto.parametro = Labirinto.returnParametroDistance(matrice);
        int k = 5;
        labirinto.caratteristiche = Labirinto.getCaratteristiche(matrice);
        labirinto.inizio = Labirinto.posizioneValore(8,matrice);
        labirinto.fine = Labirinto.posizioneValore(9,matrice);
        assertTrue(labirinto.inizio.getY() != labirinto.fine.getY() && labirinto.inizio.getX() != labirinto.fine.getX());
        assertTrue(k+2 <= labirinto.parametro);
        assertTrue(Labirinto.condizioneDifficile(labirinto.caratteristiche));
    }

    @Test
    public void testPairwise4(){
        // 1.2 coordinata inizio != coordinata fine
        // 3.6 Riconoscimento # Loop <= Max
        String difficolta = "facile";
        Labirinto labirinto = Labirinto.estraiLabirinto(difficolta, Labirinto.getLimiteVicoli(difficolta), Labirinto.getLimiteLoop(difficolta), 0);
        assertTrue(labirinto.inizio.getY() != labirinto.fine.getY() && labirinto.inizio.getX() != labirinto.fine.getX());
        assertTrue(Labirinto.getLimiteLoop(difficolta)>= labirinto.caratteristiche[1]);
        DEBUG(labirinto.maze, labirinto.x, labirinto.y, Gioco.xu, Gioco.yu);
        difficolta = "media";
        labirinto = Labirinto.estraiLabirinto(difficolta, Labirinto.getLimiteVicoli(difficolta), Labirinto.getLimiteLoop(difficolta), 0);
        assertTrue(labirinto.inizio.getY() != labirinto.fine.getY() && labirinto.inizio.getX() != labirinto.fine.getX());
        assertTrue(Labirinto.getLimiteLoop(difficolta)>= labirinto.caratteristiche[1]);
        DEBUG(labirinto.maze, labirinto.x, labirinto.y, Gioco.xu, Gioco.yu);
        difficolta = "difficile";
        labirinto = Labirinto.estraiLabirinto(difficolta, Labirinto.getLimiteVicoli(difficolta), Labirinto.getLimiteLoop(difficolta), 0);
        assertTrue(labirinto.inizio.getY() != labirinto.fine.getY() && labirinto.inizio.getX() != labirinto.fine.getX());
        assertTrue(Labirinto.getLimiteLoop(difficolta)>= labirinto.caratteristiche[1]);
        DEBUG(labirinto.maze, labirinto.x, labirinto.y, Gioco.xu, Gioco.yu);
    }

    @Test
    public void testPairwise5() {
        // 2.3 test con labirinto con fine a distanza >=k+1 dall’inizio
        // 3.5 Riconoscimento 0 Loop di intersezioni
        String difficolta = "facile";
        for (int k = 0; k <= 9; k++){
            System.out.println(k);
            Labirinto labirinto = Labirinto.estraiLabirinto(difficolta, Labirinto.getLimiteVicoli(difficolta), 0, k+1);
            assertEquals(0, labirinto.caratteristiche[1]);
            assertTrue(Labirinto.distanzaRispettataInizioFine(labirinto.inizio,labirinto.fine,k));
            DEBUG(labirinto.maze, labirinto.x, labirinto.y, Gioco.xu, Gioco.yu);
        }
        int[][] matrice = new int[][]{
                {0,0,0,0,0,0,0,0,0,0,0,0},
                {0,1,1,1,0,0,0,0,0,0,0,0},
                {0,1,0,1,0,1,1,0,1,1,1,0},
                {0,1,0,1,1,1,0,0,0,0,1,0},
                {0,0,0,0,0,1,1,1,1,1,9,0},
                {0,0,0,0,0,1,0,0,1,0,1,0},
                {0,1,1,1,0,1,0,0,0,1,1,0},
                {0,1,0,1,0,1,0,0,1,0,1,0},
                {0,1,0,1,1,1,1,1,1,0,1,0},
                {0,1,0,0,0,0,8,0,0,1,1,0},
                {0,1,0,1,1,1,1,1,1,0,0,0},
                {0,0,0,0,0,0,0,0,0,0,0,0}
        };
        Labirinto labirinto = new Labirinto((matrice.length-2)/2,(matrice[0].length-2)/2);
        labirinto.maze = Labirinto.copiaLabirinto(matrice);
        labirinto.parametro = Labirinto.returnParametroDistance(matrice);
        int k = 2;
        labirinto.caratteristiche = Labirinto.getCaratteristiche(matrice);
        assertTrue(k < labirinto.parametro);
        assertEquals(0, labirinto.caratteristiche[1]);
        assertTrue(Labirinto.condizioneFacile(labirinto.caratteristiche,labirinto.x,labirinto.y));

    }

    @Test
    public void testPairwise6(){
        // 2.4 test con labirinto con fine a distanza >k+1 dall’inizio
        // 3.6 riconoscimento di #loop <= Max
        // 4.1 difficoltà facile
        int k=8;
        for (int limite = 0 ; limite<=Labirinto.getLimiteLoop("facile"); limite++) {
            Labirinto labirinto = Labirinto.estraiLabirinto("facile", Labirinto.getLimiteVicoli("facile"), limite, k+2);
            int[] c = Labirinto.getCaratteristiche(labirinto.maze);
            assertTrue(Labirinto.condizioneFacile(c, labirinto.x, labirinto.y));
            assertEquals(k+2, labirinto.parametro);
            DEBUG(labirinto.maze, labirinto.x, labirinto.y, Gioco.xu, Gioco.yu);
        }
        int[][] matrice = new int[][]{
            {0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,1,0,0,0,0,0,9,0,0,0},
            {0,1,0,1,0,1,1,1,1,1,1,0},
            {0,0,0,0,0,0,1,0,1,0,1,0},
            {0,1,0,1,1,1,1,1,1,1,1,0},
            {0,0,0,0,8,0,0,0,1,0,1,0},
            {0,1,0,1,1,1,0,1,1,1,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0}
        };
        k=1;
        int [] c = Labirinto.getCaratteristiche(matrice);
        assertTrue(Labirinto.distanzaRispettataInizioFine(Labirinto.posizioneValore(9,matrice),Labirinto.posizioneValore(8,matrice),k+2));
        assertTrue(c[1]<=Labirinto.getLimiteLoop("facile"));
        assertTrue(Labirinto.condizioneFacile(c,matrice.length,matrice[0].length));
    }

    @Test
    public void testPairwise7(){
        // 3.2 0 vicoli ciechi
        // 4.2 labirinto difficoltà media
        for (int limite = 3 ; limite<=Labirinto.getLimiteLoop("media"); limite++) {
            Labirinto labirinto = Labirinto.estraiLabirinto("media", 0, limite, 0);
            int[] c = Labirinto.getCaratteristiche(labirinto.maze);
            assertTrue(Labirinto.condizioneMedia(c, labirinto.x, labirinto.y));
            assertEquals(0, c[0]);
            DEBUG(labirinto.maze, labirinto.x, labirinto.y, Gioco.xu, Gioco.yu);
        }
    }

    @Test
    public void testPairwise8(){
        // 2.2 labirinto con fine a distanza >=k dall’inizio
        // 3.3 # vicoli ciechi <= Max
        // 4.3 labirinto difficoltà difficile
        int[][] matrice = new int[][]{
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {0,0,1,0,1,0,1,0,1,0,1,0,0,0,1,0,0,0,0,0,1,0,1,0,0,0,0,0,1,0,0,0,1,0,1,0,1,0},
            {0,1,0,1,0,1,0,1,1,1,1,1,1,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,0,1,0,0},
            {0,0,0,0,0,0,1,0,1,0,8,0,0,0,1,0,1,0,0,0,1,0,1,0,1,0,1,0,1,0,0,0,0,0,1,0,1,0},
            {0,1,0,1,1,1,0,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,0,1,1,1,1,1,1,1,1,1,0,1,0,1,0,0},
            {0,0,1,0,1,0,1,0,0,0,1,0,0,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,1,0},
            {0,1,1,1,0,1,0,1,0,1,1,1,1,1,0,1,1,1,0,1,0,1,0,1,0,1,1,1,0,1,0,1,1,1,1,1,1,0},
            {0,0,1,0,0,0,1,0,1,0,1,0,1,0,1,0,0,0,1,0,0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0},
            {0,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,0,1,0,0},
            {0,0,0,0,1,0,1,0,1,0,0,0,0,0,1,0,1,0,1,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0},
            {0,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,0,1,1,1,0,1,1,1,1,1,1,0},
            {0,0,1,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,0,0,1,0,0,0,1,0,1,0,0,0,1,0,1,0,1,0,1,0},
            {0,1,0,1,1,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,0,1,1,1,0,1,0,0},
            {0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,1,0,0,0,0,0,1,0},
            {0,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,1,1,0,0},
            {0,0,0,0,1,0,0,0,1,0,0,0,1,0,1,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,1,0,0,0,0,0,1,0},
            {0,1,1,1,0,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,0,1,0,1,1,1,0,1,0,0},
            {0,0,1,0,0,0,1,0,0,0,1,0,0,0,0,0,1,0,0,0,1,0,1,0,1,0,1,0,0,0,1,0,0,0,1,0,1,0},
            {0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,0,1,1,1,0,1,1,0},
            {0,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0,1,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,1,0,1,0,0,0},
            {0,1,1,1,0,1,1,1,1,1,1,1,1,1,0,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,0,1,0,0},
            {0,0,1,0,1,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,1,0,1,0,1,0,0,0,1,0},
            {0,1,0,1,0,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,9,1,0,1,0,1,1,0},
            {0,0,0,0,0,0,0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0,1,0,1,0,0,0},
            {0,1,0,1,0,1,0,1,1,1,1,1,0,1,1,1,0,1,0,1,0,1,0,1,1,1,1,1,0,1,1,1,0,1,1,1,0,0},
            {0,0,0,0,1,0,0,0,1,0,0,0,0,0,1,0,1,0,1,0,0,0,1,0,1,0,0,0,0,0,0,0,1,0,1,0,0,0},
            {0,1,0,1,1,1,1,1,0,1,0,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,1,1,0,1,0,1,0,1,1,1,0,0},
            {0,0,0,0,0,0,1,0,1,0,0,0,1,0,1,0,1,0,0,0,1,0,0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0},
            {0,1,1,1,0,1,0,1,0,1,0,1,1,1,0,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0},
            {0,0,0,0,0,0,1,0,1,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,1,0,1,0},
            {0,1,1,1,0,1,1,1,1,1,0,1,1,1,0,1,0,1,1,1,1,1,0,1,0,1,0,1,0,1,1,1,1,1,0,1,1,0},
            {0,0,0,0,1,0,1,0,0,0,0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0,1,0,0,0},
            {0,1,0,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,0,1,1,1,0,1,1,1,0,1,1,1,0,1,1,1,1,0},
            {0,0,1,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,1,0,1,0,1,0,1,0,1,0,0,0,0,0,1,0},
            {0,1,0,1,1,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,1,1,1,0},
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
        };
        int [] c = Labirinto.getCaratteristiche(matrice);
        assertTrue(Labirinto.distanzaRispettataInizioFine(Labirinto.posizioneValore(9,matrice),Labirinto.posizioneValore(8,matrice), Math.min(Math.max(matrice.length,matrice[0].length)/2,20)));
        assertTrue(c[1]<=Labirinto.getLimiteLoop("difficile"));
        assertTrue(Labirinto.condizioneDifficile(c));
    }

    @Test
    public void testPairwise9(){
        // 2.2 labirinto con fine a distanza >=k dall’inizio
        // 3.5 Riconoscimento 0 loop
        // 4.1 difficoltà facile
        int[][] matrice = {
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
            {0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0},
            {0,0,0,0,0,1,1,0,0,0,0,1,1,8,1,0},
            {0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0},
            {0,0,0,0,0,1,1,0,0,1,1,1,1,0,0,0},
            {0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0},
            {0,1,1,0,0,1,1,1,1,1,1,0,0,1,1,0},
            {0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0},
            {0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,0},
            {0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0},
            {0,0,0,0,1,1,1,0,0,1,1,0,0,0,0,0},
            {0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0},
            {0,9,1,1,1,1,1,1,1,1,1,1,1,0,0,0},
            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
        };
        int [] c = Labirinto.getCaratteristiche(matrice);
        assertEquals(0, c[1]);
        assertTrue(Labirinto.condizioneFacile(c,matrice.length,matrice[0].length));
        assertTrue(Labirinto.distanzaRispettataInizioFine(Labirinto.posizioneValore(9,matrice),Labirinto.posizioneValore(8,matrice), 10));
    }

    @Test
    public void testPairwise10(){
        // 2.2 labirinto con fine a distanza >=k dall’inizio
        // 3.6 # Loop <= Max
        // 4.3 difficoltà difficile
        for (int k = 1; k<=20; k++){
            Labirinto labirinto = Labirinto.estraiLabirinto("difficile", Labirinto.getLimiteVicoli("difficile"), Labirinto.getLimiteLoop("difficile"), k);
            assertTrue(Labirinto.distanzaRispettataInizioFine(labirinto.inizio,labirinto.fine, k));
            assertTrue(Labirinto.condizioneDifficile(Labirinto.getCaratteristiche(labirinto.maze)));
        }

        int [][] matrice = {
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                {0,0,0,0,1,0,0,0,1,0,0,0,1,0,1,0,1,0,0,0,1,0,1,0,0,0,1,0,1,0,1,0,1,0,0,0,1,0,0,0},
                {0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,0},
                {0,0,8,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,0,0,0,0,0,0,1,0,1,0,1,0,0,0,0,0},
                {0,1,0,1,1,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,0,1,1,1,1,1,0,1,0,1,1,0},
                {0,0,1,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,1,0,0,0,1,0,0,0,0,0,1,0,1,0,0,0,1,0},
                {0,1,1,1,1,1,0,1,1,1,1,1,1,1,0,1,1,1,0,1,1,1,1,1,0,1,0,1,1,1,1,1,0,1,1,1,0,1,0,0},
                {0,0,1,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0,1,0,0,0,1,0,1,0},
                {0,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,0,1,0,1,1,1,0,1,1,1,1,1,1,1,1,0},
                {0,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0},
                {0,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,0,1,1,1,0,1,0,1,1,1,1,1,1,0},
                {0,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,1,0,0,0,1,0,0,0,0,0,1,0,0,0,1,0,1,0,0,0,0,0},
                {0,1,0,1,0,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,0,1,1,1,0,1,1,0},
                {0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,1,0},
                {0,1,0,1,0,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,0,1,0,1,1,1,1,1,0,1,0,0},
                {0,0,1,0,1,0,1,0,1,0,0,0,1,0,0,0,1,0,1,0,0,0,1,0,1,0,1,0,1,0,1,0,0,0,0,0,1,0,0,0},
                {0,1,1,1,0,1,0,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,1,0},
                {0,0,1,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0,1,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,1,0},
                {0,1,1,1,1,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,0,1,0,1,1,1,0,1,1,1,1,1,1,0},
                {0,0,0,0,1,0,1,0,0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0,1,0,0,0,1,0,1,0,1,0,1,0,1,0},
                {0,1,0,1,1,1,0,1,1,1,1,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,0,1,0,1,1,0},
                {0,0,1,0,0,0,1,0,1,0,0,0,1,0,0,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,0,0,1,0,0,0,1,0,0,0},
                {0,1,0,1,1,1,0,1,0,1,1,1,0,1,0,1,0,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,1,1,0,1,1,0},
                {0,0,1,0,0,0,1,0,1,0,1,0,0,0,0,0,1,0,1,0,1,0,1,0,1,0,0,0,0,0,1,0,0,0,1,0,0,0,1,0},
                {0,1,0,1,1,1,0,1,0,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,1,1,1,1,0,1,0,1,1,0},
                {0,0,0,0,1,0,1,0,1,0,1,0,0,0,1,0,1,0,0,0,1,0,0,0,1,0,1,0,1,0,0,0,1,0,1,0,0,0,0,0},
                {0,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,1,1,1,0},
                {0,0,1,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,9,0,1,0,1,0,1,0,1,0,1,0},
                {0,1,1,1,1,1,1,1,1,1,0,1,0,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,0,1,0,1,0,1,1,1,1,0},
                {0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0,1,0,1,0,0,0,1,0,0,0,1,0},
                {0,1,1,1,0,1,1,1,0,1,1,1,0,1,1,1,1,1,1,1,0,1,0,1,1,1,1,1,1,1,0,1,1,1,0,1,0,1,1,0},
                {0,0,0,0,1,0,1,0,1,0,1,0,1,0,0,0,0,0,1,0,0,0,1,0,0,0,1,0,1,0,0,0,0,0,1,0,0,0,0,0},
                {0,1,1,1,0,1,1,1,1,1,1,1,1,1,0,1,1,1,0,1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,0,1,1,0},
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
        };
        int k = 2;
        int [] c = Labirinto.getCaratteristiche(matrice);
        assertTrue(Labirinto.distanzaRispettataInizioFine(Labirinto.posizioneValore(9,matrice),Labirinto.posizioneValore(8,matrice),k));
        assertTrue(c[1]<=Labirinto.getLimiteLoop("difficile"));
        assertTrue(Labirinto.condizioneDifficile(c));
    }

    @Test
    public void testPairwise11(){
        // 2.4 labirinto con fine a distanza >k+1 dall’inizio
        // 3.3 # vicoli ciechi <= Max
        // 4.1 labirinto facile
        for (int k = 0; k<=9; k++){
            System.out.println("k = "+k);
            Labirinto labirinto = Labirinto.estraiLabirinto("facile", Labirinto.getLimiteVicoli("facile"), Labirinto.getLimiteLoop("facile"), k+1);
            assertTrue(Labirinto.distanzaRispettataInizioFine(labirinto.inizio,labirinto.fine, k));
            assertTrue(Labirinto.condizioneFacile(Labirinto.getCaratteristiche(labirinto.maze),labirinto.x,labirinto.y));
        }
        int[][] matrice = new int[][]{
                {0,0,0,0,0,0,0,0,0,0},
                {0,0,1,0,0,0,0,0,0,0},
                {0,0,1,1,1,1,1,1,0,0},
                {0,0,1,0,1,0,0,1,0,0},
                {0,0,8,1,1,1,1,0,0,0},
                {0,0,1,0,1,0,1,1,0,0},
                {0,0,1,0,1,0,0,1,0,0},
                {0,0,1,0,1,0,0,1,0,0},
                {0,0,1,1,1,1,1,9,1,0},
                {0,0,0,0,0,0,0,0,0,0}
        };
        int k=1;
        int [] c = Labirinto.getCaratteristiche(matrice);
        assertTrue(Labirinto.distanzaRispettataInizioFine(Labirinto.posizioneValore(9,matrice),Labirinto.posizioneValore(8,matrice), k+2));
        assertTrue(c[0]<=Labirinto.getLimiteVicoli("facile"));
        assertTrue(Labirinto.condizioneFacile(c,matrice.length,matrice[0].length));
    }

    private void DEBUG(int[][] maze, int x, int y, int xu, int yu) {
        for(int i=0 ; i < x ; i++) {
            StringBuilder riga = new StringBuilder();
            for(int j=0 ; j < y ; j++) {
                if(i==xu && j==yu) {
                    riga.append("X");
                } else {
                    riga.append(maze[i][j]);
                }
            }
            System.out.println("lab: "+riga.toString()+"");
        }
    }

}